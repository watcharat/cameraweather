<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>LocationSheet</name>
    <message>
        <location filename="../assets/LocationSheet.qml" line="348"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RefreshHeader</name>
    <message>
        <location filename="../assets/RefreshHeader.qml" line="28"/>
        <location filename="../assets/RefreshHeader.qml" line="71"/>
        <source>Pull to refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/RefreshHeader.qml" line="66"/>
        <source>Release to refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/RefreshHeader.qml" line="73"/>
        <source>Pull to refresh. Last refreshed </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/RefreshHeader.qml" line="107"/>
        <source>%L1y ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/RefreshHeader.qml" line="109"/>
        <location filename="../assets/RefreshHeader.qml" line="115"/>
        <source>%L1m ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/RefreshHeader.qml" line="111"/>
        <source>%L1d ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/RefreshHeader.qml" line="113"/>
        <source>%L1h ago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/RefreshHeader.qml" line="116"/>
        <source>just now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="38"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="135"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="147"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="153"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="161"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="167"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="175"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="181"/>
        <source>Check your Internet connection and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="188"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="193"/>
        <source>Determining the status. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="203"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="52"/>
        <source>Fahrenheit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1008"/>
        <source>Open Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1028"/>
        <source>Switch Camera</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1047"/>
        <source>Flash: ON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1075"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1090"/>
        <source>Capture!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1124"/>
        <source>Gallery</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1134"/>
        <source>Invite Friends</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1148"/>
        <source>Rate this App!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="1177"/>
        <source>Choose Your Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
