import bb.cascades 1.0

TabbedPane {
    Tab {
        title: "Weather"
        imageSource: "asset:///images/icon/ic_weather.png"
        MainPage {
            
        }
    }
    Tab {
        title: "About"
        imageSource: "asset:///images/icon/ic_info.png"
        AboutPage {
            
        }
    }
}
