import bb.cascades 1.0

Sheet {
    id: sheet
    property string nameLast: ""
    property string descLast: ""
    property string latitudeLast: ""
    property string longitudeLast: ""
    property variant editDialogV;
    property variant addDialogV;
    property string createtime: ""
    Page {
        id: page
        onCreationCompleted: {
            app.closeEditLocationSheet.connect(page.closeEditSheet);
            app.closeAddLocationSheet.connect(page.closeAddSheet);
        }
        function closeEditSheet(){
            editDialogV.close();
        }
        function closeAddSheet(){
            addDialogV.close();
        }
        titleBar: TitleBar {
            title: "Choose Location"
            acceptAction: ActionItem {
                //                title: "Refresh"
                imageSource: "images/icon/ic_add.png"
                onTriggered: {
                    //                    app.addLocation();
                    //                    addLocationSheet.open();
                    addDialogV = addSheet.createObject();
                    addDialogV.open();
                }

            }
            dismissAction: ActionItem {
                imageSource: "images/icon/ic_back.png"
                onTriggered: {
                    sheet.close();
                }
            }
        }
        attachedObjects: [
            ComponentDefinition {
                id: addSheet
                Sheet {
                    id: addLocationSheet
                    Page {
                        titleBar: TitleBar {
                            title: "Add Your Location"
                            dismissAction: ActionItem {
                                imageSource: "images/icon/ic_back.png"
                                onTriggered: {
                                    addLocationSheet.close();
                                }
                            }
                        }
                        Container {
                            topPadding: 30
                            leftPadding: 20
                            rightPadding: 20
                            Label {
                                text: "Name:"
                                textStyle.fontSize: FontSize.Large
                                textStyle.fontWeight: FontWeight.Bold
                            }
                            TextField {
                                id: nameTF
                                clearButtonVisible: true
                                focusHighlightEnabled: true
                                hintText: "Enter your location name here."
                            }
                            Label {
                                text: "**Currently your location name support only English."
                                textStyle.fontSize: FontSize.Small
                                textStyle.fontStyle: FontStyle.Italic
                                textStyle.color: Color.Red
                            }
                            Label {
                                text: "Description:"
                                textStyle.fontSize: FontSize.Large
                                textStyle.fontWeight: FontWeight.Bold
                            }
                            TextField {
                                id: descTF
                                clearButtonVisible: true
                                focusHighlightEnabled: true
                                hintText: "Describe your location here."
                                bottomMargin: 30
                            }
                            Divider {

                            }
                            Container {
                                layout: DockLayout {

                                }
                                horizontalAlignment: HorizontalAlignment.Fill
                                Label {
                                    text: "Country:"
                                    textStyle.fontSize: FontSize.Large
                                    textStyle.fontWeight: FontWeight.Bold
                                    verticalAlignment: VerticalAlignment.Center
                                }
                                Label {
                                    text: app.getCountryText()
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Right
                                }
                                bottomMargin: 13
                            }
                            Container {
                                layout: DockLayout {

                                }
                                horizontalAlignment: HorizontalAlignment.Fill
                                Label {
                                    text: "Latitude:"
                                    textStyle.fontSize: FontSize.Large
                                    textStyle.fontWeight: FontWeight.Bold
                                    verticalAlignment: VerticalAlignment.Center
                                }
                                Label {
                                    text: app.getLattitudeText()
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Right
                                }
                                bottomMargin: 13
                            }
                            Container {
                                layout: DockLayout {

                                }
                                horizontalAlignment: HorizontalAlignment.Fill
                                Label {
                                    text: "Longitude:"
                                    textStyle.fontSize: FontSize.Large
                                    textStyle.fontWeight: FontWeight.Bold
                                    verticalAlignment: VerticalAlignment.Center
                                }
                                Label {
                                    text: app.getLongitudeText()
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Right
                                }
                            }
                            Divider {

                            }
                            Button {
                                topMargin: 50
                                text: "Submit"
                                horizontalAlignment: HorizontalAlignment.Center
                                onClicked: {
                                    app.addLocation(nameTF.text, descTF.text);
                                }
                            }
                        }
                    }
                }
            },
            ComponentDefinition {
                id: editSheet
                Sheet {
                    id: editLocationSheet
                    Page {
                        titleBar: TitleBar {
                            title: "Edit Location"
                            dismissAction: ActionItem {
                                imageSource: "images/icon/ic_back.png"
                                onTriggered: {
                                    editLocationSheet.close();
                                }
                            }
                        }
                        Container {
                            topPadding: 30
                            leftPadding: 20
                            rightPadding: 20
                            Label {
                                text: "Name:"
                                textStyle.fontSize: FontSize.Large
                                textStyle.fontWeight: FontWeight.Bold
                            }
                            TextField {
                                id: nameTFEdit
                                text: sheet.nameLast
                                clearButtonVisible: true
                                focusHighlightEnabled: true
                                hintText: "Enter your location name here."
                            }
                            Label {
                                text: "**Currently your location name support only English."
                                textStyle.fontSize: FontSize.Small
                                textStyle.fontStyle: FontStyle.Italic
                                textStyle.color: Color.Red
                            }
                            Label {
                                text: "Description:"
                                textStyle.fontSize: FontSize.Large
                                textStyle.fontWeight: FontWeight.Bold
                            }
                            TextField {
                                id: descTFEdit
                                text: sheet.descLast
                                clearButtonVisible: true
                                focusHighlightEnabled: true
                                hintText: "Describe your location here."
                                bottomMargin: 30
                            }
                            Divider {

                            }
                            Container {
                                layout: DockLayout {

                                }
                                horizontalAlignment: HorizontalAlignment.Fill
                                Label {
                                    text: "Country:"
                                    textStyle.fontSize: FontSize.Large
                                    textStyle.fontWeight: FontWeight.Bold
                                    verticalAlignment: VerticalAlignment.Center
                                }
                                Label {
                                    text: app.getCountryText()
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Right
                                }
                                bottomMargin: 13
                            }
                            Container {
                                layout: DockLayout {

                                }
                                horizontalAlignment: HorizontalAlignment.Fill
                                Label {
                                    text: "Latitude:"
                                    textStyle.fontSize: FontSize.Large
                                    textStyle.fontWeight: FontWeight.Bold
                                    verticalAlignment: VerticalAlignment.Center
                                }
                                Label {
                                    text: sheet.latitudeLast
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Right
                                }
                                bottomMargin: 13
                            }
                            Container {
                                layout: DockLayout {

                                }
                                horizontalAlignment: HorizontalAlignment.Fill
                                Label {
                                    text: "Longitude:"
                                    textStyle.fontSize: FontSize.Large
                                    textStyle.fontWeight: FontWeight.Bold
                                    verticalAlignment: VerticalAlignment.Center
                                }
                                Label {
                                    text: sheet.longitudeLast
                                    verticalAlignment: VerticalAlignment.Center
                                    horizontalAlignment: HorizontalAlignment.Right
                                }
                            }
                            Divider {

                            }
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                Button {
                                    topMargin: 50
                                    text: "Cancel"
                                    horizontalAlignment: HorizontalAlignment.Center
                                    onClicked: {
                                        editLocationSheet.close();
                                    }
                                }
                                Button {
                                    topMargin: 50
                                    text: "Submit"
                                    horizontalAlignment: HorizontalAlignment.Center
                                    onClicked: {
                                        app.doEditLocation(sheet.createtime, nameTFEdit.text, descTFEdit.text);
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }

        ]
        Container {
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Fill
            layout: DockLayout {

            }
            Container {
                horizontalAlignment: HorizontalAlignment.Right
                topPadding: 10
                rightPadding: 15
                ImageView {
                    imageSource: "asset:///images/google_white.png"
                }
            }

            //            background: bg.imagePaint
            //            attachedObjects: [
            //                ImagePaintDefinition {
            //                    id: bg
            //                    imageSource: "images/bg_location.png"
            //                }
            //            ]
            ListView {
                id: locationListView
                objectName: "locationListView"
                dataModel: GroupDataModel {
                    id: locationDataModel
                    objectName: "locationDataModel"
                }
                listItemComponents: [
                    ListItemComponent {
                        type: "item"
                        StandardListItem {
                            title: ListItemData.areaName
                            description: ListItemData.region
                            //                            imageSource: ListItemData.icon//"images/redpin.png"
                        }
                    }
                ]
                onTriggered: {
                    sheetLoader.visible = true;
                    app.chooseLocation(indexPath);
                    sheet.close();
                }
                //Contexts
                property int activeItem: -1
                contextActions: [
                    ActionSet {
                        ActionItem {
                            title: qsTr("Edit")
                            imageSource: "asset:///images/icon/ic_edit.png"
                            onTriggered: {
                                console.log("action triggered: " + title + " active item: " + locationListView.activeItem)
                                var indexPath = new Array();
                                indexPath[0] = locationListView.activeItem;
                                var item = locationDataModel.data(indexPath);
//                                app.doEditLocation(item.areaName,item.region);
								sheet.createtime = item.createtime;
								sheet.nameLast = item.areaName;
                                sheet.descLast = item.region;
                                sheet.latitudeLast = item.latitude + "";
                                sheet.longitudeLast = item.longitude + "";
                                editDialogV = editSheet.createObject();
                                editDialogV.open();
                            }
                        }
                        
                    }
                ]
                onActivationChanged: {
                    //                        console.log("onActivationChanged, active: " + active)
                    if (active) locationListView.activeItem = indexPath[0]
                }
            }

            ActivityIndicator {
                visible: false
                id: sheetLoader
                running: true
                preferredHeight: 500
                preferredWidth: 500
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
            }
        }

    }

    onOpened: {
        sheetLoader.visible = false;
    }
}