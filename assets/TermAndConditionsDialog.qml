import bb.cascades 1.0

Dialog {
    id: dialog
    Container {
        preferredWidth: 768
        preferredHeight: 1280
        background: Color.Black
        layout: DockLayout {

        }
        topPadding: 20
        leftPadding: 20
        rightPadding: 20
        bottomPadding: 20
        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Top

            Label {
                text: "Terms of Use and Privacy Policy "
                textStyle.color: Color.White
                textStyle.fontWeight: FontWeight.Bold
                horizontalAlignment: HorizontalAlignment.Center
            }

            Label {
                text: "This application contains data from Google Maps API. By using this application you agree to the Google Maps API Terms of Service and Privacy Policy (in English) found at"
                textStyle.color: Color.White
                multiline: true
                horizontalAlignment: HorizontalAlignment.Center
                bottomMargin: 20
            }

            Label {
                text: "http://www.google.com/intl/en/policies/terms/"
                multiline: true
                textStyle.fontWeight: FontWeight.W600
                textStyle.color: Color.create(0.1,0.5,1.0)
                onTouch: {
                    if (event.isUp()) {
                        app.openTermOfService();
                    }
                }
            }
            Label {
                text: "and"
                textStyle.color: Color.White
                multiline: true
                bottomMargin: 20
            }
            Label {
                text: "http://www.google.com/intl/en/policies/privacy/"
                textStyle.color: Color.create(0.1, 0.5, 1.0)
                textStyle.fontWeight: FontWeight.W600
                multiline: true
                onTouch: {
                    if (event.isUp()) {
                        app.openPrivacy();
                    }
                }
            }
        }

        Container {
            horizontalAlignment: HorizontalAlignment.Center
            verticalAlignment: VerticalAlignment.Bottom
            layout: StackLayout {
                orientation: LayoutOrientation.LeftToRight
            }
            Button {
                text: "Decline"
                onClicked: {
                    dialog.close();
                    app.setTermCondition(false);
                }
            }
            Button {
                text: "Accept"
                onClicked: {
                    dialog.close();
                    app.setTermCondition(true);
                }
            }
        }

    }
}
