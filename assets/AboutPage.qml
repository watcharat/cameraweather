import bb.cascades 1.0

Page {
    signal close()
    titleBar: TitleBar {
        title: "About"
        dismissAction: ActionItem {  
            imageSource: "images/icon/ic_back.png"
            onTriggered: {
                close();
            }
        }
    }
    Container {
        layout: DockLayout {

        }
        
        ImageView {
            imageSource: "asset:///images/bg.jpg"
            preferredHeight: 1280
        }
        Container {
            topPadding: 60
            leftPadding: 30
            ImageView {
                imageSource: "asset:///images/logo.png"
            }
        }
        
        Container {
			topPadding: 210
			leftPadding: 50
			rightPadding: 50
            Label {
                text: "Version: 1.0.0"
                textStyle.fontWeight: FontWeight.Bold
//                textStyle.color: Color.White
            }
            Label {
                text: "mazmellow@gmail.com"
                textStyle.color: Color.Blue
                onTouch: {
                    if(event.isUp()){
                        app.openMail();
                    }
                }
            }
            
            Label {
                topMargin: 80
                text: "Google Maps API Terms of Service link: "
                textStyle.fontSize: FontSize.Small
            }
            Label {
                text: "http://www.google.com/intl/en/policies/terms/"
                multiline: true
                textStyle.color: Color.Blue
                textStyle.fontSize: FontSize.Small
                onTouch: {
                    if (event.isUp()) {
                        app.openTermOfService();
                    }
                }
            }

            Label {
                topMargin: 40
                text: "Google Maps API Privacy Policy link: "
                textStyle.fontSize: FontSize.Small
            }
            Label {
                text: "http://www.google.com/intl/en/policies/privacy/"
                textStyle.fontSize: FontSize.Small
                textStyle.color: Color.Blue
                multiline: true
                onTouch: {
                    if (event.isUp()) {
                        app.openPrivacy();
                    }
                }
            }
        }

        Container {
            verticalAlignment: VerticalAlignment.Bottom
            horizontalAlignment: HorizontalAlignment.Right
            rightPadding: 40
            bottomPadding: 50
            Label {
                text: "Powered weather data by"
                horizontalAlignment: HorizontalAlignment.Center
                textStyle.fontStyle: FontStyle.Italic
                textStyle.color: Color.Black
            }
            ImageView {
                preferredWidth: 267 * 2
                preferredHeight: 116 * 2
                imageSource: "asset:///images/logo_wwo.png"
                horizontalAlignment: HorizontalAlignment.Right
                verticalAlignment: VerticalAlignment.Bottom
            }
        }

    }
}
