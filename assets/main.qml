// Default empty project template
import bb.cascades 1.0
import bb.cascades.multimedia 1.0
import bb.system 1.0
import bb.cascades.pickers 1.0
import bb.cascades.advertisement 1.0

TabbedPane {
    function doShowTermConditionDialog() {
        var d = termDialog.createObject();
        d.open();
    }
    function doShowHelpDialog() {
        var d = helpDialog.createObject();
        d.open();
    }
    attachedObjects: [
        ComponentDefinition {
            id: termDialog
            TermAndConditionsDialog {

            }
        },
        ComponentDefinition {
            id: helpDialog
            HelpDialog {

            }
        }
    ]
    Menu.definition: MenuDefinition {
        // Add a Help action
        helpAction: HelpActionItem {
            onTriggered: {
                if (! app.isShowHelp()) {
                    app.showHelp();
                } else {
                    app.closeHelp();
                }
            }
        }

        // Add a Settings action
        //        settingsAction: SettingsActionItem {
        //        }

        // Add any remaining actions
        actions: [
            ActionItem {
                id: tempAction
                objectName: "tempAction"
                title: qsTr("Fahrenheit")
                property bool selectCelcius: true
                imageSource: "asset:///images/icon/ic_fahrenheit.png"
                onTriggered: {
                    if (app.loadComplete()) {
                        if (selectCelcius) {
                            setCelcius();
                        } else {
                            setFahrenheit();
                        }
                        app.setTemp(selectCelcius);
                        app.closeHelp();
                    } else {
                        //show toast please wait a minute
                        toast.show()
                    }

                }
                onCreationCompleted: {
                    app.cameraModeChanged.connect(actionFlash.onCameraModeChanged);
                }
                function setCelcius() {
                    tempAction.title = "Celcius";
                    tempAction.imageSource = "asset:///images/icon/ic_celcius.png"
                    tempAction.selectCelcius = false;
                }
                function setFahrenheit() {
                    tempAction.title = "Fahrenheit";
                    tempAction.imageSource = "asset:///images/icon/ic_fahrenheit.png"
                    tempAction.selectCelcius = true;
                }
            },
            ActionItem {
                title: "About"
                imageSource: "asset:///images/icon/ic_info.png"
                onTriggered: {
                    dialogSheet.open();
                }
                attachedObjects: [
                    Sheet {
                        id: dialogSheet
                        AboutPage {
                            id: aboutPage
                            onCreationCompleted: {
                                aboutPage.close.connect(dialogSheet.doClose);
                            }
                        }
                        function doClose() {
                            dialogSheet.close();
                        }
                    }
                ]
            }

        ]
    }
    onCreationCompleted: {
        _registrationHandler.registerApplication();
    }
    Tab {
        title: "Camera Weather"
        imageSource: "asset:///images/icon/ic_weather.png"
        // creates one page with a label
        Page {
            //    titleBar: TitleBar {
            //        title: "Camera Weather"
            //    }
            Container {
                id: bigContainer
                property int allHeight: 1140
                property int allWidth: 768
                layout: AbsoluteLayout {
                }
                background: Color.Black //create("#8ac6df")
                preferredHeight: allHeight
                preferredWidth: allWidth
                Container {
                    layoutProperties: AbsoluteLayoutProperties {
                        positionX: 0
                        positionY: 0
                    }
                    //            horizontalAlignment: HorizontalAlignment.Fill
                    //            verticalAlignment: VerticalAlignment.Fill
                    preferredHeight: bigContainer.allHeight
                    preferredWidth: bigContainer.allWidth
                    layout: DockLayout {

                    }
                    //            preferredHeight: 720
                    //            preferredWidth: 720

                    // CAMERA ZONE
                    Camera {
                        id: camera
                        objectName: "myCamera"
                        //                onTouch: {
                        //                    if (event.isDown()) {
                        //                        // Take photo
                        //                        capturePhoto();
                        //                    }
                        //                }

                        // When the camera is opened we want to start the viewfinder
                        onCameraOpened: {
                            camera.startViewfinder();
                        }

                        // There are loads of messages we could listen to here.
                        // onPhotoSaved and onShutterFired are taken care of in the C++ code.
                        onCameraOpenFailed: {
                            console.log("onCameraOpenFailed signal received with error " + error);
                        }
                        onViewfinderStartFailed: {
                            console.log("viewfinderStartFailed signal received with error " + error);
                        }
                        onViewfinderStopFailed: {
                            console.log("viewfinderStopFailed signal received with error " + error);
                        }
                        onPhotoCaptureFailed: {
                            console.log("photoCaptureFailed signal received with error " + error);
                        }
                        onPhotoSaveFailed: {
                            console.log("photoSaveFailed signal received with error " + error);
                        }
                        onPhotoSaved: {
                            app.manipulatePhoto(fileName);
                        }
                        onCameraResourceReleased: {
                            //screen dim
                            //                	console.log("--- onCameraResourceReleased");
                        }
                        onCameraResourceAvailable: {
                            //                    console.log("--- onCameraResourceAvailable");
                            app.restartCamera();
                        }
                    }

                    Container {
                        verticalAlignment: VerticalAlignment.Top
                        Container {
                            //top
                            id: header
                            preferredHeight: 58
                            preferredWidth: 768
                            background: bg_h.imagePaint
                            layout: DockLayout {

                            }
                            Container {
                                horizontalAlignment: HorizontalAlignment.Left
                                verticalAlignment: VerticalAlignment.Center
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                attachedObjects: [
                                    ImagePaintDefinition {
                                        id: bg_h
                                        imageSource: "asset:///images/bg_header.png"
                                    }
                                ]
                                //                                ImageView {
                                //                                    preferredHeight: header.preferredHeight
                                //                                    preferredWidth: header.preferredHeight
                                //                                    imageSource: "asset:///images/icon_programe.png"
                                //                                }
                                //                        Label {
                                //                            verticalAlignment: VerticalAlignment.Center
                                //                            text: "Camera Weather"
                                //                            textStyle.color: Color.White
                                //                            textStyle.fontWeight: FontWeight.Bold
                                //                            textStyle.fontSize: FontSize.Large
                                //                        }
                            }

                            //                            Banner {
                            //                                horizontalAlignment: HorizontalAlignment.Right
                            //                                verticalAlignment: VerticalAlignment.Center
                            //                                // zone id is used to identify your application and to track Ad performance
                            //                                // metrics by the Advertising Service
                            //                                zoneId: 187440
                            //                                refreshRate: 60
                            //                                preferredWidth: 320
                            //                                preferredHeight: 50
                            //                                transitionsEnabled: false
                            //                                // Place holder used when there is no connection to the Advertising Service
                            //                                placeHolderURL: "asset:///images/placeholder_banner.png"
                            //                                //backgroundColor: Color.Green
                            //                                //borderColor: Color.Gray
                            //                                //borderWidth: 2
                            //                            }
                        }
                        ImageView {
                            objectName: "imageView"
                            visible: false
                            preferredHeight: 1024
                            preferredWidth: 768
                        }
                    }

                    Container {
                        verticalAlignment: VerticalAlignment.Bottom
                        //banner
                        //                Container {
                        //                    id: bannerContainer
                        //                    verticalAlignment: VerticalAlignment.Bottom
                        //                    horizontalAlignment: HorizontalAlignment.Center
                        //                    preferredWidth: 768
                        //                    preferredHeight: 120
                        //                    layout: AbsoluteLayout {
                        //                    }
                        //                    Container {
                        //                        id: banner
                        //                        property bool isShow: false
                        //                        layoutProperties: AbsoluteLayoutProperties {
                        //                            positionX: 0 //768
                        //                            positionY: 0 //bannerContainer.preferredHeight
                        //                        }
                        //                        scaleX: 2.4
                        //                        scaleY: 2.4
                        //                        Banner {
                        //                            zoneId: 187440
                        //                            refreshRate: 60
                        //                            preferredWidth: 320
                        //                            preferredHeight: 50
                        //                            transitionsEnabled: true
                        //                        }
                        //                        attachedObjects: [
                        //                            ImplicitAnimationController {
                        //                                propertyName: "translationY"
                        //                                enabled: true
                        //                            }
                        //                        ]
                        //
                        //                        function showSubAction() {
                        //                            //                        translationY -= bannerContainer.preferredHeight;
                        //                            translationX -= bannerContainer.preferredWidth;
                        //                            isShow = true;
                        //                        }
                        //
                        //                        function hideSubAction() {
                        //                            //                        translationY += bannerContainer.preferredHeight;
                        //                            translationX += bannerContainer.preferredWidth;
                        //                            isShow = false;
                        //                        }
                        //                    }
                        //                    attachedObjects: [
                        //                        QTimer {
                        //                            id: timer
                        //                            interval: 10000
                        //                            onTimeout: {
                        //                                console.log("----------- TIMER TIMEOUT -----");
                        //                                if (! banner.isShow) {
                        //                                    banner.showSubAction();
                        //                                } else {
                        //                                    banner.hideSubAction();
                        //                                }
                        //                                timer.start();
                        //                            }
                        //                        }
                        //                    ]
                        //                    onCreationCompleted: {
                        //                        //                timer.start();
                        //                    }
                        //                }

                        Container {
                            //bottom
                            id: pagingContainer
                            //                    verticalAlignment: VerticalAlignment.Bottom
                            preferredHeight: 58
                            preferredWidth: 768
                            background: Color.Black //create("#8ac6df")
                            layout: DockLayout {

                            }
                            Container {
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                horizontalAlignment: HorizontalAlignment.Center
                                verticalAlignment: VerticalAlignment.Center
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }
                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p1
                                        visible: true
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }
                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p2
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }

                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p3
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }

                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p4
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }

                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p5
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }

                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p6
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }

                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p7
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }

                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p8
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }

                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p9
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                                Container {
                                    leftMargin: 20
                                    rightMargin: 20
                                    layout: DockLayout {
                                    }

                                    ImageView {
                                        imageSource: "asset:///images/page_unfocus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                    ImageView {
                                        id: p10
                                        visible: false
                                        imageSource: "asset:///images/page_focus.png"
                                        preferredHeight: 20
                                        preferredWidth: 20
                                    }
                                }
                            }
                        }
                    }

                    //END CAMERA

                    //MAIN SCROLL
                    Container {
                        id: mainScroll
                        objectName: "mainScroll"
                        visible: false
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 768
                        preferredHeight: 58 + 1024

                        property int indexFilter: 0
                        property real xA: 0.0
                        property real xB: 0.0
                        onTouch: {
                            if (event.isDown()) {
                                xA = event.windowX;
                                //                        console.log("DOWN xA: " + xA);
                            } else if (event.isUp()) {
                                //                        console.log("UP");
                                xB = event.windowX;
                                //                        console.log("xB: " + xB);
                                var dX = xB - xA;
                                //                        console.log("dX: " + dX);
                                if (dX < 0) {
                                    if (dX < -50) {
                                        //move left  >> set scroll up
                                        indexFilter ++;
                                        if (indexFilter > 9) {
                                            indexFilter = 9;
                                        }
                                        scrollView.scrollToPoint(768 * indexFilter, 0.0, ScrollAnimation.Default);
                                    }
                                } else {
                                    if (dX > 50) {
                                        //move right  >> set scroll down
                                        indexFilter --;
                                        if (indexFilter < 0) {
                                            indexFilter = 0;
                                        }
                                        scrollView.scrollToPoint(768 * indexFilter, 0.0, ScrollAnimation.Default);
                                    }
                                }
                                console.log("indexFilter = " + indexFilter);

                                p1.visible = false;
                                p2.visible = false;
                                p3.visible = false;
                                p4.visible = false;
                                p5.visible = false;
                                p6.visible = false;
                                p7.visible = false;
                                p8.visible = false;
                                p9.visible = false;
                                p10.visible = false;

                                if (indexFilter == 0) {
                                    p1.visible = true;
                                } else if (indexFilter == 1) {
                                    p2.visible = true;
                                } else if (indexFilter == 2) {
                                    p3.visible = true;
                                } else if (indexFilter == 3) {
                                    p4.visible = true;
                                } else if (indexFilter == 4) {
                                    p5.visible = true;
                                } else if (indexFilter == 5) {
                                    p6.visible = true;
                                } else if (indexFilter == 6) {
                                    p7.visible = true;
                                } else if (indexFilter == 7) {
                                    p8.visible = true;
                                } else if (indexFilter == 8) {
                                    p9.visible = true;
                                } else if (indexFilter == 9) {
                                    p10.visible = true;
                                }
                            }
                        }

                        ScrollView {
                            id: scrollView
                            Container {

                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                //MAIN CONTAINER
                                Container {
                                    objectName: "mainContainer"
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //                            horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer2"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer3"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer4"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer5"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer6"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer7"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer8"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer9"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }

                                Container {
                                    objectName: "mainContainer10"
                                    //                            visible: false
                                    preferredWidth: 768
                                    preferredHeight: mainScroll.preferredHeight
                                    //verticalAlignment: VerticalAlignment.Fill
                                    //horizontalAlignment: HorizontalAlignment.Fill
                                    layout: AbsoluteLayout {

                                    }
                                }
                            }
                            scrollViewProperties {
                                scrollMode: ScrollMode.Horizontal
                            }
                        }

                    }

                    Container {
                        id: helpContainer
                        objectName: "helpContainer"
                        visible: true
                        verticalAlignment: VerticalAlignment.Top
                        horizontalAlignment: HorizontalAlignment.Center
                        preferredWidth: 768
                        preferredHeight: 58 + 1024 + 58
                        layout: DockLayout {
                            
                        }
                        ImageView {
                            verticalAlignment: VerticalAlignment.Bottom
                            horizontalAlignment: HorizontalAlignment.Center
                            preferredWidth: 768
                            preferredHeight: 1024 + 58
                            imageSource: "asset:///images/help.png"
                            onTouch: {
                                if (event.isUp()) {
                                    app.closeHelp();
                                }
                            }
                        }
//                        Container {
//                            verticalAlignment: VerticalAlignment.Bottom
//                            horizontalAlignment: HorizontalAlignment.Center
//                            preferredWidth: 768
//                            preferredHeight: 1024 + 58
//                            background: Color.Black
//                            opacity: 0.8
//                            layout: AbsoluteLayout {
//
//                            }
//                            onTouch: {
//                                if (event.isUp()) {
//                                    app.closeHelp();
//                                }
//                            }
//                        }
                        
                    }

                    Container {
                        visible: false
                        objectName: "loaderContainer"
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                        ActivityIndicator {
                            //                    objectName: "loader"
                            running: true
                            preferredHeight: 300
                            preferredWidth: 300
                            horizontalAlignment: HorizontalAlignment.Center
                        }
                        Label {
                            objectName: "loaderLabel"
                            textStyle.color: Color.White
                            textStyle.fontSize: FontSize.Medium
                            textStyle.textAlign: TextAlign.Center
                        }
                    }

                }

                Container {
                    id: subActionContainer
                    property bool isShow: false
                    layoutProperties: AbsoluteLayoutProperties {
                        positionX: 0
                        positionY: 1140 //bigContainer.allHeight
                    }
                    //            translationY: 100
                    preferredHeight: 140
                    preferredWidth: 768
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    background: Color.create("#262626")

                    Container {
                        id: swCamContainer
                        topPadding: 10
                        leftPadding: 10
                        rightPadding: 10
                        bottomPadding: 10
                        preferredHeight: subActionContainer.preferredHeight
                        preferredWidth: subActionContainer.preferredWidth / 2
                        layout: DockLayout {
                        }
                        onTouch: {
                            if (event.isDown()) {
                                swCamContainerBlack.opacity = 0;
                            } else if (event.isUp()) {
                                swCamContainerBlack.opacity = 1;
                                app.switchCamera();
                            }
                        }
                        Container {
                            id: swCamContainerBlue
                            background: Color.create("#0053a1")
                            preferredHeight: swCamContainer.preferredHeight - 20
                            preferredWidth: swCamContainer.preferredWidth - 20
                            layout: DockLayout {
                            }
                            Container {
                                id: swCamContainerBlack
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                preferredHeight: swCamContainerBlue.preferredHeight - 6
                                preferredWidth: swCamContainerBlue.preferredWidth - 6
                                background: Color.create("#262626")
                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                ImageView {
                                    verticalAlignment: VerticalAlignment.Center
                                    imageSource: "asset:///images/icon/ic_swcam.png"
                                    rightMargin: 20
                                }
                                Label {
                                    verticalAlignment: VerticalAlignment.Center
                                    text: "Switch Camera"
                                    textStyle.color: Color.White
                                    textStyle.fontSize: FontSize.Small
                                }
                            }
                        }

                    }

                    Container {
                        id: flashContainer
                        topPadding: 10
                        leftPadding: 10
                        rightPadding: 10
                        bottomPadding: 10
                        preferredHeight: subActionContainer.preferredHeight
                        preferredWidth: subActionContainer.preferredWidth / 2
                        layout: DockLayout {
                        }
                        property bool flashOn: true
                        onTouch: {
                            if (event.isDown()) {
                                flashContainerBlack.opacity = 0;
                            } else if (event.isUp()) {
                                flashContainerBlack.opacity = 1;
                                if (flashOn) {
                                    flashLabel.text = "Flash: OFF";
                                    flashOn = false;
                                } else {
                                    flashLabel.text = "Flash: ON";
                                    flashOn = true;
                                }
                                app.setFlashCamera(flashOn);
                            }
                        }
                        Container {
                            id: flashContainerBlue
                            background: Color.create("#0053a1")
                            preferredHeight: flashContainer.preferredHeight - 20
                            preferredWidth: flashContainer.preferredWidth - 20
                            layout: DockLayout {
                            }
                            Container {
                                id: flashContainerBlack
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                preferredHeight: flashContainerBlue.preferredHeight - 6
                                preferredWidth: flashContainerBlue.preferredWidth - 6
                                background: Color.create("#262626")
                            }
                            Container {
                                verticalAlignment: VerticalAlignment.Center
                                horizontalAlignment: HorizontalAlignment.Center
                                layout: StackLayout {
                                    orientation: LayoutOrientation.LeftToRight
                                }
                                ImageView {
                                    verticalAlignment: VerticalAlignment.Center
                                    imageSource: "asset:///images/icon/ic_flash_on.png"
                                    rightMargin: 20
                                }
                                Label {
                                    id: flashLabel
                                    verticalAlignment: VerticalAlignment.Center
                                    text: "Flash: ON"
                                    textStyle.color: Color.White
                                    textStyle.fontSize: FontSize.Small
                                }
                            }
                        }

                    }

                    attachedObjects: [
                        ImplicitAnimationController {
                            propertyName: "translationY"
                            enabled: true
                        }
                    ]

                    function showSubAction() {
                        translationY -= subActionContainer.preferredHeight;
                        isShow = true;
                    }

                    function hideSubAction() {
                        translationY += subActionContainer.preferredHeight;
                        isShow = false;
                    }

                    //            animations: [
                    //                TranslateTransition {
                    //                    id: showSubAction
                    //                    toY: bigContainer.allHeight-subActionContainer.preferredHeight
                    //                    duration: 500
                    //                    onEnded: {
                    //                        subActionContainer.isShow = true;
                    //                    }
                    //                },
                    //                TranslateTransition {
                    //                    id: hideSubAction
                    //                    toY: bigContainer.allHeight
                    //                    duration: 500
                    //                    onEnded: {
                    //                        subActionContainer.isShow = false;
                    //                    }
                    //                } // TranslateTransition
                    //            ] // animations
                }

            }

            actions: [

                // height 140

                // Add an action to enable multiple selection mode. This action
                // appears in the action menu on the main Page of the app.
                //        ActionItem {
                //            title: qsTr("Setting")
                //            imageSource: "asset:///images/icon/ic_camera.png"
                //            ActionBar.placement: ActionBarPlacement.OnBar
                //            onTriggered: {
                //                if (! subActionContainer.isShow) {
                //                    //                    showSubAction.play();
                //                    subActionContainer.showSubAction();
                //                } else {
                //                    //                    hideSubAction.play();
                //                    subActionContainer.hideSubAction();
                //                }
                //                //                app.switchCamera();
                //            }
                //        },
                //                ActionItem {
                //                    title: qsTr("Decorate")
                //                    imageSource: "asset:///images/icon/ic_previous.png"
                //                    ActionBar.placement: ActionBarPlacement.OnBar
                //                    onTriggered: {
                //        //                app.refreshDesign(false);
                //                    }
                //                },
                ActionItem {
                    id: openCameraAction
                    title: qsTr("Open Camera")
                    property bool isEnable: false
                    imageSource: "asset:///images/icon/ic_camera.png"
                    ActionBar.placement: ActionBarPlacement.InOverflow
                    enabled: isEnable
                    onTriggered: {
                        if (! app.isShowHelp()) {
                            app.restartCamera();
                        }

                    }
                    onCreationCompleted: {
                        app.cameraModeChanged.connect(openCameraAction.onCameraModeChanged);
                    }
                    function onCameraModeChanged(mode) {
                        openCameraAction.isEnable = ! mode;
                    }
                },
                ActionItem {
                    id: swCameraAction
                    title: qsTr("Switch Camera")
                    imageSource: "asset:///images/icon/ic_swcam.png"
                    ActionBar.placement: ActionBarPlacement.InOverflow
                    onTriggered: {
                        if (! app.isShowHelp()) {
                            app.switchCamera();
                        }
                    }
                    property bool isEnable: true
                    enabled: isEnable
                    onCreationCompleted: {
                        app.cameraModeChanged.connect(swCameraAction.onCameraModeChanged);
                    }
                    function onCameraModeChanged(mode) {
                        swCameraAction.isEnable = mode;
                    }
                },
                ActionItem {
                    id: actionFlash
                    title: qsTr("Flash: ON")
                    property bool flashOn: true
                    imageSource: "asset:///images/icon/ic_flash_on.png"
                    ActionBar.placement: ActionBarPlacement.InOverflow
                    onTriggered: {
                        if (! app.isShowHelp()) {
                            if (flashOn) {
                                actionFlash.title = "Flash: OFF";
                                imageSource = "asset:///images/icon/ic_flash_off.png"
                                flashOn = false;
                            } else {
                                actionFlash.title = "Flash: ON";
                                imageSource = "asset:///images/icon/ic_flash_on.png"
                                flashOn = true;
                            }
                            app.setFlashCamera(flashOn);
                        }
                    }
                    property bool isEnable: true
                    enabled: isEnable
                    onCreationCompleted: {
                        app.cameraModeChanged.connect(actionFlash.onCameraModeChanged);
                    }
                    function onCameraModeChanged(mode) {
                        actionFlash.isEnable = mode;
                    }
                },
                ActionItem {
                    title: qsTr("Location")
                    imageSource: "asset:///images/icon/ic_map.png"
                    ActionBar.placement: ActionBarPlacement.OnBar
                    onTriggered: {
                        if (! app.isShowHelp()) {
                            if (app.loadComplete()) {
                                locationSheet.open();
                            } else {
                                //show toast please wait a minute
                                toast.show()
                            }
                        }
                    }
                },
                ActionItem {
                    title: qsTr("Capture!")
                    imageSource: "asset:///images/icon/ic_lens_color.png"
                    ActionBar.placement: ActionBarPlacement.OnBar
                    onTriggered: {
                        if (! app.isShowHelp()) {
                            if (app.cameraMode) {
                                console.log("app.cameraMode = true");
                                if (app.loadComplete()) {
                                    app.setDesignIndex(mainScroll.indexFilter);
                                    camera.capturePhoto();
                                } else {
                                    //show toast please wait a minute
                                    toast.show()
                                }
                            } else {
                                console.log("app.cameraMode = false");
                                if (app.loadComplete()) {
                                    app.setDesignIndex(mainScroll.indexFilter);
                                    app.saveImage();
                                } else {
                                    //show toast please wait a minute
                                    toast.show()
                                }
                            }
                        }
                    }
                    attachedObjects: [
                        SystemToast {
                            id: toast
                            body: "Please wait a minute."
                        }
                    ]
                },
                ActionItem {
                    title: qsTr("Gallery")
                    imageSource: "asset:///images/icon/ic_gallery.png"
                    ActionBar.placement: ActionBarPlacement.OnBar
                    onTriggered: {
                        if (! app.isShowHelp()) {
                            picker.open();
                        }
                    }
                },
                ActionItem {
                    title: qsTr("Invite Friends")
                    imageSource: "asset:///images/icon/ic_bbm.png"
                    onTriggered: {
                        _inviteToDownload.sendInvite();
                    }
                },
                //                ActionItem {
                //                    title: qsTr("View Fanpage")
                //                    imageSource: "asset:///images/icon/ic_facebook.png"
                //                    onTriggered: {
                //                        app.openFBPage();
                //                    }
                //                },
                ActionItem {
                    title: qsTr("Rate this App!")
                    imageSource: "asset:///images/icon/ic_like.png"
                    onTriggered: {
                        app.openAppWorld();
                    }
                }
            //                ,
            //                ActionItem {
            //                    title: qsTr("Buy Pro Version!")
            //                    imageSource: "asset:///images/icon/ic_star.png"
            //                    onTriggered: {
            //                        app.openAppWorldPro();
            //                    }
            //                }

            ]
            attachedObjects: [
                LocationSheet {
                    id: locationSheet
                },
                //        ComponentDefinition {
                //            id: cropSheet
                //            source: "CropSheet.qml"
                //        },
                FilePicker {
                    id: picker

                    property string selectedFile

                    title: qsTr("Choose Your Image")
                    mode: FilePickerMode.Picker
                    type: FileType.Picture
                    viewMode: FilePickerViewMode.GridView
                    sortBy: FilePickerSortFlag.Default
                    sortOrder: FilePickerSortOrder.Default

                    onFileSelected: {
                        //                selectedFile = selectedFiles[0]
                        app.setSelectedFilePath(selectedFiles[0]);
                        //show crop
                        //                var sheet = cropSheet.createObject();
                        //                sheet.open();
                    }
                    onPickerOpened: {
                        app.closeCamera();
                    }
                    onCanceled: {
                        app.restartCamera();
                    }
                }
            //        TutorialDialog{
            //            id: tutorialDialog
            //        }
            ]

            onCreationCompleted: {
                //        app.startUpdateWeather();
                if (app.isShowTutorial()) {
                    console.log("Show Tutorial");

                    //            var d = tutorialDialog.createObject();
                    //            d.open();
                    //            tutorialDialog.open();
                } else {
                    console.log("Not Show Tutorial");
                }
            }

        }
    }
    //    Tab {
    //        title: "My Account"
    //        imageSource: "asset:///images/icon/ic_user.png"
    //        Page {
    //            Container {
    //
    //            }
    //        }
    //    }
    //    Tab {
    //        title: "Facebook Fanpage"
    //        imageSource: "asset:///images/icon/ic_facebook.png"
    //        Page {
    //            Container {
    //
    //                WebView {
    //                    url: "https://m.facebook.com/bbdevthailand"
    //                }
    //            }
    //        }
    //    }
}
