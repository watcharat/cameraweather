
import bb.cascades 1.0

Dialog {
    id: dialog
    Container {
        layout: DockLayout {

        }

        Container {
            id: step1Container
            visible: false
            animations: [
                FadeTransition {
                    id: step1FadeOut
                    duration: 500
                    easingCurve: StockCurve.QuarticOut
                    fromOpacity: 1.0
                    toOpacity: 0.0
                    onEnded: {
                        step1Container.visible = false;
                    }
                }
            ]
            preferredWidth: 768
            preferredHeight: 1280
            background: Color.create(0.0, 0.0, 0.0, 0.5)
            layout: DockLayout {
            }
            Container {
                topPadding: 50
                leftPadding: 50
                bottomPadding: 50
                rightPadding: 50
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                background: Color.Black

                Label {
                    text: "Welcome to Camera Weather!"
                    horizontalAlignment: HorizontalAlignment.Center
                    //                verticalAlignment: VerticalAlignment.Top
                    textStyle.color: Color.Yellow
                    textStyle.fontFamily: "Helvectica"
                    textStyle.fontSize: FontSize.Large
                    textStyle.fontWeight: FontWeight.Bold
                }

                Label {
                    text: "                Each transition includes from and to properties that specify the initial and final values of the animated property. You should check the API reference for the transition that you're interested in to determine the names of the from and to properties."
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    multiline: true
                    textStyle.color: Color.White
                    textStyle.fontSize: FontSize.Small
                }

                Label {
                    opacity: 0.0
                    topMargin: 30
                    text: "< Swipe left to continue >"
                    textStyle.color: Color.Cyan
                    horizontalAlignment: HorizontalAlignment.Center
                    animations: [
                        ParallelAnimation {
                            id: swipeLeftAnimation
                            animations: [
                                FadeTransition {
                                    duration: 300
                                    easingCurve: StockCurve.QuarticOut
                                    fromOpacity: 0.0
                                    toOpacity: 1.0
                                },
                                FadeTransition {
                                    duration: 300
                                    easingCurve: StockCurve.QuarticOut
                                    fromOpacity: 1.0
                                    toOpacity: 0.0
                                }
                            ]
                            onEnded: {
                                swipeLeftAnimation.play();
                            }
                        }
                    ]
                    attachedObjects: [
                        QTimer {
                            id: timer
                            interval: 5000
                            onTimeout: {
                                console.log("----------- TIMER TIMEOUT -----");
                                swipeLeftAnimation.play();
                            }
                        }
                    ]
                    onCreationCompleted: {
                        timer.start();
                    }
                }

                property real xA: 0.0
                property real xB: 0.0
                onTouch: {
                    if (event.isDown()) {
                        xA = event.windowX;
                    } else if (event.isUp()) {
                        xB = event.windowX;
                        var dX = xB - xA;
                        if (dX < 0) {
                            if (dX < -50) {
                                //move left
                                step1FadeOut.play();
                                step2FadeIn.play();
                            }
                        }
                    }
                }

                animations: [
                    ScaleTransition {
                        id: scaleAnimation
                        toX: 1.0
                        toY: 1.0
                        duration: 2000
                        easingCurve: StockCurve.DoubleElasticOut
                    }
                ]
                onCreationCompleted: {
                    scaleAnimation.play();
                }
            }
        }

        Container {
            id: step2Container
            visible: true

            //            opacity: 0.0
            animations: [
                FadeTransition {
                    id: step2FadeIn
                    duration: 500
                    easingCurve: StockCurve.QuarticOut
                    fromOpacity: 0.0
                    toOpacity: 1.0
                    onStarted: {
                        step2Container.visible = true;
                    }
                },
                FadeTransition {
                    id: step2FadeOut
                    duration: 500
                    easingCurve: StockCurve.QuarticOut
                    fromOpacity: 1.0
                    toOpacity: 0.0
                    onEnded: {
                        step2Container.visible = false;
                    }
                }
            ]
            preferredWidth: 768
            preferredHeight: 1280

            layout: DockLayout {
            }
            Container {
                background: Color.create(0.0, 0.0, 0.0, 0.8)
                preferredWidth: 768
                preferredHeight: 1140
            }

            Label {
                opacity: 0.0
                topMargin: 30
                text: "< Swipe left to continue >"
                textStyle.color: Color.Cyan
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                animations: [
                    ParallelAnimation {
                        id: swipeLeftAnimation2
                        animations: [
                            FadeTransition {
                                duration: 300
                                easingCurve: StockCurve.QuarticOut
                                fromOpacity: 0.0
                                toOpacity: 1.0
                            },
                            FadeTransition {
                                duration: 300
                                easingCurve: StockCurve.QuarticOut
                                fromOpacity: 1.0
                                toOpacity: 0.0
                            }
                        ]
                        onEnded: {
                            swipeLeftAnimation2.play();
                        }
                    }
                ]
                attachedObjects: [
                    QTimer {
                        id: timer2
                        interval: 5000
                        onTimeout: {
                            console.log("----------- TIMER TIMEOUT -----");
                            swipeLeftAnimation2.play();
                        }
                    }
                ]
                onCreationCompleted: {
                    timer2.start();
                }
            }
            property real xA: 0.0
            property real xB: 0.0
            onTouch: {
                if (event.isDown()) {
                    xA = event.windowX;
                } else if (event.isUp()) {
                    xB = event.windowX;
                    var dX = xB - xA;
                    if (dX < 0) {
                        if (dX < -50) {
                            //move left
//                            step1FadeOut.play();
//                            step2FadeIn.play();

							dialog.close();
                        }
                    }
                }
            }
        }
    }

}
