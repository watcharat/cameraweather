// Default empty project template
import bb.cascades 1.0
import bb.cascades.multimedia 1.0
import bb.system 1.0

// creates one page with a label
Page {
    Container {
        layout: DockLayout {
            
        }
        Container {
            verticalAlignment: VerticalAlignment.Top
//            preferredHeight: 1140
//            preferredWidth: 768
            layout: DockLayout {

            }

            // CAMERA ZONE
            Camera {
                id: camera
                objectName: "myCamera"
                //                onTouch: {
                //                    if (event.isDown()) {
                //                        // Take photo
                //                        capturePhoto();
                //                    }
                //                }

                // When the camera is opened we want to start the viewfinder
                onCameraOpened: {
                    camera.startViewfinder();
                }

                // There are loads of messages we could listen to here.
                // onPhotoSaved and onShutterFired are taken care of in the C++ code.
                onCameraOpenFailed: {
                    console.log("onCameraOpenFailed signal received with error " + error);
                }
                onViewfinderStartFailed: {
                    console.log("viewfinderStartFailed signal received with error " + error);
                }
                onViewfinderStopFailed: {
                    console.log("viewfinderStopFailed signal received with error " + error);
                }
                onPhotoCaptureFailed: {
                    console.log("photoCaptureFailed signal received with error " + error);
                }
                onPhotoSaveFailed: {
                    console.log("photoSaveFailed signal received with error " + error);
                }
                onPhotoSaved: {
                    app.manipulatePhoto(fileName);
                }
                onCameraResourceReleased: {
                    //screen dim
                    //                	console.log("--- onCameraResourceReleased");
                }
                onCameraResourceAvailable: {
                    //                    console.log("--- onCameraResourceAvailable");
                    app.restartCamera();
                }
            }

            Container {
                verticalAlignment: VerticalAlignment.Top
                preferredHeight: header.preferredHeight + ads.preferredHeight
                preferredWidth: 768
                Container {
                    //top
                    id: header
                    preferredHeight: 108//75+33
                    preferredWidth: 768
                    background: bg_header.imagePaint
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    attachedObjects: [
                        ImagePaintDefinition {
                            id: bg_header
                            imageSource: "asset:///images/bg_header.png"
                        }
                    ]
                    ImageView {
                        preferredHeight: header.preferredHeight
                        preferredWidth: header.preferredHeight
                        imageSource: "asset:///images/icon_programe.png"
                    }
                    Label {
                        verticalAlignment: VerticalAlignment.Center
                        text: "Camera Weather"
                        textStyle.color: Color.White
                        textStyle.fontWeight: FontWeight.Bold
                        textStyle.fontSize: FontSize.Large
                    }
                }
                Container {
                    //top
                    id: ads
                    preferredHeight: 90
                    preferredWidth: 768
                    background: Color.Black
                    layout: DockLayout {

                    }
                    Label {
                        text: "Advertising Here!"
                        textStyle.color: Color.White
                        verticalAlignment: VerticalAlignment.Center
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                }
                //MAIN SCROLL
                Container {
                    id: mainScroll
                    //                topMargin: 75 + 90
                    verticalAlignment: VerticalAlignment.Top
                    horizontalAlignment: HorizontalAlignment.Center
                    preferredWidth: 768
                    preferredHeight: 768

                    property int indexFilter: 0
                    property real xA: 0.0
                    property real xB: 0.0
                    onTouch: {
                        if (event.isDown()) {
                            xA = event.windowX;
                            //                        console.log("DOWN xA: " + xA);
                        } else if (event.isUp()) {
                            //                        console.log("UP");
                            xB = event.windowX;
                            //                        console.log("xB: " + xB);
                            var dX = xB - xA;
                            //                        console.log("dX: " + dX);
                            if (dX < 0) {
                                if (dX < -50) {
                                    //move left  >> set scroll up
                                    indexFilter ++;
                                    if (indexFilter > 9) {
                                        indexFilter = 9;
                                    }
                                    scrollView.scrollToPoint(768 * indexFilter, 0.0, ScrollAnimation.Default);
                                }
                            } else {
                                if (dX > 50) {
                                    //move right  >> set scroll down
                                    indexFilter --;
                                    if (indexFilter < 0) {
                                        indexFilter = 0;
                                    }
                                    scrollView.scrollToPoint(768 * indexFilter, 0.0, ScrollAnimation.Default);
                                }
                            }
                            console.log("indexFilter = " + indexFilter);

                            p1.visible = false;
                            p2.visible = false;
                            p3.visible = false;
                            p4.visible = false;
                            p5.visible = false;
                            p6.visible = false;
                            p7.visible = false;
                            p8.visible = false;
                            p9.visible = false;
                            p10.visible = false;

                            if (indexFilter == 0) {
                                p1.visible = true;
                            } else if (indexFilter == 1) {
                                p2.visible = true;
                            } else if (indexFilter == 2) {
                                p3.visible = true;
                            } else if (indexFilter == 3) {
                                p4.visible = true;
                            } else if (indexFilter == 4) {
                                p5.visible = true;
                            } else if (indexFilter == 5) {
                                p6.visible = true;
                            } else if (indexFilter == 6) {
                                p7.visible = true;
                            } else if (indexFilter == 7) {
                                p8.visible = true;
                            } else if (indexFilter == 8) {
                                p9.visible = true;
                            } else if (indexFilter == 9) {
                                p10.visible = true;
                            }
                        }
                    }

                    ScrollView {
                        id: scrollView
                        Container {

                            layout: StackLayout {
                                orientation: LayoutOrientation.LeftToRight
                            }
                            //MAIN CONTAINER
                            Container {
                                objectName: "mainContainer"
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //                            horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer2"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer3"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer4"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer5"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer6"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer7"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer8"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer9"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }

                            Container {
                                objectName: "mainContainer10"
                                //                            visible: false
                                preferredWidth: 768
                                preferredHeight: mainScroll.preferredHeight
                                //verticalAlignment: VerticalAlignment.Fill
                                //horizontalAlignment: HorizontalAlignment.Fill
                                layout: AbsoluteLayout {

                                }
                            }
                        }
                        scrollViewProperties {
                            scrollMode: ScrollMode.Horizontal
                        }
                    }

                }

                

            }

            //END CAMERA

            Container {
                visible: false
                objectName: "loaderContainer"
                verticalAlignment: VerticalAlignment.Center
                horizontalAlignment: HorizontalAlignment.Center
                ActivityIndicator {
                    //                    objectName: "loader"
                    running: true
                    preferredHeight: 400
                    preferredWidth: 400
                    horizontalAlignment: HorizontalAlignment.Center
                }
                Label {
                    objectName: "loaderLabel"
                    textStyle.color: Color.White
                    textStyle.fontSize: FontSize.Medium
                    textStyle.textAlign: TextAlign.Center
                }
            }

        }

		Container {
      		preferredWidth: 768
      		verticalAlignment: VerticalAlignment.Bottom

            Container {
                //bottom
                id: pagingContainer
                verticalAlignment: VerticalAlignment.Bottom
                preferredHeight: 58
                preferredWidth: 768
                background: Color.Black
                layout: DockLayout {

                }
                Container {
                    layout: StackLayout {
                        orientation: LayoutOrientation.LeftToRight
                    }
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }
                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p1
                            visible: true
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }
                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p2
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }

                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p3
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }

                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p4
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }

                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p5
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }

                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p6
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }

                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p7
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }

                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p8
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }

                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p9
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                    Container {
                        leftMargin: 20
                        rightMargin: 20
                        layout: DockLayout {
                        }

                        ImageView {
                            imageSource: "asset:///images/page_unfocus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                        ImageView {
                            id: p10
                            visible: false
                            imageSource: "asset:///images/page_focus.png"
                            preferredHeight: 20
                            preferredWidth: 20
                        }
                    }
                }
            }
            Container {
                id: menu
                preferredHeight: 256
                preferredWidth: 768
                verticalAlignment: VerticalAlignment.Bottom
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                Container {
                    preferredHeight: menu.preferredHeight
                    preferredWidth: menu.preferredHeight
                    background: Color.Red
                }
                Container {
                    preferredHeight: menu.preferredHeight
                    preferredWidth: menu.preferredHeight
                    background: Color.Green
                }
                Container {
                    preferredHeight: menu.preferredHeight
                    preferredWidth: menu.preferredHeight
                    background: Color.Blue
                }
            }
        }

    }
    //    actions: [
    //
    //        // height 140
    //
    //        // Add an action to enable multiple selection mode. This action
    //        // appears in the action menu on the main Page of the app.
    //        ActionItem {
    //            title: qsTr("Setting")
    //            imageSource: "asset:///images/icon/ic_camera.png"
    //            ActionBar.placement: ActionBarPlacement.OnBar
    //            onTriggered: {
    //                if (! subActionContainer.isShow) {
    //                    //                    showSubAction.play();
    //                    subActionContainer.showSubAction();
    //                } else {
    //                    //                    hideSubAction.play();
    //                    subActionContainer.hideSubAction();
    //                }
    //                //                app.switchCamera();
    //            }
    //        },
    //        //        ActionItem {
    //        //            title: qsTr("Decorate")
    //        //            imageSource: "asset:///images/icon/ic_previous.png"
    //        //            ActionBar.placement: ActionBarPlacement.OnBar
    //        //            onTriggered: {
    //        ////                app.refreshDesign(false);
    //        //            }
    //        //        },
    //        ActionItem {
    //            title: qsTr("Capture!")
    //            imageSource: "asset:///images/icon/ic_lens.png"
    //            ActionBar.placement: ActionBarPlacement.OnBar
    //            onTriggered: {
    //                if (app.loadComplete()) {
    //                    app.setDesignIndex(mainScroll.indexFilter);
    //                    camera.capturePhoto();
    //                } else {
    //                    //show toast please wait a minute
    //                    toast.show()
    //                }
    //            }
    //            attachedObjects: [
    //                SystemToast {
    //                    id: toast
    //                    body: "Please wait a minute. Application is rendering an image now."
    //                }
    //            ]
    //        },
    //        //        ActionItem {
    //        //            title: qsTr("Gallery")
    //        //            imageSource: "asset:///images/icon/ic_gallery.png"
    //        //            ActionBar.placement: ActionBarPlacement.OnBar
    //        //            onTriggered: {
    //        ////                app.refreshDesign(true);
    //        //            }
    //        //        },
    //        ActionItem {
    //            title: qsTr("Location")
    //            imageSource: "asset:///images/icon/ic_earth.png"
    //            ActionBar.placement: ActionBarPlacement.OnBar
    //            onTriggered: {
    //                locationSheet.open();
    //            }
    //        }
    //    ]
    attachedObjects: [
        LocationSheet {
            id: locationSheet
        }
    ]
}
