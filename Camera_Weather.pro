APP_NAME = Camera_Weather

CONFIG += qt warn_on cascades10
QT += network
LIBS += -lbbcascadespickers -lbbcascadesmultimedia -lbbsystem -lQtLocationSubset -lbbdata -lscreen -lcrypto -lcurl -lpackageinfo -lbbdevice -lbbcascadesadvertisement -lbb -lbbplatformbbm

include(config.pri)

device {
    CONFIG(debug, debug|release) {
        # Device-Debug custom configuration
    }

    CONFIG(release, debug|release) {
        # Device-Release custom configuration
    }
}

simulator {
    CONFIG(debug, debug|release) {
        # Simulator-Debug custom configuration
    }
}
