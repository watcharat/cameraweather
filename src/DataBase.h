/*
 * DataBase.h
 *
 *  Created on: Jan 18, 2013
 *      Author: watcharatsuwannarat
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <QtSql/QtSql>
#include <QVariantList>
#include <bb/data/SqlDataAccess>
#include "LocationObject.h"
#include "ForecastObject.h"

using namespace bb::data;

class DataBase {

private:
	DataBase();
	static DataBase* _instance;
	bool filterLocation(LocationObject locationObject);


public:
	SqlDataAccess *sqlDataAccess;
	static DataBase* getInstance();
	virtual ~DataBase();
	QVariantList execSelect(const QString query);
	void execInsertUpdateDelete(const QString query);

	bool copyDbToDataFolder(const QString databaseName);
	QSqlDatabase mDb;
	QString mDbNameWithPath;

	void insertOrUpdateLocation(LocationObject locationObject);
	void insertForecast(ForecastObject forecastObject);
	bool updateLocationByCreateTime(QString createtime, QString newName, QString newDesc);

	QVariantList getAllWeatherData();
	QVariantList getAllLocation();
	QVariantList getLocationByLatLon(double latitude, double longitude);
	QVariantList getForecastByTime(QString time);

};

#endif /* DATABASE_H_ */
