/*
 * ForecastObject.h
 *
 *  Created on: Jun 19, 2013
 *      Author: watcharatsuwannarat
 */

#ifndef FORECASTOBJECT_H_
#define FORECASTOBJECT_H_

#include <QString>

class ForecastObject {
public:
	ForecastObject();
	~ForecastObject();

	QString getDate() const;
    void setDate(QString date);
    QString getPrecipMm() const;
    void setPrecipMm(QString precipMm);
    QString getTempMaxC() const;
    void setTempMaxC(QString tempMaxC);
    QString getTempMaxF() const;
    void setTempMaxF(QString tempMaxF);
    QString getTempMinC() const;
    void setTempMinC(QString tempMinC);
    QString getTempMinF() const;
    void setTempMinF(QString tempMinF);
    QString getTime() const;
    void setTime(QString time);
    QString getWeatherCode() const;
    void setWeatherCode(QString weatherCode);
    QString getWeatherDesc() const;
    void setWeatherDesc(QString weatherDesc);
    QString getWinddir16Point() const;
    void setWinddir16Point(QString winddir16Point);
    QString getWinddirDegree() const;
    void setWinddirDegree(QString winddirDegree);
    QString getWinddirection() const;
    void setWinddirection(QString winddirection);
    QString getWindspeedKmph() const;
    void setWindspeedKmph(QString windspeedKmph);
    QString getWindspeedMiles() const;
    void setWindspeedMiles(QString windspeedMiles);
    QString time;
    QString date;
    QString precipMM;
    QString tempMaxC;
    QString tempMaxF;
    QString tempMinC;
    QString tempMinF;
    QString weatherCode;
    QString weatherDesc;
    QString winddir16Point;
    QString winddirDegree;
    QString winddirection;
    QString windspeedKmph;
    QString windspeedMiles;
};

#endif /* FORECASTOBJECT_H_ */
