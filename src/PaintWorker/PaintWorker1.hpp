/*
 * PaintWorker1.hpp
 *
 *  Created on: Jun 9, 2013
 *      Author: watcharatsuwannarat
 */

#ifndef PAINTWORKER1_HPP_
#define PAINTWORKER1_HPP_

#include <qobject.h>
#include <bb/cascades/Container>
#include <bb/cascades/ImageView>
#include <bb/cascades/Color>
#include <bb/cascades/AbsoluteLayoutProperties>

namespace bb {
namespace cascades {

class PaintWorker1: public QObject {
	Q_OBJECT
public:
	PaintWorker1();
	PaintWorker1(bb::cascades::Container *_mainContainer,float _topY, float _imageWidth,float _imageHeight
			, QString _areaName, QString _tempText, QString _weatherDescValue);
	~PaintWorker1();

	void startRender();

public slots:
  void processRender();
  void processPaint();
  void errorHandler(QString err);
  void onRenderFinished();

signals:
  void finishedRender();
  void errorRender(QString err);

  void finishedPaint();
  void errorPaint(QString err);

private:
  int drawTextImage(int x, int y, int newHeight, QString wording,
  		int maxWidth, QString folderIcon, bb::cascades::Container *container);

//  ApplicationUI app;
  bb::cascades::Container *mainContainer;
  bool isRenderFinished;
  float topY;
  float imageWidth;
  float imageHeight;
  QString areaName;
  QString tempText;
  QString weatherDescValue;

  QString character;
  QString iconfileName;
  QString assetFolder;

  QImageReader reader;
  QImage icon;
  QSize iconSize;


};

} /* namespace cascades */
} /* namespace bb */
#endif /* PAINTWORKER1_HPP_ */
