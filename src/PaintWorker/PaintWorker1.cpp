/*
 * PaintWorker1.cpp
 *
 *  Created on: Jun 9, 2013
 *      Author: watcharatsuwannarat
 */

#include "PaintWorker1.hpp"

namespace bb {
namespace cascades {

PaintWorker1::PaintWorker1(){

}

PaintWorker1::PaintWorker1(bb::cascades::Container *_mainContainer,float _topY, float _imageWidth, float _imageHeight
		, QString _areaName, QString _tempText, QString _weatherDescValue) {
	// TODO Auto-generated constructor stub
//	app = _app;
	mainContainer = _mainContainer;
	topY = _topY;
	imageWidth = _imageWidth;
	imageHeight = _imageHeight;
	areaName = _areaName;
	tempText = _tempText;
	weatherDescValue = _weatherDescValue;
	isRenderFinished = false;
}

PaintWorker1::~PaintWorker1() {
	// TODO Auto-generated destructor stub
}

void PaintWorker1::startRender(){
	qDebug() << "--- START RENDER ----";

	// Create a thread
	QThread* thread = new QThread;
//	Worker* worker = new Worker();

	// Give QThread ownership of Worker Object
	this->moveToThread(thread);

	// Connect worker error signal to this errorHandler SLOT.
	bool res = connect(this, SIGNAL(errorRender(QString)), this, SLOT(errorHandler(QString)));
	if(res){
		qDebug() << "CONNECT errorRender with errorHandler OK";
	}else{
		qDebug() << "CONNECT errorRender with errorHandler FAILED";
	}
//	connect(this, SIGNAL(errorRender(QString)), app, SLOT(onPaintWorker1Error(QString)));

	// Connects the thread�s started() signal to the process() slot in the worker, causing it to start.
	res = connect(thread, SIGNAL(started()), this, SLOT(processRender()));
	if(res){
			qDebug() << "CONNECT started with processRender OK";
		}else{
			qDebug() << "CONNECT started with processRender FAILED";
		}

	// Connect worker finished signal to trigger thread quit, then delete.
	res = connect(this, SIGNAL(finishedRender()), thread, SLOT(quit()));
	if(res){
		qDebug() << "CONNECT finishedRender with quit OK";
	}else{
		qDebug() << "CONNECT finishedRender with quit FAILED";
	}

	res = connect(this, SIGNAL(finishedRender()), this, SLOT(onRenderFinished()));
	if(res){
		qDebug() << "CONNECT finishedRender with onRenderFinished OK";
	}else{
		qDebug() << "CONNECT finishedRender with onRenderFinished FAILED";
	}
//	connect(this, SIGNAL(finishedRender()), app, SLOT(onPaintWorker1Finished()));

	// Make sure the thread object is deleted after execution has finished.
	res = connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	if(res){
		qDebug() << "CONNECT finished with deleteLater OK";
	}else{
		qDebug() << "CONNECT finished with deleteLater FAILED";
	}

	Q_ASSERT(res);
	Q_UNUSED(res);

	thread->start();
}

void PaintWorker1::errorHandler(QString err){
	qDebug() << "errorHandler: "+err;
	isRenderFinished = false;
	mainContainer->removeAll();
	mainContainer->setVisible(true);
}

void PaintWorker1::onRenderFinished(){
	qDebug() << "onRenderFinished";
	isRenderFinished = true;
	mainContainer->setVisible(true);
}

void PaintWorker1::processRender(){
	emit finishedRender();
//	mainContainer->setVisible(false);
//	mainContainer->removeAll();
//	//black layer
//		Container *bg = new Container();
//		bg->setPreferredSize(imageWidth, imageHeight / 2);
//		bg->setBackground(Color::Black);
//		bg->setOpacity(0.2);
//		AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
//		bgProperties->setPositionX(0.0);
//		bgProperties->setPositionY(topY + imageHeight / 2);
//		bg->setLayoutProperties(bgProperties);
//		mainContainer->add(bg);
//		bgProperties = 0;
//		bg = 0;
//
//	//draw area
//		int areaY = topY + imageHeight / 2;
//		int areaHeight = 220;
//		drawTextImage(0, areaY, areaHeight, areaName.toUpper(), 600,
//				"WinterthurConsided", mainContainer);
//
//	//draw temperature
//		int tempY = areaY + areaHeight + 10;
//		int tempHeight = 160;
//		int nextX = drawTextImage(0, tempY, tempHeight, /*(temp_C + "*C")*/tempText.toUpper(),
//				0, "WinterthurConsided", mainContainer);
//
//	//draw country
//		int countryX = nextX + 20;
//		int countryHeight = tempHeight / 2;
//		drawTextImage(countryX, tempY, countryHeight, weatherDescValue.toUpper(), 0,
//				"WinterthurConsided", mainContainer);
//
//	//draw datetime
//		QString m_time = "Fri May 10 05:03:45 2013";
//		int datetimeY = tempY + countryHeight;
//		drawTextImage(countryX, datetimeY, countryHeight, m_time.toUpper(), 0,
//				"WinterthurConsided", mainContainer);
//
//	emit finishedRender();
}

void PaintWorker1::processPaint(){

}

int PaintWorker1::drawTextImage(int x, int y, int newHeight, QString wording,
		int maxWidth, QString folderIcon, bb::cascades::Container *container) {
	qDebug()
			<< "drawTextImage() x:" + QString::number(x) + ", y:"
					+ QString::number(y) + ", newHeight:"
					+ QString::number(newHeight) + ", wording:" + wording
					+ ", maxWidth:" + maxWidth + ", folderIcon:" + folderIcon;

//start position
	int newWidth;

	for (int i = 0; i < wording.length(); ++i) {
		//limited width of
		if (maxWidth > 0 && x > maxWidth) {
			character = "_dot";
			iconfileName = assetFolder + "images/fontimage/" + folderIcon + "/"
					+ character + ".png";

			reader.setFileName(iconfileName);
			icon = reader.read();
			iconSize = icon.size();
			newWidth = (newHeight * iconSize.width()) / iconSize.height();

			for (int j = 0; j < 3; j++) {
				ImageView* imageView = ImageView::create(iconfileName);
				imageView->setPreferredHeight(newHeight);
				imageView->setPreferredWidth(newWidth);

				AbsoluteLayoutProperties* imgProperties =
						AbsoluteLayoutProperties::create();
				imgProperties->setPositionX(x);
				imgProperties->setPositionY(y);
				imageView->setLayoutProperties(imgProperties);

				imgProperties = 0;

				container->add(imageView);
				imageView = 0;

				x += newWidth;
			}
			break;
		}
		character = wording[i];

		if (character == "@") {
			character = "_at";
		} else if (character == ":") {
			character = "_colon";
		} else if (character == ",") {
			character = "_comma";
		} else if (character == "\"") {
			character = "_dbquote";
		} else if (character == ".") {
			character = "_dot";
		} else if (character == "\'") {
			character = "_quote";
		} else if (character == ";") {
			character = "_semicolon";
		} else if (character == "#") {
			character = "_sharp";
		} else if (character == "/") {
			character = "_sl";
		} else if (character == " ") {
			character = "_space";
		} else if (character == "*") {
			character = "_temp";
		}
		iconfileName = assetFolder + "images/fontimage/" + folderIcon + "/"
				+ character + ".png";

		reader.setFileName(iconfileName);
		icon = reader.read();
		iconSize = icon.size();
		newWidth = (newHeight * iconSize.width()) / iconSize.height();

		ImageView* imageView = ImageView::create(iconfileName);
		imageView->setPreferredHeight(newHeight);
		imageView->setPreferredWidth(newWidth);

		AbsoluteLayoutProperties* imgProperties =
				AbsoluteLayoutProperties::create();
		imgProperties->setPositionX(x);
		imgProperties->setPositionY(y);
		imageView->setLayoutProperties(imgProperties);
		imgProperties = 0;

		container->add(imageView);
		imageView = 0;

		x += newWidth;
	}

	return x;
}

} /* namespace cascades */
} /* namespace bb */
