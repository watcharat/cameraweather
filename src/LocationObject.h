/*
 * LocationObject.h
 *
 *  Created on: May 12, 2013
 *      Author: watcharatsuwannarat
 */

#ifndef LOCATIONOBJECT_H_
#define LOCATIONOBJECT_H_

#include <QString>

class LocationObject {
public:
	LocationObject();
	virtual ~LocationObject();
    QString getAreaName() const;
	void setAreaName(QString areaName);
	QString getCountry() const;
	void setCountry(QString country);
	double getLatitude() const;
	void setLatitude(double latitude);
	double getLongitude() const;
	void setLongitude(double longitude);
	QString getRegion() const;
	void setRegion(QString region);
	QString getCountry2() const;
    void setCountry2(QString country2);
    QString getIsUserLocation() const;
    void setIsUserLocation(QString isUserLocation);
    QString getCreatetime() const;
	void setCreatetime(QString createtime);
	QString areaName;
    QString country;
    double latitude;
    double longitude;
    QString region;
    QString country2;
    QString isUserLocation;
    QString createtime;
};
#endif /* LOCATIONOBJECT_H_ */
