/*
 * DataBase.cpp
 *
 *  Created on: Jan 18, 2013
 *      Author: watcharatsuwannarat
 */

#include "DataBase.h"

DataBase* DataBase::_instance;

DataBase::DataBase() {
	// TODO Auto-generated constructor stub
}

DataBase::~DataBase() {
	// TODO Auto-generated destructor stub
	if (mDb.isOpen()) {
		QSqlDatabase::removeDatabase(mDbNameWithPath);
		mDb.removeDatabase("QSQLITE");
	}
	_instance = 0;
}

DataBase* DataBase::getInstance() {
	if (_instance == 0) {
		_instance = new DataBase;
	}
	return _instance;
}

bool DataBase::copyDbToDataFolder(const QString databaseName) {
	qDebug() << "copyDbToDataFolder() databaseName: " + databaseName;
	QString dataFolder = QDir::homePath();
	QString newFileName = dataFolder + "/" + databaseName;
	QFile newFile(newFileName);

	mDbNameWithPath = "data/" + databaseName;
	mDb = QSqlDatabase::addDatabase("QSQLITE", "database_helper_connection");
	mDb.setDatabaseName(mDbNameWithPath);

	sqlDataAccess = new SqlDataAccess(mDbNameWithPath);

	if (!newFile.exists()) {
		qDebug() << "!newFile.exists()";
		QString appFolder(QDir::homePath());
		appFolder.chop(4);
		QString originalFileName = appFolder + "app/native/assets/sql/"
				+ databaseName;
		QFile originalFile(originalFileName);

		if (originalFile.exists()) {
			qDebug() << "originalFile.exists()";
			return originalFile.copy(newFileName);
		} else {
			qDebug() << "Failed to copy file data base file does not exists.";
			return false;
		}
	} else {
		qDebug() << "newFile.exists()";
	}

	return true;
}

QVariantList DataBase::execSelect(const QString query) {
	qDebug() << "execSelect(): " + query;
	QVariantList sqlData;
//	SqlDataAccess sqlDataAccess(mDbNameWithPath);
	sqlData = sqlDataAccess->execute(query).value<QVariantList>();
	if (sqlDataAccess->hasError()) {
		DataAccessError err = sqlDataAccess->error();
		qWarning() << "SQL error: type=" << err.errorType() << ": "
				<< err.errorMessage();
		return sqlData;
	}

	if (!mDb.isValid()) {
		qWarning()
				<< "Could not set data base name probably due to invalid driver.";
		return sqlData;
	}

	return sqlData;
}

void DataBase::execInsertUpdateDelete(const QString query) {
	qDebug() << "execInsertUpdateDelete(): " + query;
//	SqlDataAccess sqlDataAccess(mDbNameWithPath);
	sqlDataAccess->execute(query);
}

bool DataBase::filterLocation(LocationObject locationObject) {
	QString name = locationObject.getAreaName().toUpper();
	qDebug() << "name: " + name;
	bool notFound = false;
	for (int i = 0; i < name.length(); i++) {
		qDebug() << "name[" + QString::number(i) + "] = " + name.at(i);

		 // =-+*/:;'"()#$!?@~`[]{}<>^%_÷±•\|¤«»¥€£$¡¿&
		if (name.at(i) == 'A' || name.at(i) == 'B' || name.at(i) == 'C'
				|| name.at(i) == 'D' || name.at(i) == 'E' || name.at(i) == 'F'
				|| name.at(i) == 'G' || name.at(i) == 'H' || name.at(i) == 'I'
				|| name.at(i) == 'J' || name.at(i) == 'K' || name.at(i) == 'L'
				|| name.at(i) == 'M' || name.at(i) == 'N' || name.at(i) == 'O'
				|| name.at(i) == 'P' || name.at(i) == 'Q' || name.at(i) == 'R'
				|| name.at(i) == 'S' || name.at(i) == 'T' || name.at(i) == 'U'
				|| name.at(i) == 'V' || name.at(i) == 'W' || name.at(i) == 'X'
				|| name.at(i) == 'Y' || name.at(i) == 'Z' || name.at(i) == ' '
				|| name.at(i) == '*' || name.at(i) == '(' || name.at(i) == ')'
				|| name.at(i) == '.' || name.at(i) == ',' || name.at(i) == '&'
				|| name.at(i) == '@' || name.at(i) == '!' || name.at(i) == '?'
				|| name.at(i) == '-' || name.at(i) == '=' || name.at(i) == '+'
				|| name.at(i) == '/' || name.at(i) == ':' || name.at(i) == ';'
				|| name.at(i) == '\''|| name.at(i) == '\"'|| name.at(i) == '#'
				|| name.at(i) == '~' || name.at(i) == '`'
				|| name.at(i) == '[' || name.at(i) == ']' || name.at(i) == '{'
				|| name.at(i) == '}' || name.at(i) == '<' || name.at(i) == '>'
				|| name.at(i) == '%' || name.at(i) == '_' || name.at(i) == '÷'
				|| name.at(i) == '±' || name.at(i) == '•' || name.at(i) == '\\'
				|| name.at(i) == '|' || name.at(i) == '¤' || name.at(i) == '«'
				|| name.at(i) == '»' || name.at(i) == '¥' || name.at(i) == '€'
				|| name.at(i) == '£' || name.at(i) == '$' || name.at(i) == '¡'
				|| name.at(i) == '¿' || name.at(i) == '1'
				|| name.at(i) == '2' || name.at(i) == '3' || name.at(i) == '4'
				|| name.at(i) == '5' || name.at(i) == '6' || name.at(i) == '7'
				|| name.at(i) == '8' || name.at(i) == '9' || name.at(i) == '0') {
			continue;
		} else {
			notFound = true;
			qDebug() << "FOUND NOT ENGLISH!!!";
			break;
		}
	}
	return notFound;
}

void DataBase::insertForecast(ForecastObject forecastObject) {
	qDebug() << "DB insertForecast()";

	QString query =
			QString(
					"INSERT INTO forecast (time,date,precipMM,tempMaxC,tempMaxF,tempMinC,tempMinF,weatherCode,weatherDesc,winddir16Point,winddirDegree,winddirection,windspeedKmph,windspeedMiles) VALUES (");
	query.append("'");
	query.append(forecastObject.getTime());
	query.append("','");
	query.append(forecastObject.getDate());
	query.append("','");
	query.append(forecastObject.getPrecipMm());
	query.append("','");
	query.append(forecastObject.getTempMaxC());
	query.append("','");
	query.append(forecastObject.getTempMaxF());
	query.append("','");
	query.append(forecastObject.getTempMinC());
	query.append("','");
	query.append(forecastObject.getTempMinF());
	query.append("','");
	query.append(forecastObject.getWeatherCode());
	query.append("','");
	query.append(forecastObject.getWeatherDesc());
	query.append("','");
	query.append(forecastObject.getWinddir16Point());
	query.append("','");
	query.append(forecastObject.getWinddirDegree());
	query.append("','");
	query.append(forecastObject.getWinddirection());
	query.append("','");
	query.append(forecastObject.getWindspeedKmph());
	query.append("','");
	query.append(forecastObject.getWindspeedMiles());
	query.append("')");

	execInsertUpdateDelete(query);
}

void DataBase::insertOrUpdateLocation(LocationObject locationObject) {
	qDebug() << "DB insertOrUpdateLocation()";

	if (filterLocation(locationObject)) {
		return;
	}

	QVariantList list = execSelect(
			"SELECT * FROM location WHERE createtime = '"
					+ locationObject.getCreatetime()+"'");
	QString query;
	if (list.count() > 0) {
		//do update

//		query = "UPDATE location SET areaName = '"
//				+ areaName.getContentCode() + "', country = '"
//				+ areaName.getArtistId() + "', latitude = "
//				+ areaName.getSongNameEn() + ", longitude = "
//				+ areaName.getSongNameTh() + ", region = '"
//				+ areaName.getAlbumTh() + "', country2 = '"
//				+ areaName.getArtistEn() + "', isUserLocation = '"
//				+ areaName.getArtistTh() + "'";
	} else {
		//do insert

		query =
				QString(
						"INSERT INTO location (areaName,country,latitude,longitude,region,country2,isUserLocation,createtime) VALUES (");
		query.append("'");
		query.append(locationObject.getAreaName());
		query.append("','");
		query.append(locationObject.getCountry());
		query.append("',");
		query.append(QString::number(locationObject.getLatitude()));
		query.append(",");
		query.append(QString::number(locationObject.getLongitude()));
		query.append(",'");
		query.append(locationObject.getRegion());
		query.append("','");
		query.append(locationObject.getCountry2());
		query.append("','");
		query.append(locationObject.getIsUserLocation());
		query.append("','");
		query.append(locationObject.getCreatetime());
		query.append("')");
	}
	execInsertUpdateDelete(query);

}

bool DataBase::updateLocationByCreateTime(QString createtime, QString newName, QString newDesc){
	bool success = false;

	QVariantList list = execSelect(
				"SELECT * FROM location WHERE createtime = '"+createtime+ "'");
	QString query;
	if (list.count() > 0) {
		QVariantMap map = list.at(0).toMap();
		query = "UPDATE location SET areaName = '"
						+ newName + "', country = '"
						+ map.value("country").toString() + "', latitude = "
						+ map.value("latitude").toString() + ", longitude = "
						+ map.value("longitude").toString() + ", region = '"
						+ newDesc + "', country2 = '"
						+ map.value("country2").toString() + "', isUserLocation = '"
						+ map.value("isUserLocation").toString() + "' WHERE createtime = '"+createtime+ "'";
		execInsertUpdateDelete(query);
		success = true;
	}


	return success;
}

QVariantList DataBase::getAllWeatherData() {
	qDebug() << "-- DB getAllWeatherData()";

	QString query = QString("SELECT * FROM weatherdata");

//	QVariantList list = execSelect(query);
//		QVariantList newList = QVariantList();
//		for(int i=0; i<list.size();i++){
//			QVariantMap map = list.at(i).toMap();
//			map.insert("icon","asset:///images/redpin.png");
//			newList.append(map);
//		}
//		return newList;

	return execSelect(query);
}

QVariantList DataBase::getAllLocation() {
	qDebug() << "-- DB getAllLocation()";

	QString query = QString("SELECT * FROM location");

//	QVariantList list = execSelect(query);
//		QVariantList newList = QVariantList();
//		for(int i=0; i<list.size();i++){
//			QVariantMap map = list.at(i).toMap();
//			map.insert("icon","asset:///images/redpin.png");
//			newList.append(map);
//		}
//		return newList;

	return execSelect(query);
}

QVariantList DataBase::getLocationByLatLon(double latitude, double longitude) {
	qDebug()
			<< "-- DB getLocation() latitude: " + QString::number(latitude)
					+ ", longitude: " + QString::number(longitude);

	QString query = QString("SELECT * FROM location WHERE latitude <= ");
	query.append(QString::number(latitude + 0.05));
	query.append(" AND longitude <= ");
	query.append(QString::number(longitude + 0.05));
	query.append(" AND latitude >= ");
	query.append(QString::number(latitude - 0.05));
	query.append(" AND longitude >= ");
	query.append(QString::number(longitude - 0.05));

//	QVariantList list = execSelect(query);
//	QVariantList newList = QVariantList();
//	for(int i=0; i<list.size();i++){
//		QVariantMap map = list.at(i).toMap();
//		map.insert("icon","asset:///images/redpin.png");
//		newList.append(map);
//	}
//	return newList;
	return execSelect(query);
}

QVariantList DataBase::getForecastByTime(QString time) {
	qDebug() << "-- DB getForecastByTime() time: " + time;

	QString query = QString("SELECT * FROM forecast WHERE time = '");
	query.append(time);
	query.append("'");

//	QVariantList list = execSelect(query);
//	QVariantList newList = QVariantList();
//	for(int i=0; i<list.size();i++){
//		QVariantMap map = list.at(i).toMap();
//		map.insert("icon","asset:///images/redpin.png");
//		newList.append(map);
//	}
//	return newList;
	return execSelect(query);
}

