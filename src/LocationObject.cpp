/*
 * LocationObject.cpp
 *
 *  Created on: May 12, 2013
 *      Author: watcharatsuwannarat
 */

#include "LocationObject.h"


LocationObject::LocationObject() {
	// TODO Auto-generated constructor stub

}

LocationObject::~LocationObject() {
	// TODO Auto-generated destructor stub
}

QString LocationObject::getAreaName() const {
	return areaName;
}

void LocationObject::setAreaName(QString areaName) {
	this->areaName = areaName;
}

QString LocationObject::getCountry() const {
	return country;
}

void LocationObject::setCountry(QString country) {
	this->country = country;
}

double LocationObject::getLatitude() const {
	return latitude;
}

void LocationObject::setLatitude(double latitude) {
	this->latitude = latitude;
}

double LocationObject::getLongitude() const {
	return longitude;
}

void LocationObject::setLongitude(double longitude) {
	this->longitude = longitude;
}

QString LocationObject::getRegion() const {
	return region;
}

void LocationObject::setRegion(QString region) {
	this->region = region;
}

QString LocationObject::getCountry2() const {
	return country2;
}

void LocationObject::setCountry2(QString country2) {
	this->country2 = country2;
}

QString LocationObject::getIsUserLocation() const {
	return isUserLocation;
}

void LocationObject::setIsUserLocation(QString isUserLocation) {
	this->isUserLocation = isUserLocation;
}

QString LocationObject::getCreatetime() const {
	return createtime;
}

void LocationObject::setCreatetime(QString createtime) {
	this->createtime = createtime;
}



