// Default empty project template
#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include "DataBase.h"

#include <QObject>
#include <QtGui/qpainter.h>

#include <QtLocationSubset/QGeoPositionInfo>
#include <QtLocationSubset/QGeoPositionInfoSource>
//#include <bb/cascades/ActivityIndicator>
#include <bb/cascades/multimedia/Camera>

#include <bb/cascades/multimedia/CameraUnit>
#include <bb/cascades/Container>
#include <bb/cascades/Label>
#include <bb/cascades/Color>
#include <bb/cascades/AbsoluteLayoutProperties>
#include <bb/cascades/ImageView>
#include <bb/cascades/Image>
#include <bb/cascades/GroupDataModel>
#include <bb/cascades/ActionItem>

#include <bb/cascades/pickers/FilePicker>
#include <bb/cascades/pickers/FilePickerMode>
#include <bb/cascades/pickers/FilePickerSortFlag>
#include <bb/cascades/pickers/FilePickerSortOrder>
#include <bb/cascades/pickers/FilePickerViewMode>
#include <bb/cascades/pickers/FileType>

#include "InviteToDownload.hpp"
#include <bb/system/InvokeRequest>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeTargetReply>
#include <bb/cascades/advertisement/Banner>

#include <bb/system/SystemDialog>
#include "Profile.hpp"

namespace bb {
namespace cascades {
class Application;
}
}

using namespace bb::cascades;
using namespace bb::system;
using namespace QtMobilitySubset;
using namespace bb::cascades::multimedia;

/*!
 * @brief Application pane object
 *
 *Use this object to create and init app UI, to create context objects, to register the new meta types etc.
 */
class ApplicationUI: public QObject {
Q_OBJECT
Q_PROPERTY(bool cameraMode READ isCameraMode WRITE setCameraMode NOTIFY cameraModeChanged)
public:
	ApplicationUI(bb::cascades::Application *app);
	~ApplicationUI();

	Q_INVOKABLE
	void startUpdateWeather();

	Q_INVOKABLE
	void openFBPage();
	Q_INVOKABLE
	void openAppWorld();
	Q_INVOKABLE
	void openAppWorldPro();
	Q_INVOKABLE
	void openMail();

	Q_INVOKABLE
	void showHelp();
	Q_INVOKABLE
	void closeHelp();
	Q_INVOKABLE
	bool isShowHelp();

	Q_INVOKABLE
	bool addLocation(QString name, QString desc);
	bool checkLocationNameEng(QString name);

	Q_INVOKABLE
	bool isShowTutorial();
	Q_INVOKABLE
	void setShowTutorial(bool isShow);

	Q_INVOKABLE
	void setTemp(bool selTemp);

	Q_INVOKABLE
	void setTermCondition(bool ok);

	Q_INVOKABLE
	void saveImage();

	Q_INVOKABLE
	void restartCamera();

	Q_INVOKABLE
	void closeCamera();

	Q_INVOKABLE
	bool loadComplete();

	Q_INVOKABLE
	void setSelectedFilePath(const QString &path);
	void deleteSelectedFilePath();

	Q_INVOKABLE
	QString getSelectedFilePath();

	Q_INVOKABLE
	void setFlashCamera(bool isOpenFlash);

	Q_INVOKABLE
	void manipulatePhoto(const QString &fileName);

	Q_INVOKABLE
	void showPhotoInCard(const QString fileName);

	Q_INVOKABLE
	void setDesignIndex(int index);

	Q_INVOKABLE QString getCountryText();
	Q_INVOKABLE QString getLattitudeText();
	Q_INVOKABLE QString getLongitudeText();

	Q_INVOKABLE
	void showErrorInternet();

	Q_INVOKABLE
	void openTermOfService();
	Q_INVOKABLE
	void openPrivacy();

	void showError(QString wordBody);

	Q_INVOKABLE
	void refreshDesign(bool next);

	Q_INVOKABLE
	void refreshLocation();

	void refreshForecast();

	Q_INVOKABLE bool doEditLocation(QString createtime, QString newName, QString newDesc);

	Q_INVOKABLE
	void switchCamera();

	Q_INVOKABLE
	void chooseLocation(QVariantList indexPath);

	void setCameraMode(bool mode);
	bool isCameraMode();

signals:
	void chooseCelcius();
	void chooseFahrenheit();
	void cameraModeChanged(bool);
	void showTermConditionDialog();
	void closeEditLocationSheet();
	void closeAddLocationSheet();

private:
//
	bool forTest;
	bool cameraRear;
//	ActivityIndicator *loader;
	Container *loaderContainer;
	bb::cascades::Label *loaderLabel;

	DataBase *_db;

	QString timeRequestData;
	QString dateNowText;
	QString timeNowText;
	QSettings *settings;
//	QNetworkRequest request;
//	QNetworkAccessManager *networkAccessManager;
	SystemDialog *popupError;
	Camera *camera;
	bool cameraMode;
	bool isDay;

	Profile *profile;
	InviteToDownload *inviteToDownload;

	bool sHelp;
	Container *mainScroll;
	Container *helpContainer;

	QDate dateNow;
	int nowDay;
	int nowDayOfWeek;
	QVariantList foreCastQueue;

	ActionItem *tempAction;
	ImageView *imageView;
	Container *mainContainer;
	Container *mainContainer2;
	Container *mainContainer3;
	Container *mainContainer4;
	Container *mainContainer5;
	Container *mainContainer6;
	Container *mainContainer7;
	Container *mainContainer8;
	Container *mainContainer9;
	Container *mainContainer10;
	GroupDataModel *locationDataModel;

	QVariantMap descriptionMap;
	QVariantMap dayIconMap;
	QVariantMap nightIconMap;

	QVariantList locationList;
	QVariantList forecastList;
//	QVariantMap locationObject;

	QString assetFolder;
	QString key_weather, key_google;

	bool isLoadComplete;
	int countError;
	int designIndex;
	int designNum;
	int indexLocation;

	//paint -----------------------
	float topY;
	float imageWidth;
	float imageHeight;

	//write image to fileSystem
	QString imagefileName;
	QString selectedFilePath;
	QString lastedPath;

	QString iconfileName;
	QImageReader reader;
	QString character;
	QImage icon;
	QSize iconSize;

//	int xIcon;
//	ImageView* imageView;
//	AbsoluteLayoutProperties* imgProperties;
	//----------------------------------------------

	// The central object to retrieve location information
	QGeoPositionInfoSource *m_positionSource;

	double m_latitude;
	double m_longitude;
//	QVariantMap sel_locationObj;
	QString m_time;
	QString temp_C, temp_F, weatherCode, weatherDescValue,weatherIconPath;
	QString cloudcover, humidity, precipMM, pressure, visibility, winddir16Point, winddirDegree, windspeedKmph, windspeedMiles;

	//humidity humidity.png %
	//precipMM rain.png mm
	//pressure pressure.png hPa
	//visibility visibility.png m
	//winddir16Point direction.png
	//windspeedKmph wind.png km/h

	QString areaName, country, region, country2;

	QString _createtime,_newName,_newDesc;

	bb::system::InvokeTargetReply *_invokeTargetReply;

	QString getTimeMillis();
	void getAllData();
//	void beginRenderImage();
	bool checkForUpdateDataByTime();
	int getWidthText(int newHeight, QString wording, QString folderIcon, int maxWidth, bool isTemp);
	int drawTextImage(int x, int y, int newHeight, QString wording,
			int maxWidth, QString folderIcon, Container *container, bool isTemp, float opacity);
	QString checkCase(QString c);
	int renderImageView(Container *_container, QString iconfileName, int newHeight, int x, int y, float opacity);
	QString getTempText();
	QString getDirectionText(bool needShort);
//	bool loadCon1;
//	bool loadCon2;
//	bool loadCon3;
//	bool loadCon4;
//	bool loadCon5;
//	bool loadCon6;
//	bool loadCon7;
//	bool loadCon8;
//	bool loadCon9;
//	bool loadCon10;

	void refresh();
	void testWorkerRender(bb::cascades::Container* container);

//	void renderContainer1();
//	void renderContainer2();
//	void renderContainer3();
//	void renderContainer4();
//	void renderContainer5();
//	void renderContainer6();
//	void renderContainer7();
//	void renderContainer8();
//	void renderContainer9();
//	void renderContainer10();

	//request api
	void doRequestWeather();
	void doRequestLocation();
	void doRequestCountry();

	void paintBG(QPainter &painter, int x, int y, int width, int height, QColor &color, float opacity);
	int paintTextImage(int x, int y, int newHeight, QString wording,
			int maxWidth, QString folderIcon, QPainter &painter, bool isTemp, float opacity);
	int paintImageView(QPainter &painter, QString iconfileName, int newHeight, int x, int y, float opacity);

	void paintImage1(QPainter &painter, QSize &size);
	void paintImage2(QPainter &painter, QSize &size);
	void paintImage3(QPainter &painter, QSize &size);
	void paintImage4(QPainter &painter, QSize &size);
	void paintImage5(QPainter &painter, QSize &size);
	void paintImage6(QPainter &painter, QSize &size);
	void paintImage7(QPainter &painter, QSize &size);
	void paintImage8(QPainter &painter, QSize &size);
	void paintImage9(QPainter &painter, QSize &size);
	void paintImage10(QPainter &painter, QSize &size);

private slots:

	void renderContainer1();
	void renderContainer2();
	void renderContainer3();
	void renderContainer4();
	void renderContainer5();
	void renderContainer6();
	void renderContainer7();
	void renderContainer8();
	void renderContainer9();
	void renderContainer10();

	void onInvokeResult();
	void onInviteSuccess(bool success);

	void clickEditLocationOK(bb::system::SystemUiResult::Type value);

	void onRequestFinishedWeather(QNetworkReply* reply);
	void onRequestFinishedLocation(QNetworkReply* reply);
	void onRequestFinishedCountry(QNetworkReply* reply);

	// When we get a ShutterFired event from the system, we play a shutter sound.
	void onShutterFired();

	// This slot is invoked whenever new location information are retrieved
	void positionUpdated(const QGeoPositionInfo & pos);

	// This slot is invoked whenever a timeout happend while retrieving location information
	void positionUpdateTimeout();

//	void onPaintWorker1Error(QString &err);
//	void onPaintWorker1Finished();
};

#endif /* ApplicationUI_HPP_ */
