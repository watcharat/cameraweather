/*
 * ForecastObject.cpp
 *
 *  Created on: Jun 19, 2013
 *      Author: watcharatsuwannarat
 */

#include "ForecastObject.h"

ForecastObject::ForecastObject() {
	// TODO Auto-generated constructor stub

}

ForecastObject::~ForecastObject() {
	// TODO Auto-generated destructor stub
}

QString ForecastObject::getDate() const {
	return date;
}

void ForecastObject::setDate(QString date) {
	this->date = date;
}

QString ForecastObject::getPrecipMm() const {
	return precipMM;
}

void ForecastObject::setPrecipMm(QString precipMm) {
	precipMM = precipMm;
}

QString ForecastObject::getTempMaxC() const {
	return tempMaxC;
}

void ForecastObject::setTempMaxC(QString tempMaxC) {
	this->tempMaxC = tempMaxC;
}

QString ForecastObject::getTempMaxF() const {
	return tempMaxF;
}

void ForecastObject::setTempMaxF(QString tempMaxF) {
	this->tempMaxF = tempMaxF;
}

QString ForecastObject::getTempMinC() const {
	return tempMinC;
}

void ForecastObject::setTempMinC(QString tempMinC) {
	this->tempMinC = tempMinC;
}

QString ForecastObject::getTempMinF() const {
	return tempMinF;
}

void ForecastObject::setTempMinF(QString tempMinF) {
	this->tempMinF = tempMinF;
}

QString ForecastObject::getTime() const {
	return time;
}

void ForecastObject::setTime(QString time) {
	this->time = time;
}

QString ForecastObject::getWeatherCode() const {
	return weatherCode;
}

void ForecastObject::setWeatherCode(QString weatherCode) {
	this->weatherCode = weatherCode;
}

QString ForecastObject::getWeatherDesc() const {
	return weatherDesc;
}

void ForecastObject::setWeatherDesc(QString weatherDesc) {
	this->weatherDesc = weatherDesc;
}

QString ForecastObject::getWinddir16Point() const {
	return winddir16Point;
}

void ForecastObject::setWinddir16Point(QString winddir16Point) {
	this->winddir16Point = winddir16Point;
}

QString ForecastObject::getWinddirDegree() const {
	return winddirDegree;
}

void ForecastObject::setWinddirDegree(QString winddirDegree) {
	this->winddirDegree = winddirDegree;
}

QString ForecastObject::getWinddirection() const {
	return winddirection;
}

void ForecastObject::setWinddirection(QString winddirection) {
	this->winddirection = winddirection;
}

QString ForecastObject::getWindspeedKmph() const {
	return windspeedKmph;
}

void ForecastObject::setWindspeedKmph(QString windspeedKmph) {
	this->windspeedKmph = windspeedKmph;
}

QString ForecastObject::getWindspeedMiles() const {
	return windspeedMiles;
}

void ForecastObject::setWindspeedMiles(QString windspeedMiles) {
	this->windspeedMiles = windspeedMiles;
}

