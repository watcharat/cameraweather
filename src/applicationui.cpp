// Default empty project template
#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/multimedia/CameraSettings>
#include <bb/cascades/multimedia/CameraFocusMode>
#include <bb/cascades/multimedia/CameraFlashMode>
//#include <QtGui/QImage>
//#include <QtGui/QImageReader>
//#include <QtGui/QTransform>
//#include <QtGui/QFont>
//#include <QtGui/QPen>
//#include <QtGui/QPolygon>
//#include <QtGui/QColor>
//
//#include <QtGui/QFontDatabase>
#include "PaintWorker/PaintWorker1.hpp"
#include <bb/system/InvokeRequest>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeTargetReply>
#include <bb/data/JsonDataAccess>
#include <bb/data/XmlDataAccess>
#include <bb/platform/geo/GeoLocation.hpp>

#include "ProfileEditor.hpp"
#include "RegistrationHandler.hpp"
#include <bb/system/SystemUiResult>

#include <bps/soundplayer.h>

using namespace bb::cascades;
using namespace bb::system;
using namespace bb::platform::geo;

#define FONT_WINTER "WinterthurConsided"
#define FONT_CANDAL "Candal"

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
		QObject(app), m_latitude(0), m_longitude(0), m_positionSource(
				QGeoPositionInfoSource::createDefaultSource(this)) {

	//----------  PREPARE DATA  -----------------------
	cameraRear = true;
//	loadCon1 = false;
//	loadCon2 = false;
//	loadCon3 = false;
//	loadCon4 = false;
//	loadCon5 = false;
//	loadCon6 = false;
//	loadCon7 = false;
//	loadCon8 = false;
//	loadCon9 = false;
//	loadCon10 = false;

	forTest = false;

	sHelp = false;
	setCameraMode(true);
	isLoadComplete = false;
	designIndex = 0;
	designNum = 9;
	indexLocation = 0;

	topY = 58.0; //128.0;
	imageWidth = 768.0;
	imageHeight = 1024.0;

	countError = 0;
	key_weather = "tc4feefhdfuh75f6pvupfnhz";
	key_google = "AIzaSyBuJweYhLFmX3fvlKhWkv6lnZqNlItSlcs";

	QString appFolder(QDir::homePath());
	appFolder.chop(4);
	assetFolder = appFolder + "app/native/assets/";
	selectedFilePath = "";
	lastedPath = "";

	QCoreApplication::setOrganizationName("MazMellow");
	QCoreApplication::setApplicationName("Camera Weather");
	settings = new QSettings("MazMellow", "Camera Weather");

//	qDebug() << "before setting value: "+settings->value("temp").toString();
	if (settings->value("temp").isNull()) {
		settings->setValue("temp", "c");
	}
	qDebug() << "after setting value: " + settings->value("temp").toString();

	country = settings->value("country").toString();
	country2 = settings->value("country2").toString();

	//FOR TEST-------------------------
//	if (forTest) {
//		settings->setValue("temp_C", "33");
//		settings->setValue("temp_F", "91");
//		settings->setValue("weatherCode", "116");
//		settings->setValue("weatherDescValue", "Partly Cloudy");
//	}
	// --------------------------------

	_db = DataBase::getInstance();
	_db->copyDbToDataFolder("cameraweather.db");

//	//get current time
//	QTime time = QTime::currentTime();
//	QString timeString = time.toString();
//	qDebug() << "timeString = " + timeString;
//
	//get current date and time
	QDateTime dateTime = QDateTime::currentDateTime();
	QString dateTimeString = dateTime.toString();
	qDebug() << "dateTimeString = " + dateTimeString;

	dateNow = dateTime.date();
	dateNowText = dateNow.toString("ddd, d MMM yyyy");
	qDebug() << "dateNowText: " + dateNowText;

	QTime timeNow = dateTime.time();
	qDebug() << "timeNow: " + timeNow.toString("hh:mm");
	int hour = timeNow.hour();
	if (hour > 6 && hour < 18) {
		isDay = true;
		timeNowText = timeNow.toString("hh:mm") + " am";
	} else {
		isDay = false;
		timeNowText = timeNow.toString("hh:mm") + " pm";
	}

//----------------------------------------------------

	qmlRegisterType<QTimer>("bb.cascades", 1, 0, "QTimer");
	qmlRegisterType<bb::cascades::advertisement::Banner>(
			"bb.cascades.advertisement", 1, 0, "Banner");
	qmlRegisterType<Camera>("bb.cascades.multimedia", 1, 0, "Camera");
	qmlRegisterType<pickers::FilePicker>("bb.cascades.pickers", 1, 0,
			"FilePicker");
	qmlRegisterUncreatableType<pickers::FilePickerMode>("bb.cascades.pickers",
			1, 0, "FilePickerMode", "");
	qmlRegisterUncreatableType<pickers::FilePickerSortFlag>(
			"bb.cascades.pickers", 1, 0, "FilePickerSortFlag", "");
	qmlRegisterUncreatableType<pickers::FilePickerSortOrder>(
			"bb.cascades.pickers", 1, 0, "FilePickerSortOrder", "");
	qmlRegisterUncreatableType<pickers::FileType>("bb.cascades.pickers", 1, 0,
			"FileType", "");
	qmlRegisterUncreatableType<pickers::FilePickerViewMode>(
			"bb.cascades.pickers", 1, 0, "FilePickerViewMode", "");

	QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this);
	qml->setContextProperty("app", this);

	//bbm
	const QString uuid(QLatin1String("bd9dc8c4-95ad-444b-a5c6-ea4baaa0acde"));
	RegistrationHandler *registrationHandler = new RegistrationHandler(uuid,
			app);
	qml->setContextProperty("_registrationHandler", registrationHandler);
	profile = new Profile(registrationHandler->context(), app);
	qml->setContextProperty("_profile", profile);
	inviteToDownload = new InviteToDownload(registrationHandler->context(),
			app);
	QObject::connect(registrationHandler, SIGNAL(registered()),
			inviteToDownload, SLOT(show()));
	qml->setContextProperty("_inviteToDownload", inviteToDownload);
	connect(inviteToDownload, SIGNAL(inviteSuccess(bool)), this,
			SLOT(onInviteSuccess(bool)));

	AbstractPane *root = qml->createRootObject<AbstractPane>();
	if (root) {
		app->setScene(root);

		loaderContainer = root->findChild<Container*>("loaderContainer");
		loaderLabel = root->findChild<Label*>("loaderLabel");

		tempAction = root->findChild<ActionItem*>("tempAction");
		if (tempAction) {
			connect(this, SIGNAL(chooseCelcius()), tempAction,
					SLOT(setCelcius()));
			connect(this, SIGNAL(chooseFahrenheit()), tempAction,
					SLOT(setFahrenheit()));
		}
		imageView = root->findChild<ImageView*>("imageView");

		mainContainer = root->findChild<Container*>("mainContainer");
		mainContainer2 = root->findChild<Container*>("mainContainer2");
		mainContainer3 = root->findChild<Container*>("mainContainer3");
		mainContainer4 = root->findChild<Container*>("mainContainer4");
		mainContainer5 = root->findChild<Container*>("mainContainer5");
		mainContainer6 = root->findChild<Container*>("mainContainer6");
		mainContainer7 = root->findChild<Container*>("mainContainer7");
		mainContainer8 = root->findChild<Container*>("mainContainer8");
		mainContainer9 = root->findChild<Container*>("mainContainer9");
		mainContainer10 = root->findChild<Container*>("mainContainer10");

		mainScroll = root->findChild<Container*>("mainScroll");
		helpContainer = root->findChild<Container*>("helpContainer");

		camera = root->findChild<Camera*>("myCamera");
		QObject::connect(camera, SIGNAL(shutterFired()), this,
				SLOT(onShutterFired()));

		//must check camera's state before open it
		// Retrieve a list of accessible camera units.
		QList<CameraUnit::Type> list = camera->supportedCameras();

		if (list.isEmpty() || list.contains(CameraUnit::None)) {
			// Report: "No cameras are accessible."
			//show error and quit app
			showError(
					"The camera is in use. Close any application using it and try again.");
			app->quit();
		} else {
			if (list.contains(CameraUnit::Rear)) {
				// The default camera unit is rear
				// so no parameter is necessary.
				camera->open(CameraUnit::Rear);
				cameraRear = true;

				CameraSettings *setting = new CameraSettings();
				camera->getSettings(setting);
				setting->setFocusMode(CameraFocusMode::ContinuousAuto);
				camera->applySettings(setting);
				setting = 0;
			} else if (list.contains(CameraUnit::Front)) {
				// Open the front camera unit.
				camera->open(CameraUnit::Front);
				cameraRear = false;

				CameraSettings *setting = new CameraSettings();
				camera->getSettings(setting);
				setting->setFocusMode(CameraFocusMode::ContinuousAuto);
				camera->applySettings(setting);
				setting = 0;
			} else {
				// Report unknown error.
				//show error and quit app
				showError(
						"The camera is in use. Close any application using it and try again.");
//				app->quit();
				Application::instance()->exit(0);
			}
		}

		locationDataModel = root->findChild<GroupDataModel*>(
				"locationDataModel");
		locationDataModel->setGrouping(ItemGrouping::None);
		if (locationDataModel) {
			qDebug() << "locationDataModel != null";
		} else {
			qDebug() << "locationDataModel == null";
		}
	}

	//prepare description text   <codes><condition>
	descriptionMap = QVariantMap();
	dayIconMap = QVariantMap();
	nightIconMap = QVariantMap();
	QString code, description, day_icon, night_icon;

	QVariantMap dataMap = QVariantMap();
//	XmlDataAccess xda;
//	QVariant data = xda.load("sql/weathercode.xml", "/codes/condition");
//	if (!data.isNull()) {
//		qDebug() << "data not null";
	QVariantList codeList = _db->getAllWeatherData(); //data.toList();
	for (int i = 0; i < codeList.size(); i++) {
		dataMap = codeList.at(i).toMap();
		code = dataMap.value("code").toString();
		description = dataMap.value("description").toString();
		day_icon = dataMap.value("day_icon").toString();
		night_icon = dataMap.value("night_icon").toString();

		qDebug() << "----[" + QString::number(i) + "]----";
		qDebug() << "---- code: " + code;
		qDebug() << "---- description: " + description;
		qDebug() << "---- day_icon: " + day_icon;
		qDebug() << "---- night_icon: " + night_icon;

		descriptionMap.insert(code, description);
		dayIconMap.insert(code, day_icon);
		nightIconMap.insert(code, night_icon);
	}

//	}else{
//		qDebug() << "data == null";
//	}

	if (settings->value("temp").toString() == "c") {
		qDebug() << "emit set button fahrenheit";
		emit chooseFahrenheit();
	} else {
		qDebug() << "emit set button celcius";
		emit chooseCelcius();
	}
	startUpdateWeather();

	bool res = connect(this, SIGNAL(showTermConditionDialog()), root,
			SLOT(doShowTermConditionDialog()));
	if (res) {
		qDebug() << "doShowTermConditionDialog ok";
	} else {
		qDebug() << "doShowTermConditionDialog fail";
	}
	if (settings->value("terms").isNull()
			|| !(settings->value("terms").toBool())) {
		//show terms user dialog
		emit showTermConditionDialog();
	}

	//first tutorial
	if (isShowTutorial()) {
		showHelp();
	}
}

void ApplicationUI::closeHelp() {
	qDebug() << "closeHelp()";
	sHelp = false;
	helpContainer->setVisible(false);
	mainScroll->setVisible(true);
}

void ApplicationUI::showHelp() {
	qDebug() << "showHelp()";
	sHelp = true;
	helpContainer->setVisible(true);
	mainScroll->setVisible(false);
}

bool ApplicationUI::isShowHelp() {
	return sHelp;
}

void ApplicationUI::openTermOfService() {
	//http://www.google.com/intl/en/policies/terms/
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.browser");
	// What do we want the target application to do with it?
	request.setAction("bb.action.OPEN");
	// What are we sending?
	//	request.setMimeType("text/plain");
	// Where is the data?
	request.setUri(QUrl("http://www.google.com/intl/en/policies/terms/"));

	//	QByteArray byte;
	//	byte.append(text);
	//	request.setData(byte);

	//	request.setData(text.toUtf8());
//		QVariantMap payload;
//		payload["object_type"] = "page";
//		payload["object_id"] = "328506290597521"; // BlackBerry NA Facebook page
//		request.setMetadata(payload);

	InvokeTargetReply *reply = invokeManager.invoke(request);
	if (reply) {
		// Remember to take ownership of the reply object
		reply->setParent(this);
		// Listen for the invoke response
		QObject::connect(reply, SIGNAL(finished()), this,
				SLOT(onInvokeResult()));
		// Store reply somewhere, so you can
		// access it when onInvokeResult fires
		_invokeTargetReply = reply;
	}
}

void ApplicationUI::openPrivacy() {
	//http://www.google.com/intl/en/policies/privacy/
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.browser");
	// What do we want the target application to do with it?
	request.setAction("bb.action.OPEN");
	// What are we sending?
	//	request.setMimeType("text/plain");
	// Where is the data?
	request.setUri(QUrl("http://www.google.com/intl/en/policies/privacy/"));

	//	QByteArray byte;
	//	byte.append(text);
	//	request.setData(byte);

	//	request.setData(text.toUtf8());
	//		QVariantMap payload;
	//		payload["object_type"] = "page";
	//		payload["object_id"] = "328506290597521"; // BlackBerry NA Facebook page
	//		request.setMetadata(payload);

	InvokeTargetReply *reply = invokeManager.invoke(request);
	if (reply) {
		// Remember to take ownership of the reply object
		reply->setParent(this);
		// Listen for the invoke response
		QObject::connect(reply, SIGNAL(finished()), this,
				SLOT(onInvokeResult()));
		// Store reply somewhere, so you can
		// access it when onInvokeResult fires
		_invokeTargetReply = reply;
	}
}

void ApplicationUI::setTermCondition(bool ok) {
	if (ok) {
		settings->setValue("terms", true);
	} else {
		//quit app
		//Application::instance()->exit(0);
	}
}

void ApplicationUI::openFBPage() {
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("com.rim.bb.app.facebook");
	// What do we want the target application to do with it?
	request.setAction("bb.action.OPEN");
	// What are we sending?
//	request.setMimeType("text/plain");
	// Where is the data?
	//	request.setUri(QUrl("file:///path/to/image.png"));

	//	QByteArray byte;
	//	byte.append(text);
	//	request.setData(byte);

//	request.setData(text.toUtf8());
	QVariantMap payload;
	payload["object_type"] = "page";
	payload["object_id"] = "328506290597521"; // BlackBerry NA Facebook page
	request.setMetadata(payload);

	InvokeTargetReply *reply = invokeManager.invoke(request);
	if (reply) {
		// Remember to take ownership of the reply object
		reply->setParent(this);
		// Listen for the invoke response
		QObject::connect(reply, SIGNAL(finished()), this,
				SLOT(onInvokeResult()));
		// Store reply somewhere, so you can
		// access it when onInvokeResult fires
		_invokeTargetReply = reply;
	}
}

void ApplicationUI::openAppWorld() {
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.appworld");
	request.setAction("bb.action.OPEN");
//	request.setUri(QUrl("appworld:///"));
	request.setUri(QUrl("appworld://content/32658887"));
	InvokeTargetReply *reply = invokeManager.invoke(request);
	if (reply) {
		reply->setParent(this);
		QObject::connect(reply, SIGNAL(finished()), this,
				SLOT(onInvokeResult()));
		_invokeTargetReply = reply;
	}
}

void ApplicationUI::openMail() {
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.pim.uib.email.hybridcomposer");
	request.setAction("bb.action.COMPOSE");
	request.setMimeType("message/rfc822");
	request.setUri(QUrl("mailto:mazmellow@gmail.com"));
	InvokeTargetReply *reply = invokeManager.invoke(request);
	if (reply) {
		reply->setParent(this);
		QObject::connect(reply, SIGNAL(finished()), this,
				SLOT(onInvokeResult()));
		_invokeTargetReply = reply;
	}
}

void ApplicationUI::openAppWorldPro() {
	InvokeManager invokeManager;
	InvokeRequest request;
	request.setTarget("sys.appworld");
	request.setAction("bb.action.OPEN");
	request.setUri(QUrl("appworld:///"));
//	request.setUri(QUrl("appworld://content/<contentid>"));
	InvokeTargetReply *reply = invokeManager.invoke(request);
	if (reply) {
		reply->setParent(this);
		QObject::connect(reply, SIGNAL(finished()), this,
				SLOT(onInvokeResult()));
		_invokeTargetReply = reply;
	}
}

void ApplicationUI::onInvokeResult() {
	// Check for errors
	switch (_invokeTargetReply->error()) {
	// Invocation could not find the target
	// did we use the right target ID?
	case InvokeReplyError::NoTarget: {
		qDebug() << "invokeFinished(): Error: no target";
		break;
	}
		// There was a problem with the invoke request
		// did we set all the values correctly?
	case InvokeReplyError::BadRequest: {
		qDebug() << "invokeFinished(): Error: bad request";
		break;
	}
		// Something went completely
		// wrong inside the invocation request
		// Find an alternate route :(
	case InvokeReplyError::Internal: {
		qDebug() << "invokeFinished(): Error: internal";
		break;
	}
		// Woohoo! The invoke request was successful
	default:
		qDebug() << "invokeFinished(): Invoke Succeeded";
		break;
	}

	// A little house keeping never hurts...
	delete _invokeTargetReply;

}

void ApplicationUI::onInviteSuccess(bool success) {
	if (success) {
		qDebug() << "onInviteSuccess(): true";
	} else {
		qDebug() << "onInviteSuccess(): false";
	}
}

void ApplicationUI::setTemp(bool selTemp) {
	if (selTemp) {
		settings->setValue("temp", "c");
	} else {
		settings->setValue("temp", "f");
	}
	refresh();
}

bool ApplicationUI::isShowTutorial() {
	return settings->value("showTutorial", true).toBool();
}

void ApplicationUI::setShowTutorial(bool isShow) {
	settings->setValue("showTutorial", isShow);
}

void ApplicationUI::startUpdateWeather() {
	//----- RETRIEVE WEATHER DATA -------------
	if (m_positionSource) {
		connect(m_positionSource,
				SIGNAL(positionUpdated(const QGeoPositionInfo &)), this,
				SLOT(positionUpdated(const QGeoPositionInfo &)));
		connect(m_positionSource, SIGNAL(updateTimeout()), this,
				SLOT(positionUpdateTimeout()));

		qDebug() << "Initialized QGeoPositionInfoSource";
	} else {
		qDebug() << "Failed to initialized QGeoPositionInfoSource";
	}

	if (m_positionSource) {

		if (forTest) {
			//FOR TEST  13.741112,100.554581
			m_latitude = 13.741112;
			m_longitude = 100.554581;
			if (checkForUpdateDataByTime()) {
				//				if (forTest) {
				//					getAllData();
				//				} else {
				//get new data
				countError = 0;
				doRequestWeather();
				//				}
			} else {
				getAllData();
			}
		} else {
			m_positionSource->startUpdates();
			if (loaderContainer) {
				loaderContainer->setVisible(true);
				loaderLabel->setText("Getting your location data...");
			}
		}

		qDebug() << "Position updates started";
	}
	//------------------------------------------
}

ApplicationUI::~ApplicationUI() {
	// TODO Auto-generated destructor stub
	m_positionSource = 0;
	deleteSelectedFilePath();
}

void ApplicationUI::setCameraMode(bool mode) {
	cameraMode = mode;
	emit cameraModeChanged(cameraMode);
}

bool ApplicationUI::isCameraMode() {
	return cameraMode;
}

void ApplicationUI::setDesignIndex(int index) {
	designIndex = index;
	qDebug() << "designIndex: " + QString::number(designIndex);
}

void ApplicationUI::switchCamera() {
	cameraRear = !cameraRear;
	camera->close();
	restartCamera();
}

void ApplicationUI::deleteSelectedFilePath(){
	qDebug() << "deleteSelectedFilePath: " + selectedFilePath;
	if(selectedFilePath!=""){
		QFile *file = new QFile(selectedFilePath);
		if (file->exists()) {
			if (file->remove()) {
				qDebug() << "REMOVE SUCCESS";
				selectedFilePath = "";
			} else {
				qDebug() << "REMOVE FAIL";
			}
		}
	}
}

void ApplicationUI::setSelectedFilePath(const QString &path) {
	qDebug() << "setSelectedFilePath():" + path;
	deleteSelectedFilePath();
	lastedPath = path;
//	selectedFilePath = path;
	//check wheter size of image
	reader.setFileName(path);
	icon = reader.read();
	iconSize = icon.size();
	if (iconSize.width() >= iconSize.height()) {
		//scale to height
		icon = icon.scaledToHeight(1024, Qt::SmoothTransformation);
		if (icon.size().width() > 768) {
			//crop width
			qDebug() << "crop width";
			icon = icon.copy((icon.size().width() - 768) / 2, 0, 768, 1024);
		}
	} else if (icon.height() > iconSize.width()) {
		//scale to width
		icon = icon.scaledToWidth(768, Qt::SmoothTransformation);
		if (icon.size().height() > 1024) {
			//crop height
			qDebug() << "crop height";
			icon = icon.copy(0, (icon.size().height() - 1024) / 2, 768, 1024);
		}
	}/*else{
	 //width = heigth
	 //		icon = icon.scaled(768,1024,Qt::KeepAspectRatio, Qt::SmoothTransformation);
	 icon = icon.scaledToHeight(1024, Qt::SmoothTransformation);
	 }*/

	qDebug() << "icon width: " + QString::number(icon.size().width());
	qDebug() << "icon hight: " + QString::number(icon.size().height());

	qDebug() << "path:" + path;
	imagefileName = path.mid(path.lastIndexOf('/') + 1,
			path.lastIndexOf('.'));
	imagefileName.chop(4);
	imagefileName = imagefileName + "_"
			+ QString::number(QDateTime::currentMSecsSinceEpoch()) + ".jpg";
	qDebug() << "imagefileName:" + imagefileName;

	QDir dir;
	qDebug() << "BEFORE QDir::currentPath() = "+QDir::currentPath();
	qDebug() << "BEFORE dir.path() = "+dir.path();
	dir.cd("tmp/");

	qDebug() << "AFTER QDir::currentPath() = "+QDir::currentPath();
	qDebug() << "AFTER dir.path() = "+dir.path();

	QString pathMain = /*"file://" +*/ QDir::currentPath() + "/" + dir.path()+ "/";
	selectedFilePath = pathMain + imagefileName;
//	QString oldPath = assetFolder + imagefileName;
//	qDebug() << "OLD selectedFilePath: " + oldPath;
	qDebug() << "NEW selectedFilePath: " + selectedFilePath;

//	showError("selectedFilePath : "+selectedFilePath);
	if (icon.save(selectedFilePath, "JPG")) {
		imageView->setImageSource(QUrl(selectedFilePath));
		imageView->setVisible(true);
		qDebug() << "SAVE SUCCESS!";
	} else {
		qDebug() << "SAVE FAILED!";
		//popup
		selectedFilePath = "";
		showError(
				"Sorry, there is a problem about select image. Please try again.");
		restartCamera();

	}

//	imageView->setImage(icon);
}

QString ApplicationUI::getSelectedFilePath() {
	return selectedFilePath;
}

void ApplicationUI::restartCamera() {
	qDebug() << "restartCamera()";
	setCameraMode(true);
	camera->setVisible(true);
	selectedFilePath = "";
	imageView->setVisible(false);
	if (cameraRear) {
		camera->open(CameraUnit::Rear);
	} else {
		camera->open(CameraUnit::Front);
	}
}

void ApplicationUI::closeCamera() {
	qDebug() << "Close Camera";
	setCameraMode(false);
	camera->close();
	camera->setVisible(false);
}

void ApplicationUI::setFlashCamera(bool isOpenFlash) {
	if (isOpenFlash) {
		qDebug() << "setFlashCamera: ON";
		CameraSettings *setting = new CameraSettings();
		camera->getSettings(setting);
		setting->setFlashMode(CameraFlashMode::Auto);
		camera->applySettings(setting);
		setting = 0;
	} else {
		qDebug() << "setFlashCamera: OFF";
		CameraSettings *setting = new CameraSettings();
		camera->getSettings(setting);
		setting->setFlashMode(CameraFlashMode::Off);
		camera->applySettings(setting);
		setting = 0;
	}
//	restartCamera();
}

void ApplicationUI::positionUpdated(const QGeoPositionInfo& pos) {
	qDebug() << "positionUpdated()";

//	m_latitude = pos.coordinate().latitude();
//	m_longitude = pos.coordinate().longitude();

	//italy
//	m_latitude = 41.902277;
//	m_longitude = 12.546387;

	//peru
	m_latitude = 41.892934;
	m_longitude = 12.48251;

	m_time = pos.timestamp().toString();
	qDebug() << "m_latitude = " + QString::number(m_latitude);
	qDebug() << "m_longitude = " + QString::number(m_longitude);
	qDebug() << "m_time = " + m_time; //Fri May 10 05:03:45 2013
	disconnect(m_positionSource,
			SIGNAL(positionUpdated(const QGeoPositionInfo &)), this,
			SLOT(positionUpdated(const QGeoPositionInfo &)));
	disconnect(m_positionSource, SIGNAL(updateTimeout()), this,
			SLOT(positionUpdateTimeout()));

	if (checkForUpdateDataByTime()) {
		if (forTest) {
			getAllData();
		} else {
			//get new data
			countError = 0;
			doRequestWeather();
		}
	} else {
		getAllData();
	}

}

bool ApplicationUI::checkForUpdateDataByTime() {
	qDebug() << "-- checkForUpdateDataByTime()";
	int currentTime = QDateTime::currentMSecsSinceEpoch();
	QVariant lastedLat = settings->value("lastedLat");
	QVariant lastedLon = settings->value("lastedLon");

	qDebug() << "GET LAT: " + QString::number(m_latitude);
	qDebug() << "GET LON: " + QString::number(m_longitude);

	if (lastedLat.isNull() || lastedLon.isNull()) {
		//first open
		settings->setValue("lastedTime", currentTime);
		settings->setValue("lastedLat", m_latitude);
		settings->setValue("lastedLon", m_longitude);
		qDebug() << " >> first open";
		return true;
	} else {
		double d_lastedLat = lastedLat.toDouble();
		double d_lastedLon = lastedLon.toDouble();

		qDebug() << "LAST LAT: " + QString::number(d_lastedLat);
		qDebug() << "LAST LON: " + QString::number(d_lastedLon);

		double deltaLat = m_latitude - d_lastedLat;
		if (deltaLat < 0) {
			deltaLat = deltaLat * (-1);
		}
		double deltaLon = m_longitude - d_lastedLon;
		if (deltaLon < 0) {
			deltaLon = deltaLon * (-1);
		}

		qDebug() << "DELTA LAT: " + QString::number(deltaLat);
		qDebug() << "DELTA LON: " + QString::number(deltaLon);

//		if ((qAbs(m_latitude - d_lastedLat) <= 0.003) && (qAbs(m_longitude - d_lastedLon) <= 0.003)) {
		if (deltaLat <= 0.003 && deltaLon <= 0.003) {
			//use old location data
			int lastedTime = settings->value("lastedTime").toInt();

			qDebug() << "CURRENT TIME: " + QString::number(currentTime);
			qDebug() << "LAST TIME: " + QString::number(lastedTime);
			int deltaTime = currentTime - lastedTime;
			qDebug() << "DELTA TIME: " + QString::number(deltaTime);

			if (deltaTime >= (2 * 60 * 60 * 1000)) {
				// if more than 2 hr. then update new weather data
				qDebug() << " >> more than 2 hr";
				settings->setValue("lastedTime", currentTime);
				settings->setValue("lastedLat", m_latitude);
				settings->setValue("lastedLon", m_longitude);
				return true;
			} else {
				qDebug() << " >> less than 2 hr";
				settings->setValue("lastedTime", currentTime);
				settings->setValue("lastedLat", m_latitude);
				settings->setValue("lastedLon", m_longitude);
				return false;
			}
		} else {
			// new place
			qDebug() << " >> new place";
			settings->setValue("lastedTime", currentTime);
			settings->setValue("lastedLat", m_latitude);
			settings->setValue("lastedLon", m_longitude);
			return true;
		}
	}

}

QString ApplicationUI::getTimeMillis() {
	int timeEpoch = QDateTime::currentMSecsSinceEpoch();
	if (timeEpoch < 0) {
		timeEpoch = -timeEpoch;
	}
	QString timeMillis = QString::number(timeEpoch);
	qDebug() << "--> getTimeMillis: " + timeMillis;
	return timeMillis;
}

void ApplicationUI::doRequestWeather() {
	qDebug() << "doRequestWeather()";

	if (countError == 3) {
		showErrorInternet();
		return;
	}

	loaderLabel->setText("Getting area's temperature...");
//	QNetworkRequest request = QNetworkRequest();
	QNetworkRequest request = QNetworkRequest();

	//http://api.worldweatheronline.com/free/v1/weather.ashx?q=London&format=json&num_of_days=5&key=tc4feefhdfuh75f6pvupfnhz
	QString url = QString(
			"http://api.worldweatheronline.com/free/v1/weather.ashx?q=");
	url.append(QString::number(m_latitude));
	url.append(",");
	url.append(QString::number(m_longitude));
	url.append("&format=json&key=");
	url.append(key_weather);
	url.append("&num_of_days=8");
	qDebug() << "doRequestWeather URL: " + url;
	request.setUrl(QUrl(url));
	QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(
			this);
	bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
			this, SLOT(onRequestFinishedWeather(QNetworkReply*)));
	Q_ASSERT(res);
	Q_UNUSED(res);
	networkAccessManager->get(request);
}

void ApplicationUI::onRequestFinishedWeather(QNetworkReply* reply) {
	qDebug() << "--onRequestFinishedWeather()";

	QString response;

	if (reply) {
		qDebug()
				<< QString(" StatusCode:")
						+ reply->attribute(
								QNetworkRequest::HttpStatusCodeAttribute).toString();
		if (reply->error() == QNetworkReply::NoError) {
			QByteArray buffer = reply->readAll();
			QString str = QString::fromUtf8(buffer);
			qDebug() << "Response: " + str;

			if (str == "") {
				qDebug() << "str = null";
				return;
			}

			settings->setValue("timeRequestData", getTimeMillis());
			timeRequestData = settings->value("timeRequestData").toString();

			//parse json
			bb::data::JsonDataAccess ja;
			QVariant jsonva = ja.loadFromBuffer(str);

			QVariantMap map = jsonva.toMap(); //.find("browser")->toMap();
			if (map.contains("data")) {
				QVariantMap data = map.find("data")->toMap();
				if (data.contains("current_condition")) {
					QVariantMap currentData =
							(data.find("current_condition")->toList()).at(0).toMap();
					if (currentData.contains("temp_C")) {
						temp_C = currentData.value("temp_C").toString();
						qDebug() << "temp_C = " + temp_C;
						settings->setValue("temp_C", temp_C);
					}
					if (currentData.contains("temp_F")) {
						temp_F = currentData.value("temp_F").toString();
						qDebug() << "temp_F = " + temp_F;
						settings->setValue("temp_F", temp_F);
					}
					if (currentData.contains("weatherCode")) {
						weatherCode =
								currentData.value("weatherCode").toString();
						qDebug() << "weatherCode = " + weatherCode;
						settings->setValue("weatherCode", weatherCode);
					}
					if (currentData.contains("weatherDesc")) {
						QVariantMap weatherDesc = (currentData.find(
								"weatherDesc")->toList()).at(0).toMap();
						if (weatherDesc.contains("value")) {
							weatherDescValue =
									weatherDesc.value("value").toString();
							qDebug()
									<< "weatherDescValue = " + weatherDescValue;
							settings->setValue("weatherDescValue",
									weatherDescValue);
						} else {
							qDebug()
									<< "=== Can't find tag value in weatherDesc";
						}
					}
					if (currentData.contains("cloudcover")) {
						cloudcover = currentData.value("cloudcover").toString();
						qDebug() << "cloudcover = " + cloudcover;
						settings->setValue("cloudcover", cloudcover);
					}
					if (currentData.contains("humidity")) {
						humidity = currentData.value("humidity").toString();
						qDebug() << "humidity = " + humidity;
						settings->setValue("humidity", humidity);
					}
					if (currentData.contains("precipMM")) {
						precipMM = currentData.value("precipMM").toString();
						qDebug() << "precipMM = " + precipMM;
						settings->setValue("precipMM", precipMM);
					}
					if (currentData.contains("pressure")) {
						pressure = currentData.value("pressure").toString();
						qDebug() << "pressure = " + pressure;
						settings->setValue("pressure", pressure);
					}
					if (currentData.contains("visibility")) {
						visibility = currentData.value("visibility").toString();
						qDebug() << "visibility = " + visibility;
						settings->setValue("visibility", visibility);
					}
					if (currentData.contains("winddir16Point")) {
						winddir16Point =
								currentData.value("winddir16Point").toString();
						qDebug() << "winddir16Point = " + winddir16Point;
						settings->setValue("winddir16Point", winddir16Point);
					}
					if (currentData.contains("winddirDegree")) {
						temp_C = currentData.value("winddirDegree").toString();
						qDebug() << "winddirDegree = " + winddirDegree;
						settings->setValue("winddirDegree", winddirDegree);
					}
					if (currentData.contains("windspeedKmph")) {
						windspeedKmph =
								currentData.value("windspeedKmph").toString();
						qDebug() << "windspeedKmph = " + windspeedKmph;
						settings->setValue("windspeedKmph", windspeedKmph);
					}
					if (currentData.contains("windspeedMiles")) {
						windspeedMiles =
								currentData.value("windspeedMiles").toString();
						qDebug() << "windspeedMiles = " + windspeedMiles;
						settings->setValue("windspeedMiles", windspeedMiles);
					}
				}

				if (data.contains("weather")) {
					QVariantList weathers = data.find("weather")->toList();

					qDebug()
							<< "weathers.size() = "
									+ QString::number(weathers.size());

					for (int i = 0; i < weathers.size(); i++) {
						QVariantMap weather = weathers.at(i).toMap();
						ForecastObject forecastObj = ForecastObject();
						forecastObj.setTime(timeRequestData);
						if (weather.contains("date")) {
							forecastObj.setDate(
									weather.value("date").toString());
						}
						if (weather.contains("precipMM")) {
							forecastObj.setPrecipMm(
									weather.value("precipMM").toString());
						}
						if (weather.contains("tempMaxC")) {
							forecastObj.setTempMaxC(
									weather.value("tempMaxC").toString());
						}
						if (weather.contains("tempMaxF")) {
							forecastObj.setTempMaxF(
									weather.value("tempMaxF").toString());
						}
						if (weather.contains("tempMinC")) {
							forecastObj.setTempMinC(
									weather.value("tempMinC").toString());
						}
						if (weather.contains("tempMinF")) {
							forecastObj.setTempMinF(
									weather.value("tempMinF").toString());
						}
						if (weather.contains("weatherCode")) {
							forecastObj.setWeatherCode(
									weather.value("weatherCode").toString());
						}
						if (weather.contains("weatherDesc")) {
							QVariantMap weatherDesc = (weather.find(
									"weatherDesc")->toList()).at(0).toMap();
							if (weatherDesc.contains("value")) {
								forecastObj.setWeatherDesc(
										weatherDesc.value("value").toString());
							}
						}
						if (weather.contains("winddir16Point")) {
							forecastObj.setWinddir16Point(
									weather.value("winddir16Point").toString());
						}
						if (weather.contains("winddirDegree")) {
							forecastObj.setWinddirDegree(
									weather.value("winddirDegree").toString());
						}
						if (weather.contains("winddirection")) {
							forecastObj.setWinddirection(
									weather.value("winddirection").toString());
						}
						if (weather.contains("windspeedKmph")) {
							forecastObj.setWindspeedKmph(
									weather.value("windspeedKmph").toString());
						}
						if (weather.contains("windspeedMiles")) {
							forecastObj.setWindspeedMiles(
									weather.value("windspeedMiles").toString());
						}
						_db->insertForecast(forecastObj);
					}
				}
			}

			countError = 0;
//			doRequestLocation();
			doRequestCountry();

		} else {
			qDebug() << QString("Error: ") + reply->errorString();
//			showErrorInternet();
			countError++;
			doRequestWeather();
		}
	} else {
		//no reply
//		showErrorInternet();
		countError++;
		doRequestWeather();
	}

	// Deletes the reply the next time the event loop is entered
	reply->deleteLater();
}

void ApplicationUI::doRequestLocation() {
	qDebug() << "doRequestLocation()";

	if (countError == 3) {
		showErrorInternet();
		return;
	}

//	loaderLabel->setText("Getting informations around your area...");
	//	QNetworkRequest request = QNetworkRequest();
	QNetworkRequest request = QNetworkRequest();

	//http://api.worldweatheronline.com/free/v1/search.ashx?q=London&format=json&key_weather=tc4feefhdfuh75f6pvupfnhz
//	QString url = QString(
//			"http://api.worldweatheronline.com/free/v1/search.ashx?q=");
//	url.append(QString::number(m_latitude));
//	url.append(",");
//	url.append(QString::number(m_longitude));
//	url.append("&format=json&key_weather=");
//	url.append(key_weather);

	//https://maps.googleapis.com/maps/api/place/nearbysearch/json?language=en&location=13.805492,100.641198&radius=500&sensor=false&key=AIzaSyBuJweYhLFmX3fvlKhWkv6lnZqNlItSlcs
	QString url =
			QString(
					"https://maps.googleapis.com/maps/api/place/nearbysearch/json?language=en&location=");
	url.append(QString::number(m_latitude));
	url.append(",");
	url.append(QString::number(m_longitude));
	url.append("&radius=500&sensor=false&key=");
	url.append(key_google);

	qDebug() << "doRequestLocation URL: " + url;
	request.setUrl(QUrl(url));
	QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(
			this);
	bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
			this, SLOT(onRequestFinishedLocation(QNetworkReply*)));
	Q_ASSERT(res);
	Q_UNUSED(res);
	networkAccessManager->get(request);
}

void ApplicationUI::onRequestFinishedLocation(QNetworkReply* reply) {
	qDebug() << "--onRequestFinishedLocation()";

	QString response;

	if (reply) {
		qDebug()
				<< QString(" StatusCode:")
						+ reply->attribute(
								QNetworkRequest::HttpStatusCodeAttribute).toString();
		if (reply->error() == QNetworkReply::NoError) {
			QByteArray buffer = reply->readAll();
			QString str = QString::fromUtf8(buffer);
			qDebug() << "Response: " + str;

			if (str == "") {
				qDebug() << "str = null";
				return;
			}

			//parse json
			bb::data::JsonDataAccess ja;
			QVariant jsonva = ja.loadFromBuffer(str);

			QVariantMap map = jsonva.toMap();

			if (map.contains("results")) {
				QVariantList resultList = map.find("results")->toList();

				for (int i = 0; i < resultList.length(); i++) {
					QVariantMap result = (resultList.at(i)).toMap();
					qDebug() << "--------[" + QString::number(i) + "]-------- ";
					LocationObject locationObject = LocationObject();
					if (result.contains("name")) {
						QString name = result.value("name").toString();
						qDebug() << "name: " + name;
						locationObject.setAreaName(name);
					}
					if (result.contains("geometry")) {
						QVariantMap geometry = result.find("geometry")->toMap();
						if (geometry.contains("location")) {
							QVariantMap location =
									geometry.find("location")->toMap();
							if (location.contains("lat")) {
								QString lat = location.value("lat").toString();
								qDebug() << "lat: " + lat;
								locationObject.setLatitude(lat.toDouble());
							}
							if (location.contains("lng")) {
								QString lng = location.value("lng").toString();
								qDebug() << "lng: " + lng;
								locationObject.setLongitude(lng.toDouble());
							}
						}
					}
					if (result.contains("vicinity")) {
						QString vicinity = result.value("vicinity").toString();
						qDebug() << "vicinity: " + vicinity;
						locationObject.setRegion(vicinity);
					}
					locationObject.setCountry(country);
					locationObject.setCountry2(country2);
					locationObject.setIsUserLocation("n");
					locationObject.setCreatetime(getTimeMillis());

					_db->insertOrUpdateLocation(locationObject);
				}

			}
			countError = 0;
			getAllData();

		} else {
			qDebug() << QString("Error: ") + reply->errorString();
			countError++;
			doRequestLocation();
		}

	} else {
		countError++;
		doRequestLocation();
//		showErrorInternet();
	}

	reply->deleteLater();
}

void ApplicationUI::doRequestCountry() {
	qDebug() << "doRequestCountry()";

	if (countError == 3) {
		showErrorInternet();
		return;
	}

	loaderLabel->setText("Getting informations around your area...");
	//	QNetworkRequest request = QNetworkRequest();
	QNetworkRequest request = QNetworkRequest();

	//http://api.worldweatheronline.com/free/v1/search.ashx?q=London&format=json&key_weather=tc4feefhdfuh75f6pvupfnhz
	QString url = QString(
			"http://api.worldweatheronline.com/free/v1/search.ashx?q=");
	url.append(QString::number(m_latitude));
	url.append(",");
	url.append(QString::number(m_longitude));
	url.append("&format=json&key=");
	url.append(key_weather);

	//https://maps.googleapis.com/maps/api/place/nearbysearch/json?language=en&location=13.805492,100.641198&radius=500&sensor=false&key=AIzaSyBuJweYhLFmX3fvlKhWkv6lnZqNlItSlcs
//	QString url =
//			QString(
//					"https://maps.googleapis.com/maps/api/place/nearbysearch/json?language=en&location=");
//	url.append(QString::number(m_latitude));
//	url.append(",");
//	url.append(QString::number(m_longitude));
//	url.append("&radius=500&sensor=false&key=");
//	url.append(key_google);

	qDebug() << "doRequestCountry URL: " + url;
	request.setUrl(QUrl(url));
	QNetworkAccessManager *networkAccessManager = new QNetworkAccessManager(
			this);
	bool res = connect(networkAccessManager, SIGNAL(finished(QNetworkReply*)),
			this, SLOT(onRequestFinishedCountry(QNetworkReply*)));
	Q_ASSERT(res);
	Q_UNUSED(res);
	networkAccessManager->get(request);
}

void ApplicationUI::onRequestFinishedCountry(QNetworkReply* reply) {
	qDebug() << "--onRequestFinishedCountry()";

	QString response;

	if (reply) {
		qDebug()
				<< QString(" StatusCode:")
						+ reply->attribute(
								QNetworkRequest::HttpStatusCodeAttribute).toString();
		if (reply->error() == QNetworkReply::NoError) {
			QByteArray buffer = reply->readAll();
			QString str = QString::fromUtf8(buffer);
			qDebug() << "Response: " + str;

			if (str == "") {
				qDebug() << "str = null";
				return;
			}

			//parse json
			bb::data::JsonDataAccess ja;
			QVariant jsonva = ja.loadFromBuffer(str);

			QVariantMap map = jsonva.toMap();

			if (map.contains("search_api")) {
				QVariantMap search_api = map.find("search_api")->toMap();
				if (search_api.contains("result")) {
					QVariantList resultList =
							search_api.find("result")->toList();

//					for (int i = 0; i < resultList.length(); i++) {
					QVariantMap result = (resultList.at(0)).toMap();
//						LocationObject locationObject = LocationObject();
//						if (result.contains("areaName")) {
//							QVariantMap areaName =
//									(result.find("areaName")->toList()).at(0).toMap();
//							if (areaName.contains("value")) {
//								QString areaNameValue =
//										areaName.value("value").toString();
//								locationObject.setAreaName(areaNameValue);
//							} else {
//								qDebug()
//										<< " Can't find areaName.contains(value)";
//							}
//						}
					if (result.contains("country")) {
						QVariantMap countryT =
								(result.find("country")->toList()).at(0).toMap();
						if (countryT.contains("value")) {
							country = countryT.value("value").toString();
							qDebug() << "--> country = " + country;
							settings->setValue("country", country);
//								locationObject.setCountry(countryValue);
						} else {
							qDebug() << " Can't find country.contains(value)";
						}
					}
//						if (result.contains("latitude")) {
//							QString latitude =
//									result.value("latitude").toString();
//							locationObject.setLatitude(latitude.toDouble());
//						}
//						if (result.contains("longitude")) {
//							QString longitude =
//									result.value("longitude").toString();
//							locationObject.setLongitude(longitude.toDouble());
//						}
					if (result.contains("weatherUrl")) {
						QVariantMap weatherUrl =
								(result.find("weatherUrl")->toList()).at(0).toMap();
						if (weatherUrl.contains("value")) {
							QString weatherUrlValue =
									weatherUrl.value("value").toString();
							//substring
							//http:\/\/www.worldweatheronline.com\/Ban-Bang-Toei-weather\/Krung-Thep\/TH.aspx
							country2 = weatherUrlValue.mid(
									weatherUrlValue.indexOf(".aspx") - 2, 2);
							qDebug() << "--> country2 = " + country2;
							settings->setValue("country2", country2);
						} else {
							qDebug()
									<< " Can't find weatherUrl.contains(value)";
						}
					}
//						_db->insertOrUpdateLocation(locationObject);
//					}

				}
			}
			doRequestLocation();
//			getAllData();

		} else {
			qDebug() << QString("Error: ") + reply->errorString();
//			showErrorInternet();
			countError++;
			doRequestCountry();
		}
	} else {
		//no reply
		countError++;
		doRequestCountry();
//		showErrorInternet();
	}

	// Deletes the reply the next time the event loop is entered
	reply->deleteLater();
}

void ApplicationUI::getAllData() {
	qDebug() << "---------------> getAllData()";

	if (isShowTutorial()) {
		setShowTutorial(false);
	} else {
		closeHelp();
	}

//get old data
	timeRequestData = settings->value("timeRequestData").toString();
	temp_C = settings->value("temp_C").toString();
	temp_F = settings->value("temp_F").toString();
	weatherCode = settings->value("weatherCode").toString();
	cloudcover = settings->value("cloudcover").toString();
	humidity = settings->value("humidity").toString();
	precipMM = settings->value("precipMM").toString();
	pressure = settings->value("pressure").toString();
	visibility = settings->value("visibility").toString();
	winddir16Point = settings->value("winddir16Point").toString();
	winddirDegree = settings->value("winddirDegree").toString();
	windspeedKmph = settings->value("windspeedKmph").toString();
	windspeedMiles = settings->value("windspeedMiles").toString();

	weatherDescValue = settings->value("weatherDescValue").toString();
	if (isDay) {
		weatherIconPath = dayIconMap.value(weatherCode).toString();
	} else {
		weatherIconPath = nightIconMap.value(weatherCode).toString();
	}

	qDebug() << " timeRequestData: " + timeRequestData;
	qDebug() << " temp_C: " + temp_C;
	qDebug() << " temp_F: " + temp_F;
	qDebug() << " weatherCode: " + weatherCode;
	qDebug() << " cloudcover: " + cloudcover;
	qDebug() << " humidity: " + humidity;
	qDebug() << " precipMM: " + precipMM;
	qDebug() << " pressure: " + pressure;
	qDebug() << " visibility: " + visibility;
	qDebug() << " winddir16Point: " + winddir16Point;
	qDebug() << " winddirDegree: " + winddirDegree;
	qDebug() << " windspeedKmph: " + windspeedKmph;
	qDebug() << " windspeedMiles: " + windspeedMiles;
	qDebug() << " isDay: " + QString::number(isDay);
	qDebug() << " weatherDescValue: " + weatherDescValue;
	qDebug() << " weatherIconPath: " + weatherIconPath;

	refreshLocation();
	refreshForecast();

	foreCastQueue = QVariantList();
	nowDay = dateNow.day();
	nowDayOfWeek = dateNow.dayOfWeek();
	qDebug() << "nowDay = " + QString::number(nowDay);
	qDebug() << "nowDayOfWeek = " + QString::number(nowDayOfWeek);
	int d = nowDayOfWeek;
	//0=Sun, 1=Mon
	qDebug() << "nowDayOfWeek = " + QString::number(nowDayOfWeek);
	qDebug() << "forecastList.size() = " + QString::number(forecastList.size());
	for (int i = 0; i < forecastList.size(); i++) {
		QVariantMap forecast = forecastList.at(i).toMap();
//		QString dmyText = forecast.value("date").toString();
//		QString dText = dmyText.mid(dmyText.lastIndexOf('-') + 1);
//		if(dText.length()>1 && dText.startsWith("0")){
//			dText = dText.mid(1);
//		}
//		qDebug() << "dText = " + dText;
//		if (dText.toInt() > nowDay) {

//			int d = dText.toInt() - nowDay + nowDayOfWeek;

		if (d >= 7) {
			d -= 7;
		}
		QString dow = "";
		QString fdow = "";
		if (d == 0) {
			dow = "Sun";
			fdow = "Sunday";
		} else if (d == 1) {
			dow = "Mon";
			fdow = "Monday";
		} else if (d == 2) {
			dow = "Tue";
			fdow = "Tuesday";
		} else if (d == 3) {
			dow = "Wed";
			fdow = "Wednesday";
		} else if (d == 4) {
			dow = "Thu";
			fdow = "Thursday";
		} else if (d == 5) {
			dow = "Fri";
			fdow = "Friday";
		} else if (d == 6) {
			dow = "Sat";
			fdow = "Saturday";
		}
		QVariantMap map = QVariantMap();
		map.insert("dayOfWeek", dow);
		map.insert("fullDayOfWeek", fdow);
		map.insert("tempMaxC", forecast.value("tempMaxC").toString());
		map.insert("tempMinC", forecast.value("tempMinC").toString());
		map.insert("tempMaxF", forecast.value("tempMaxF").toString());
		map.insert("tempMinF", forecast.value("tempMinF").toString());
		map.insert("weatherCode", forecast.value("weatherCode").toString());
//			qDebug() << "insert weatherCode = "+ forecast.value("weatherCode").toString();
		foreCastQueue.append(map);
//		}

		d++;
	}

	QVariantMap locationObj = (locationList.last()).toMap();
	areaName = locationObj.value("areaName").toString();
	qDebug() << " areaName: " + areaName;
//	country = locationObj.value("country").toString();
//	region = locationObj.value("region").toString();

	isLoadComplete = false;

	QThread* thread = new QThread;
	PaintWorker1 *pw = new PaintWorker1();
	pw->moveToThread(thread);
	connect(thread, SIGNAL(started()), pw, SLOT(processRender()));
	connect(pw, SIGNAL(finishedRender()), thread, SLOT(quit()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer1()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer2()));
	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer3()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer4()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer5()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer6()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer7()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer8()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer9()));
//	connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer10()));
	connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	thread->start();

//	QFuture<void> future = QtConcurrent::run(&ApplicationUI::renderContainer1);

	isLoadComplete = true;

	if (loaderContainer) {
		loaderLabel->setText("Complete!");
		loaderContainer->setVisible(false);
	}
}

//void ApplicationUI::onPaintWorker1Error(){
//
//}
//
//void ApplicationUI::onPaintWorker1Finished(){
//
//}

void ApplicationUI::testWorkerRender(bb::cascades::Container* container) {
	PaintWorker1 *pw = new PaintWorker1(container, topY, imageWidth,
			imageHeight, areaName, QString(temp_C + "*C"), weatherDescValue);
//	pw.startRender();

	QThread* thread = new QThread;
	pw->moveToThread(thread);

	bool res = connect(thread, SIGNAL(started()), pw, SLOT(processRender()));
	if (res) {
		qDebug() << "CONNECT started with processRender OK";
	} else {
		qDebug() << "CONNECT started with processRender FAILED";
	}

	// Connect worker finished signal to trigger thread quit, then delete.
	res = connect(pw, SIGNAL(finishedRender()), thread, SLOT(quit()));
	if (res) {
		qDebug() << "CONNECT finishedRender with quit OK";
	} else {
		qDebug() << "CONNECT finishedRender with quit FAILED";
	}

//		res = connect(pw, SIGNAL(finishedRender()), pw, SLOT(onRenderFinished()));
	res = connect(pw, SIGNAL(finishedRender()), this, SLOT(renderContainer1()));
	if (res) {
		qDebug() << "CONNECT finishedRender with onRenderFinished OK";
	} else {
		qDebug() << "CONNECT finishedRender with onRenderFinished FAILED";
	}
	//	connect(this, SIGNAL(finishedRender()), app, SLOT(onPaintWorker1Finished()));

	// Make sure the thread object is deleted after execution has finished.
	res = connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));
	if (res) {
		qDebug() << "CONNECT finished with deleteLater OK";
	} else {
		qDebug() << "CONNECT finished with deleteLater FAILED";
	}

	Q_ASSERT(res);
	Q_UNUSED(res);

	thread->start();
}

void ApplicationUI::renderContainer1() {

	mainContainer->removeAll();

	//black layer
	Container *bg = new Container();
	bg->setPreferredSize(imageWidth / 2 + 40, imageHeight);
	bg->setBackground(Color::Black);
	bg->setOpacity(0.2);
	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
	bgProperties->setPositionX(0.0);
	bgProperties->setPositionY(topY);
	bg->setLayoutProperties(bgProperties);
	mainContainer->add(bg);
	bgProperties = 0;
	bg = 0;

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	int x1 = 40; //120;
	int y1 = topY + 120;
	drawTextImage(x1, y1, 250, tempText, 0, FONT_WINTER, mainContainer, true,
			1.0);

	QString iconfileName;
	QImageReader reader;
	QImage icon;
	QSize iconSize;
	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
	qDebug() << "iconfileName = " + iconfileName;
	int y2 = y1 + 230;
	reader.setFileName(iconfileName);
	icon = reader.read();
	iconSize = icon.size();
	int newHeight = 200;
	int newWidth = (newHeight * iconSize.width()) / iconSize.height();
	ImageView* imageView = ImageView::create(iconfileName);
	imageView->setPreferredSize(newWidth, newHeight);
	AbsoluteLayoutProperties* imgProperties =
			AbsoluteLayoutProperties::create();
	imgProperties->setPositionX(x1 - 10);
	imgProperties->setPositionY(y2);
	imageView->setLayoutProperties(imgProperties);
	imgProperties = 0;
	mainContainer->add(imageView);
	imageView = 0;

	int x2 = x1 + newWidth + 20;
	y2 += 20;
	QString textDesc = weatherDescValue;
	if (textDesc.indexOf(' ') != -1) {
		//substring
		while (textDesc.indexOf(' ') != -1) {
			QString text = textDesc.left(textDesc.indexOf(' '));
			drawTextImage(x2, y2, 80, text, 0, FONT_WINTER, mainContainer,
					false, 1.0);
			y2 += 80;
			qDebug() << "list append: " + text;
			textDesc = textDesc.right(textDesc.indexOf(' '));
		}
		drawTextImage(x2, y2, 80, textDesc, 0, FONT_WINTER, mainContainer,
				false, 1.0);
		qDebug() << "list append: " + textDesc;
	} else {
		drawTextImage(x2, y2, 80, textDesc, 0, FONT_WINTER, mainContainer,
				false, 1.0);
		qDebug() << "list append: " + textDesc;
	}

	QString max = "";
	QString min = "";
	QVariantMap f = forecastList.at(0).toMap();
	if (settings->value("temp").toString() == "c") {
		max = f.value("tempMaxC").toString() + "*C";
		min = f.value("tempMinC").toString() + "*C";
	} else {
		max = f.value("tempMaxF").toString() + "*F";
		min = f.value("tempMinF").toString() + "*F";
	}
	int y3 = y2 + newHeight - 80;
	drawTextImage(x1 + 20, y3, 80, max + "/" + min, 0, FONT_WINTER,
			mainContainer, true, 1.0);

	int x3 = 40;
	int y4 = topY + 1024 - 20 - 130;
	drawTextImage(x3, y4, 130, areaName, 600, FONT_CANDAL, mainContainer, false,
			1.0);
//
	int y5 = y4 - 60;
	QString word = dateNowText + "  " + timeNowText;
	qDebug() << "word = " + word;
	drawTextImage(x3, y5, 60, word, 0, FONT_WINTER, mainContainer, false, 1.0);

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer2() {

	mainContainer2->removeAll();

	int y0 = topY + ((imageHeight * 2) / 3);
	//black layer
	Container *bg = new Container();
	bg->setPreferredSize(imageWidth, imageHeight / 3);
	bg->setBackground(Color::Black);
	bg->setOpacity(0.2);
	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
	bgProperties->setPositionX(0.0);
	bgProperties->setPositionY(y0);
	bg->setLayoutProperties(bgProperties);
	mainContainer2->add(bg);
	bgProperties = 0;
	bg = 0;

	int x1 = 30;
	int y1 = topY + 30;
	drawTextImage(x1, y1, 60, dateNowText, 0, FONT_WINTER, mainContainer2,
			false, 1.0);
	y1 += 60;
	drawTextImage(x1, y1, 60, timeNowText, 0, FONT_WINTER, mainContainer2,
			false, 1.0);

	QString iconfileName;
	QImageReader reader;
	QImage icon;
	QSize iconSize;
	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
//	qDebug() << "iconfileName = " + iconfileName;
//	int y2 = y1+230;
	reader.setFileName(iconfileName);
	icon = reader.read();
	iconSize = icon.size();
	int nh = 200;
	int nw = (nh * iconSize.width()) / iconSize.height();
	ImageView* imageView = ImageView::create(iconfileName);
	imageView->setPreferredSize(nw, nh);
	AbsoluteLayoutProperties* imgProperties =
			AbsoluteLayoutProperties::create();

	int x2 = 768 - nw - 40;
	int y3 = y0 - nh / 2 + 30;
	imgProperties->setPositionX(x2);
	imgProperties->setPositionY(y3);
	imageView->setLayoutProperties(imgProperties);
	imgProperties = 0;
	mainContainer2->add(imageView);
	imageView = 0;

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	int x3 = 540;
	int y4 = y0 + nh / 2 + 20;
	drawTextImage(x3, y4, 160, tempText, 0, FONT_WINTER, mainContainer2, true,
			1.0);

	iconfileName = assetFolder + "images/redpin_big" + ".png";
	reader.setFileName(iconfileName);
	icon = reader.read();
	iconSize = icon.size();
	ImageView* imageView2 = ImageView::create(iconfileName);
	int newHeight = 160;
	int newWidth = (newHeight * iconSize.width()) / iconSize.height();
	imageView2->setPreferredSize(newWidth, newHeight);
	AbsoluteLayoutProperties* imgProperties2 =
			AbsoluteLayoutProperties::create();

	int x4 = 50;
	int y5 = y0 - iconSize.height() / 2 + 90;

	qDebug() << "renderContainer2";
	qDebug() << "y5: " + QString::number(y5);
	qDebug() << "iconSize.height(): " + QString::number(iconSize.height());

	imgProperties2->setPositionX(x4);
	imgProperties2->setPositionY(y5);
	imageView2->setLayoutProperties(imgProperties2);
	imgProperties2 = 0;
	mainContainer2->add(imageView2);
	imageView2 = 0;

	int y6 = y5 + newHeight + 20;
	int x5 = x4 - 30;
	drawTextImage(x5, y6, 120, areaName, 370, FONT_CANDAL, mainContainer2,
			false, 1.0);

	y6 += 120 - 20;
	drawTextImage(x5, y6, 80, country, 370, FONT_CANDAL, mainContainer2, false,
			1.0);

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer2, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer3() {

	mainContainer3->removeAll();
//black layer

	int y0 = topY + (imageHeight / 2) - 30;
	Container *bg = new Container();
	bg->setPreferredSize(imageWidth, (imageHeight / 2) + 30);
	bg->setBackground(Color::Black);
	bg->setOpacity(0.2);
	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
	bgProperties->setPositionX(0.0);
	bgProperties->setPositionY(y0);
	bg->setLayoutProperties(bgProperties);
	mainContainer3->add(bg);
	bgProperties = 0;
	bg = 0;

	int xd = 30;
	int yd = topY + 30;
	drawTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, mainContainer3,
			false, 1.0);
	yd += 60;
	drawTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, mainContainer3,
			false, 1.0);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	int x1 = 30;
	int y1 = y0 + 20;
	drawTextImage(x1, y1, 80, weatherDescValue + " / " + tempText, 650,
			FONT_CANDAL, mainContainer3, true, 1.0);

	y1 += 60;
	drawTextImage(x1, y1, 150, areaName, 580, FONT_CANDAL, mainContainer3,
			false, 1.0);

	y1 += 180;
	int x2 = 128;
	if (foreCastQueue.size() > 0) {

		for (int i = 1; i < 4; i++) {
			QVariantMap map = foreCastQueue.at(i).toMap();
			QString iconfileName;
			QImageReader reader;
			QImage icon;
			QSize iconSize;
			QString path = "";

			qDebug()
					<< "map.value(weatherCode) = "
							+ map.value("weatherCode").toString();
			if (isDay) {
				path =
						dayIconMap.value(map.value("weatherCode").toString()).toString();

			} else {
				path =
						nightIconMap.value(map.value("weatherCode").toString()).toString();

			}
			iconfileName = assetFolder + "images/weather/" + path + ".png";
			reader.setFileName(iconfileName);
			icon = reader.read();
			iconSize = icon.size();
			ImageView* imageView = ImageView::create(iconfileName);
			imageView->setPreferredSize(150, 150);
			AbsoluteLayoutProperties* imgProperties =
					AbsoluteLayoutProperties::create();

			int yy = y1 - 30;
			imgProperties->setPositionX(x2 - 75);
			imgProperties->setPositionY(yy);
			imageView->setLayoutProperties(imgProperties);
			imgProperties = 0;
			mainContainer3->add(imageView);
			imageView = 0;
			yy += 150;

			drawTextImage(x2 - 25, yy, 70, (map.value("dayOfWeek").toString()),
					0, FONT_WINTER, mainContainer3, false, 1.0);
			yy += 70;

			QString max = "";
			QString min = "";
			if (settings->value("temp").toString() == "c") {
				max = map.value("tempMaxC").toString() + "*C";
				min = map.value("tempMinC").toString() + "*C";
			} else {
				max = map.value("tempMaxF").toString() + "*F";
				min = map.value("tempMinF").toString() + "*F";
			}
			drawTextImage(x2 - 90, yy, 70, max + "/" + min, 0, FONT_WINTER,
					mainContainer3, true, 1.0);

			x2 += 240;
		}
	}

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer3, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer4() {

	mainContainer4->removeAll();

	Container *bg = new Container();
	bg->setPreferredSize(imageWidth, imageHeight / 2 - 70);
	bg->setBackground(Color::Black);
	bg->setOpacity(0.2);
	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
	bgProperties->setPositionX(0.0);
	bgProperties->setPositionY(topY);
	bg->setLayoutProperties(bgProperties);
	mainContainer4->add(bg);
	bgProperties = 0;
	bg = 0;

	int x0 = 40;
	int y0 = topY + 30;
	drawTextImage(x0, y0, 170, areaName, 500, FONT_CANDAL, mainContainer4,
			false, 1.0);

	y0 += 190 - 50;
	drawTextImage(x0, y0, 90, country, 600, FONT_CANDAL, mainContainer4, false,
			1.0);

	y0 += 80;

	iconfileName = assetFolder + "images/termometer.png";
	int x00 = renderImageView(mainContainer4, iconfileName, 130, x0, y0 + 40,
			1.0);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	drawTextImage(x00 + 10, y0 + 20, 160, tempText, 0, FONT_WINTER,
			mainContainer4, true, 1.0);

	int x1 = 768 - 280;
	int y1 = topY + imageHeight / 2 - 150 - 80;
	//pressure
	QString pathPressure = assetFolder + "images/pressure.png";
	int xP = renderImageView(mainContainer4, pathPressure, 60, x1, y1, 1.0);
	drawTextImage(xP + 20, y1, 60, pressure + " hPa", 0, FONT_WINTER,
			mainContainer4, false, 1.0);

	y1 += 60 + 10;
	//precipMM
	QString pathPrecipMM = assetFolder + "images/rain.png";
	renderImageView(mainContainer4, pathPrecipMM, 60, x1, y1, 1.0);
	drawTextImage(xP + 20, y1, 60, precipMM + " mm", 0, FONT_WINTER,
			mainContainer4, false, 1.0);

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer4, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer5() {

	mainContainer5->removeAll();

	int y0 = topY + ((imageHeight * 2) / 3);
	//black layer
	Container *bg = new Container();
	bg->setPreferredSize(imageWidth, imageHeight / 3);
	bg->setBackground(Color::Black);
	bg->setOpacity(0.2);
	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
	bgProperties->setPositionX(0.0);
	bgProperties->setPositionY(y0);
	bg->setLayoutProperties(bgProperties);
	mainContainer5->add(bg);
	bgProperties = 0;
	bg = 0;

	int x1 = 30;
	int y1 = topY + 30;
	drawTextImage(x1, y1, 60, dateNowText, 0, FONT_WINTER, mainContainer5,
			false, 1.0);
	y1 += 60;
	drawTextImage(x1, y1, 60, timeNowText, 0, FONT_WINTER, mainContainer5,
			false, 1.0);

	int x0 = 30;
	y0 -= 80;
	drawTextImage(x0, y0, 160, areaName, 380, FONT_CANDAL, mainContainer5,
			false, 1.0);
	drawTextImage(x0, y0 + 140, 80, weatherDescValue, 500, FONT_CANDAL,
			mainContainer5, false, 1.0);

	iconfileName = assetFolder + "images/redpin_big" + ".png";
	renderImageView(mainContainer5, iconfileName, 180, 600, y0 + 20, 1.0);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}

	int y3 = y0 + 240;

	iconfileName = assetFolder + "images/termometer.png";
	int x2 = renderImageView(mainContainer5, iconfileName, 120, x0, y3 + 30,
			1.0);

	drawTextImage(x2 + 10, y3, 160, tempText, 0, FONT_WINTER, mainContainer5,
			true, 1.0);

	y3 += 10;
	//pressure
	QString pathPressure = assetFolder + "images/pressure.png";
	int xP = renderImageView(mainContainer5, pathPressure, 60, 500, y3, 1.0);
	drawTextImage(xP + 20, y3, 60, pressure + " hPa", 0, FONT_WINTER,
			mainContainer5, false, 1.0);

	y3 += 60 + 10;
	//precipMM
	QString pathPrecipMM = assetFolder + "images/rain.png";
	renderImageView(mainContainer5, pathPrecipMM, 60, 500, y3, 1.0);
	drawTextImage(xP + 20, y3, 60, precipMM + " mm", 0, FONT_WINTER,
			mainContainer5, false, 1.0);

//	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
//	renderImageView(mainContainer5, iconfileName, 200, 550, y3-30, 1.0);

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer5, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer6() {

	mainContainer6->removeAll();

	int delta = 50;

	//black layer
	int y0 = topY + imageHeight / 2 + delta;
	Container *bg = new Container();
	bg->setPreferredSize(imageWidth, imageHeight / 2 - delta);
	bg->setBackground(Color::Black);
	bg->setOpacity(0.2);
	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
	bgProperties->setPositionX(0.0);
	bgProperties->setPositionY(y0);
	bg->setLayoutProperties(bgProperties);
	mainContainer6->add(bg);
	bgProperties = 0;
	bg = 0;

	int xd = 30;
	int yd = topY + 30;
	drawTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, mainContainer6,
			false, 1.0);
	yd += 60;
	drawTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, mainContainer6,
			false, 1.0);

	int x0 = 30;
	QString pathLine = assetFolder + "images/line.png";
	renderImageView(mainContainer6, pathLine, 0, 0, y0 + 120, 1.0);
	drawTextImage(x0, y0, 150, areaName, 550, FONT_CANDAL, mainContainer6,
			false, 1.0);

	int y1 = y0 + 130;
	renderImageView(mainContainer6, pathLine, 0, 0, y1 + 90, 1.0);
	drawTextImage(x0, y1, 100,
			getTempText() + " / " + weatherDescValue.toUpper(), 650,
			FONT_WINTER, mainContainer6, true, 1.0);

	int y2 = y1 + 130;
	//pressure
	QString pathPressure = assetFolder + "images/pressure.png";
	int xP = renderImageView(mainContainer6, pathPressure, 60, x0, y2, 1.0);
	int xPt = drawTextImage(xP, y2, 60, pressure + " hPa", 0, FONT_WINTER,
			mainContainer6, false, 1.0);

	//humidity
	QString pathHumidity = assetFolder + "images/humidity.png";
	int xH = renderImageView(mainContainer6, pathHumidity, 60, xPt + 60, y2,
			1.0);
	int xHt = drawTextImage(xH, y2, 60, humidity + " %", 0, FONT_WINTER,
			mainContainer6, false, 1.0);

	//wind speed
	QString pathWind = assetFolder + "images/wind.png";
	int xW = renderImageView(mainContainer6, pathWind, 60, xHt + 60, y2, 1.0);
	drawTextImage(xW, y2, 60, windspeedKmph + " km/h", 0, FONT_WINTER,
			mainContainer6, false, 1.0);

	int y3 = y2 + 80;
	//precipMM
	QString pathPrecipMM = assetFolder + "images/rain.png";
	renderImageView(mainContainer6, pathPrecipMM, 60, x0, y3, 1.0);
	drawTextImage(xP, y3, 60, precipMM + " mm", 0, FONT_WINTER, mainContainer6,
			false, 1.0);

	//visibility
	QString pathVisibility = assetFolder + "images/visibility.png";
	renderImageView(mainContainer6, pathVisibility, 60, xPt + 60, y3, 1.0);
	drawTextImage(xH, y3, 60, visibility + " m", 0, FONT_WINTER, mainContainer6,
			false, 1.0);

	//wind direction
	QString pathDirection = assetFolder + "images/direction.png";
	renderImageView(mainContainer6,pathDirection,60, xHt+60, y3, 1.0);
	QString directionText = getDirectionText(true);
	drawTextImage(xW, y3, 60, directionText, 0, FONT_WINTER, mainContainer6,
			false, 1.0);

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer6, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer7() {

	mainContainer7->removeAll();

	int xd = 30;
	int yd = topY + 30;
	drawTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, mainContainer7,
			false, 1.0);
	yd += 60;
	drawTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, mainContainer7,
			false, 1.0);

	int x0 = (imageWidth - 500) / 2;
	int y0 = topY + (imageHeight - 500) / 2;
//	int y0 = topY + ((imageHeight - 80 - 120 - 20) - 500)/2 + 50;
	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
	renderImageView(mainContainer7, iconfileName, 500, x0, y0, 0.7);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	int width1 = getWidthText(80, tempText + " | " + weatherDescValue,
			FONT_WINTER, 0, true);
	int x1 = (768 - width1) / 2;
	int y1 = topY + imageHeight - 80 - 120 - 20;
	drawTextImage(x1, y1, 80, tempText + " | " + weatherDescValue, 0,
			FONT_WINTER, mainContainer7, true, 1.0);

	y1 += 80;

	QString aName = areaName;
	if (aName.length() > 12) {
		aName = aName.left(12);
		aName = aName + "...";
	}
	qDebug() << "aName = " + aName;

	int width2 = getWidthText(120, aName + ", " + country2, FONT_WINTER, 0,
			true);
	int x2 = (768 - width2) / 2;
	drawTextImage(x2, y1, 120, aName + ", " + country2, 0, FONT_WINTER,
			mainContainer7, true, 1.0);

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer7, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer8() {

	mainContainer8->removeAll();
////black layer
//	Container *bg = new Container();
//	bg->setPreferredSize(imageWidth, imageHeight / 2);
//	bg->setBackground(Color::Black);
//	bg->setOpacity(0.2);
//	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
//	bgProperties->setPositionX(0.0);
//	bgProperties->setPositionY(topY + imageHeight / 2);
//	bg->setLayoutProperties(bgProperties);
//	mainContainer8->add(bg);
//	bgProperties = 0;
//	bg = 0;

	int xd = 30;
	int yd = topY + 30;
	drawTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, mainContainer8,
			false, 1.0);
	yd += 60;
	drawTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, mainContainer8,
			false, 1.0);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*";
	} else {
		tempText = temp_F + "*";
	}
	getWidthText(350, tempText, FONT_CANDAL, 0, true);
	int x1 = 30;
	int y1 = topY + (imageHeight - 350);
	int x2 = drawTextImage(x1, y1, 350, tempText, 0, FONT_WINTER,
			mainContainer8, true, 1.0);

	x2 -= 40;
	int y2 = topY + (imageHeight - 100 - 40);
	drawTextImage(x2, y2, 100, areaName, 650, FONT_WINTER, mainContainer8, true,
			1.0);

	int x3 = 768 - 200 - 30;
	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
	renderImageView(mainContainer8, iconfileName, 200, x3, y1, 1.0);

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer8, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer9() {

	mainContainer9->removeAll();

	int x0 = 30;
	int y0 = topY + (imageHeight * 3) / 4 - 50;

	//black layer
	Container *bg = new Container();
	bg->setPreferredSize(imageWidth, imageHeight / 4 + 50);
	bg->setBackground(Color::Black);
	bg->setOpacity(0.2);
	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
	bgProperties->setPositionX(0.0);
	bgProperties->setPositionY(y0);
	bg->setLayoutProperties(bgProperties);
	mainContainer9->add(bg);
	bgProperties = 0;
	bg = 0;

	int xd = 30;
	int yd = topY + 30;
	drawTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, mainContainer9,
			false, 1.0);
	yd += 60;
	drawTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, mainContainer9,
			false, 1.0);

	drawTextImage(x0, y0, 100, areaName, 600, FONT_CANDAL, mainContainer9,
			false, 1.0);

	int y1 = y0 + 90;
	iconfileName = assetFolder + "images/direction.png";
	int x1 = renderImageView(mainContainer9, iconfileName, 100, x0, y1, 1.0);
	QString directionText = getDirectionText(true);
	int x2 = drawTextImage(x0, y1 + 100, 80, directionText, 0, FONT_WINTER,
			mainContainer9, false, 1.0);
	int x3 = 0;
	if (x1 >= x2) {
		x3 = x1 + 10;
	} else {
		x3 = x2 + 10;
	}

	int x4 = drawTextImage(x3, y1 - 15, 250, windspeedKmph, 0, FONT_CANDAL,
			mainContainer9, false, 1.0) + 10;
	drawTextImage(x4, y1 + 70, 120, "km/h", 0, FONT_WINTER, mainContainer9,
			false, 1.0);

	int x5 = 550;
	int y2 = y0 + (topY + imageHeight - y0 - 50 - 10 - 50 - 10 - 50) / 2 + 30;
	//temperature
	QString pathTemp = assetFolder + "images/termometer.png";
	int xP = renderImageView(mainContainer9, pathTemp, 50, x5 + 15, y2, 1.0);
	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	xP += 30;
	drawTextImage(xP, y2, 50, tempText, 0, FONT_WINTER, mainContainer9, true,
			1.0);
	y2 += 50 + 10;
	//pressure
	QString pathPressure = assetFolder + "images/pressure.png";
	renderImageView(mainContainer9, pathPressure, 50, x5, y2, 1.0);
	drawTextImage(xP, y2, 50, pressure + " hPa", 0, FONT_WINTER, mainContainer9,
			false, 1.0);
	y2 += 50 + 10;
	//precipMM
	QString pathPrecipMM = assetFolder + "images/rain.png";
	renderImageView(mainContainer9, pathPrecipMM, 50, x5, y2, 1.0);
	drawTextImage(xP, y2, 50, precipMM + " mm", 0, FONT_WINTER, mainContainer9,
			false, 1.0);

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer9, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::renderContainer10() {

	mainContainer10->removeAll();

//	//black layer
//	Container *bg = new Container();
//	bg->setPreferredSize(imageWidth, imageHeight / 2);
//	bg->setBackground(Color::Black);
//	bg->setOpacity(0.2);
//	AbsoluteLayoutProperties* bgProperties = AbsoluteLayoutProperties::create();
//	bgProperties->setPositionX(0.0);
//	bgProperties->setPositionY(topY + imageHeight / 2);
//	bg->setLayoutProperties(bgProperties);
//	mainContainer10->add(bg);
//	bgProperties = 0;
//	bg = 0;

	//draw area, country
	int x0 = 20;
	int y0 = topY + 40;
	drawTextImage(x0, y0, 150, areaName, 550, FONT_CANDAL, mainContainer10,
			false, 1.0);
	y0 += 150;
	drawTextImage(x0, y0 - 20, 80, country, 550, FONT_CANDAL, mainContainer10,
			false, 1.0);

	int y1 = y0 + 80;
	int x1 = 350;
	int x2 = 510;
	if (foreCastQueue.size() > 0) {

		qDebug()
				<< "foreCastQueue.size() = "
						+ QString::number(foreCastQueue.size());

		for (int i = 0; i < 7; i++) {
			QVariantMap map = foreCastQueue.at(i).toMap();

			drawTextImage(x0, y1 + 10, 80,
					(map.value("fullDayOfWeek").toString()), x1, FONT_WINTER,
					mainContainer10, false, 1.0);
			QString path = "";
			if (isDay) {
				path =
						dayIconMap.value(map.value("weatherCode").toString()).toString();
			} else {
				path =
						nightIconMap.value(map.value("weatherCode").toString()).toString();
			}
			iconfileName = assetFolder + "images/weather/" + path + ".png";
			renderImageView(mainContainer10, iconfileName, 90, x1, y1, 1.0);

			QString max = "";
			QString min = "";
			if (settings->value("temp").toString() == "c") {
				max = map.value("tempMaxC").toString() + "*C";
				min = map.value("tempMinC").toString() + "*C";
			} else {
				max = map.value("tempMaxF").toString() + "*F";
				min = map.value("tempMinF").toString() + "*F";
			}
			drawTextImage(x2, y1 + 10, 80, max + "/" + min, 0, FONT_WINTER,
					mainContainer10, true, 1.0);

			if (i <= 5) {
				//draw line
				iconfileName = assetFolder + "images/line.png";
				renderImageView(mainContainer10, iconfileName, 14, 0, y1 + 90,
						1.0);
			}
			y1 += 110;
		}
	}

	//draw logo
	int xl = 480;
	int yl = topY + 5;
	iconfileName = assetFolder + "images/logotran.png";
	renderImageView(mainContainer10, iconfileName, 40, xl, yl, 0.5);
}

int ApplicationUI::renderImageView(Container *_container, QString iconfileName,
		int newHeight, int x, int y, float opacity) {

	QImageReader reader;
	QImage icon;
	QSize iconSize;
	reader.setFileName(iconfileName);
	icon = reader.read();
	iconSize = icon.size();
	ImageView* imageView = ImageView::create(iconfileName);
	int newWidth = iconSize.width();
	if (newHeight > 0) {
		newWidth = (newHeight * iconSize.width()) / iconSize.height();
	}
	imageView->setPreferredSize(newWidth, newHeight);
	AbsoluteLayoutProperties* imgProperties =
			AbsoluteLayoutProperties::create();

	imgProperties->setPositionX(x);
	imgProperties->setPositionY(y);
	imageView->setLayoutProperties(imgProperties);
	imageView->setOpacity(opacity);
	imgProperties = 0;
	_container->add(imageView);
	imageView = 0;
	return x + newWidth;
}

QString ApplicationUI::getTempText() {
	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	return tempText;
}

QString ApplicationUI::getDirectionText(bool needShort) {
	qDebug() << "winddir16Point = " + winddir16Point;
	QString text = "";
	if (winddir16Point == "N") {
		text = "North";
	} else if (winddir16Point == "NE" || winddir16Point == "EN"
			|| winddir16Point == "NEN" || winddir16Point == "ENE") {
		if(needShort){
			text = "N.East";
		}else{
			text = "North East";
		}
	} else if (winddir16Point == "E") {
		text = "East";
	} else if (winddir16Point == "SE" || winddir16Point == "ES"
			|| winddir16Point == "SES" || winddir16Point == "ESE") {
		if(needShort){
			text = "S.East";
		}else{
			text = "South East";
		}
	} else if (winddir16Point == "S") {
		text = "South";
	} else if (winddir16Point == "SW" || winddir16Point == "WS"
			|| winddir16Point == "WSW" || winddir16Point == "SWS") {
		if(needShort){
			text = "S.West";
		}else{
			text = "South West";
		}
	} else if (winddir16Point == "W") {
		text = "West";
	} else if (winddir16Point == "NW" || winddir16Point == "WN"
			|| winddir16Point == "NWN" || winddir16Point == "WNW") {
		if(needShort){
			text = "N.West";
		}else{
			text = "North West";
		}
	}
	qDebug() << "text: " + text;
	return text;
}

int ApplicationUI::getWidthText(int newHeight, QString wording,
		QString folderIcon, int maxWidth, bool isTemp) {
	int newWidth;
	QString character;
	QString iconfileName;
	QImageReader reader;
	QImage icon;
	QSize iconSize;

	int x = 0;

	for (int i = 0; i < wording.length(); ++i) {
		//limited width of
		if (maxWidth > 0 && x > maxWidth) {
			character = "_dot";
			iconfileName = assetFolder + "images/fontimage/" + folderIcon + "/"
					+ character + ".png";

			reader.setFileName(iconfileName);
			icon = reader.read();
			iconSize = icon.size();
			newWidth = (newHeight * iconSize.width()) / iconSize.height();

			for (int j = 0; j < 3; j++) {
				x += newWidth;
			}
			break;
		}
		character = wording[i];

		if (character == "@") {
			character = "_at";
		} else if (character == ":") {
			character = "_colon";
		} else if (character == ",") {
			character = "_comma";
		} else if (character == "\"") {
			character = "_dbquote";
		} else if (character == ".") {
			character = "_dot";
		} else if (character == "\'") {
			character = "_quote";
		} else if (character == ";") {
			character = "_semicolon";
		} else if (character == "#") {
			character = "_sharp";
		} else if (character == "/") {
			character = "_sl";
		} else if (character == " ") {
			character = "_space";
		} else if (character == "*") {
			if (isTemp) {
				character = "_temp";
			} else {
				character = "_star";
			}
		} else if (character == "(") {
			character = "_bl";
		} else if (character == ")") {
			character = "_br";
		} else if (character == "&") {
			character = "_and";
		} else if (character == "!") {
			character = "_exclamation";
		} else if (character == "?") {
			character = "_question";
		} else if (character == "-") {
			character = "_minus";
		} else if (character == "=") {
			character = "_equal";
		} else if (character == "+") {
			character = "_plus";
		} else if (character == "~") {
			character = "_tilde";
		} else if (character == "`") {
			character = "_grave";
		} else if (character == "[") {
			character = "_lsb";
		} else if (character == "]") {
			character = "_rsb";
		} else if (character == "{") {
			character = "_lcb";
		} else if (character == "}") {
			character = "_rcb";
		} else if (character == "<") {
			character = "_less";
		} else if (character == ">") {
			character = "_more";
		} else if (character == "%") {
			character = "_percent";
		} else if (character == "_") {
			character = "_under";
		} else if (character == "÷") {
			character = "_divide";
		} else if (character == "±") {
			character = "_plusminus";
		} else if (character == "•") {
			character = "_dotup";
		} else if (character == "\\") {
			character = "_bsl";
		} else if (character == "|") {
			character = "_line";
		} else if (character == "¤") {
			character = "_ostar";
		} else if (character == "«") {
			character = "_less2";
		} else if (character == "»") {
			character = "_more2";
		} else if (character == "¥") {
			character = "_yen";
		} else if (character == "€") {
			character = "_euro";
		} else if (character == "£") {
			character = "_pound";
		} else if (character == "$") {
			character = "_dollar";
		} else if (character == "¡") {
			character = "_iexc";
		} else if (character == "¿") {
			character = "_iques";
		} else {
			character = checkCase(character);
		}
		iconfileName = assetFolder + "images/fontimage/" + folderIcon + "/"
				+ character + ".png";

		reader.setFileName(iconfileName);
		icon = reader.read();
		iconSize = icon.size();
		newWidth = (newHeight * iconSize.width()) / iconSize.height();

		x += newWidth;
	}

	return x;
}

int ApplicationUI::drawTextImage(int x, int y, int newHeight, QString wording,
		int maxWidth, QString folderIcon, Container *container, bool isTemp,
		float opacity) {
//	qDebug()
//			<< "drawTextImage() x:" + QString::number(x) + ", y:"
//					+ QString::number(y) + ", newHeight:"
//					+ QString::number(newHeight) + ", wording:" + wording
//					+ ", maxWidth:" + maxWidth + ", folderIcon:" + folderIcon;

//start position
	int newWidth;
	QString character;
	QString iconfileName;
	QImageReader reader;
	QImage icon;
	QSize iconSize;

	for (int i = 0; i < wording.length(); ++i) {
		//limited width of
		if (maxWidth > 0 && x > maxWidth) {
			character = "_dot";
			iconfileName = assetFolder + "images/fontimage/" + folderIcon + "/"
					+ character + ".png";

			reader.setFileName(iconfileName);
			icon = reader.read();
			iconSize = icon.size();
			newWidth = (newHeight * iconSize.width()) / iconSize.height();

			for (int j = 0; j < 3; j++) {
				ImageView* imageView = ImageView::create(iconfileName);
				imageView->setPreferredHeight(newHeight);
				imageView->setPreferredWidth(newWidth);

				AbsoluteLayoutProperties* imgProperties =
						AbsoluteLayoutProperties::create();
				imgProperties->setPositionX(x);
				imgProperties->setPositionY(y);
				imageView->setLayoutProperties(imgProperties);
				imageView->setOpacity(opacity);
				imgProperties = 0;

				container->add(imageView);
				imageView = 0;

				x += newWidth;
			}
			break;
		}
		character = wording[i];

		if (character == "@") {
			character = "_at";
		} else if (character == ":") {
			character = "_colon";
		} else if (character == ",") {
			character = "_comma";
		} else if (character == "\"") {
			character = "_dbquote";
		} else if (character == ".") {
			character = "_dot";
		} else if (character == "\'") {
			character = "_quote";
		} else if (character == ";") {
			character = "_semicolon";
		} else if (character == "#") {
			character = "_sharp";
		} else if (character == "/") {
			character = "_sl";
		} else if (character == " ") {
			character = "_space";
		} else if (character == "*") {
			if (isTemp) {
				character = "_temp";
			} else {
				character = "_star";
			}
		} else if (character == "(") {
			character = "_bl";
		} else if (character == ")") {
			character = "_br";
		} else if (character == "&") {
			character = "_and";
		} else if (character == "!") {
			character = "_exclamation";
		} else if (character == "?") {
			character = "_question";
		} else if (character == "-") {
			character = "_minus";
		} else if (character == "=") {
			character = "_equal";
		} else if (character == "+") {
			character = "_plus";
		} else if (character == "~") {
			character = "_tilde";
		} else if (character == "`") {
			character = "_grave";
		} else if (character == "[") {
			character = "_lsb";
		} else if (character == "]") {
			character = "_rsb";
		} else if (character == "{") {
			character = "_lcb";
		} else if (character == "}") {
			character = "_rcb";
		} else if (character == "<") {
			character = "_less";
		} else if (character == ">") {
			character = "_more";
		} else if (character == "%") {
			character = "_percent";
		} else if (character == "_") {
			character = "_under";
		} else if (character == "÷") {
			character = "_divide";
		} else if (character == "±") {
			character = "_plusminus";
		} else if (character == "•") {
			character = "_dotup";
		} else if (character == "\\") {
			character = "_bsl";
		} else if (character == "|") {
			character = "_line";
		} else if (character == "¤") {
			character = "_ostar";
		} else if (character == "«") {
			character = "_less2";
		} else if (character == "»") {
			character = "_more2";
		} else if (character == "¥") {
			character = "_yen";
		} else if (character == "€") {
			character = "_euro";
		} else if (character == "£") {
			character = "_pound";
		} else if (character == "$") {
			character = "_dollar";
		} else if (character == "¡") {
			character = "_iexc";
		} else if (character == "¿") {
			character = "_iques";
		} else {
			character = checkCase(character);
		}
		iconfileName = assetFolder + "images/fontimage/" + folderIcon + "/"
				+ character + ".png";

		reader.setFileName(iconfileName);
		icon = reader.read();
		iconSize = icon.size();
		newWidth = (newHeight * iconSize.width()) / iconSize.height();

		ImageView* imageView = ImageView::create(iconfileName);
		imageView->setPreferredHeight(newHeight);
		imageView->setPreferredWidth(newWidth);

		AbsoluteLayoutProperties* imgProperties =
				AbsoluteLayoutProperties::create();
		imgProperties->setPositionX(x);
		imgProperties->setPositionY(y);
		imageView->setLayoutProperties(imgProperties);
		imageView->setOpacity(opacity);
		imgProperties = 0;

		container->add(imageView);
		imageView = 0;

		x += newWidth;
	}

	return x;
}

QString ApplicationUI::checkCase(QString c) {
//	qDebug() << "checkCase(): "+c;
	if (c == "A" || c == "B" || c == "C" || c == "D" || c == "E" || c == "F"
			|| c == "G" || c == "H" || c == "I" || c == "J" || c == "K"
			|| c == "L" || c == "M" || c == "N" || c == "O" || c == "P"
			|| c == "Q" || c == "R" || c == "S" || c == "T" || c == "U"
			|| c == "V" || c == "W" || c == "X" || c == "Y" || c == "Z"
			|| c == "0" || c == "1" || c == "2" || c == "3" || c == "4"
			|| c == "5" || c == "6" || c == "7" || c == "8" || c == "9") {
//		qDebug() << "return = "+c;
		return c;
	} else {
//		qDebug() << "return = "+c+"_";
		return c + "_";
	}

}

void ApplicationUI::refreshDesign(bool next) {
	if (next) {
//		indexLocation++;
//		if (indexLocation > (locationList.length() - 1)) {
//			indexLocation = 0;
//		}
		designIndex++;
		if (designIndex > designNum) {
			designIndex = 0;
		}
	} else {
//		indexLocation--;
//		if (indexLocation < 0) {
//			indexLocation = locationList.length() - 1;
//		}
		designIndex--;
		if (designIndex < 0) {
			designIndex = designNum;
		}
	}
//	beginRenderImage();

//	mainContainer->setVisible(false);
//	mainContainer2->setVisible(false);
//	mainContainer3->setVisible(false);
//	mainContainer4->setVisible(false);
//	mainContainer5->setVisible(false);
//	mainContainer6->setVisible(false);
//	mainContainer7->setVisible(false);
//	mainContainer8->setVisible(false);
//	mainContainer9->setVisible(false);
//	mainContainer10->setVisible(false);

	if (designIndex == 0) {
//		if (!loadCon1) {
//			renderContainer1();
//		}
//		mainContainer->setVisible(true);
//		testWorkerRender(mainContainer);
	} else if (designIndex == 1) {
//		if (!loadCon2) {
//			renderContainer2();
//		}
//		mainContainer2->setVisible(true);
//		testWorkerRender(mainContainer2);
	} else if (designIndex == 2) {
//		if (!loadCon3) {
//			renderContainer3();
//		}
//		mainContainer3->setVisible(true);
//		testWorkerRender(mainContainer3);
	} else if (designIndex == 3) {
//		if (!loadCon4) {
//			renderContainer4();
//		}
//		mainContainer4->setVisible(true);
//		testWorkerRender(mainContainer4);
	} else if (designIndex == 4) {
//		if (!loadCon5) {
//			renderContainer5();
//		}
//		mainContainer5->setVisible(true);
//		testWorkerRender(mainContainer5);
	} else if (designIndex == 5) {
//		if (!loadCon6) {
//			renderContainer6();
//		}
//		mainContainer6->setVisible(true);
//		testWorkerRender(mainContainer6);
	} else if (designIndex == 6) {
//		if (!loadCon7) {
//			renderContainer7();
//		}
//		mainContainer7->setVisible(true);
//		testWorkerRender(mainContainer7);
	} else if (designIndex == 7) {
//		if (!loadCon8) {
//			renderContainer8();
//		}
//		mainContainer8->setVisible(true);
//		testWorkerRender(mainContainer8);
	} else if (designIndex == 8) {
//		if (!loadCon9) {
//			renderContainer9();
//		}
//		mainContainer9->setVisible(true);
//		testWorkerRender(mainContainer9);
	} else if (designIndex == 9) {
//		if (!loadCon10) {
//			renderContainer10();
//		}
//		mainContainer10->setVisible(true);
//		testWorkerRender(mainContainer10);
	}

}

void ApplicationUI::showError(QString wordBody) {
	popupError = new SystemDialog("OK");
	QString wordHead("Error!");
	popupError->setTitle(wordHead);
	popupError->setBody(wordBody);
	popupError->show();
}

void ApplicationUI::showErrorInternet() {
	popupError = new SystemDialog("OK");
	QString wordHead("Error!");
	popupError->setTitle(wordHead);
	QString wordBody("Internet connection problem. Please try again later.");
	popupError->setBody(wordBody);
	popupError->show();
}

void ApplicationUI::positionUpdateTimeout() {
	qDebug() << "positionUpdateTimeout() received";
	m_positionSource->startUpdates();
}

void ApplicationUI::onShutterFired() {
	soundplayer_play_sound("event_camera_shutter");
}

void ApplicationUI::showPhotoInCard(const QString fileName) {
// Create InvokeManager and InvokeRequest objects to able to invoke a card with a viewer that will show the
// latest bomber photo.
	bb::system::InvokeManager manager;
	bb::system::InvokeRequest request;

// Setup what to show and in what target.
	request.setUri(QUrl::fromLocalFile(fileName));
	request.setTarget("sys.pictures.card.previewer");
	request.setAction("bb.action.VIEW");
	InvokeTargetReply *targetReply = manager.invoke(request);
//setting the parent to "this" will make the manager live on after this function is destroyed
	manager.setParent(this);

	if (targetReply == NULL) {
		qDebug() << "InvokeTargetReply is NULL: targetReply = " << targetReply;
	} else {
		targetReply->setParent(this);
	}
}

void ApplicationUI::saveImage() {
	qDebug() << "saveImage()";

	onShutterFired();
	//write image to camera folder

	//copy to camera foldeimagefileName
	reader.setFileName(selectedFilePath);
	icon = reader.read();
	QString pathSave = QDir::currentPath() + "/shared/camera/" + imagefileName;
	qDebug() << "pathSave: " + pathSave;
	if (icon.save(pathSave, "JPG")) {

		deleteSelectedFilePath();
//		QDir dir(selectedFilePath);
//		if (dir.exists()) {
//			if (dir.remove(imagefileName)) {
//				qDebug() << "REMOVE SUCCESS";
//			} else {
//				qDebug() << "REMOVE FAIL";
//			}
//		}else{
//
//		}

//		selectedFilePath = "";
//		imagefileName = "";

		manipulatePhoto(pathSave);
		deleteSelectedFilePath();
		setSelectedFilePath(lastedPath);  //create new one
	} else {
		showError("Can't save image. Please try again.");
	}

}

void ApplicationUI::manipulatePhoto(const QString &fileName) {
	qDebug()
			<< "manipulatePhoto() designIndex = "
					+ QString::number(designIndex);

	QImageReader reader;

// Set image name from the given file name.
	reader.setFileName(fileName);
	QImage image = reader.read();
	image = image.scaledToWidth(768, Qt::SmoothTransformation);
	QSize imageSize = image.size();
	qDebug() << "-------> image width = " + QString::number(imageSize.width());
	qDebug()
			<< "-------> image height = " + QString::number(imageSize.height());

//
//    // Gray it out! (this is not the gray-scale algorithm that should be used)
//	for (int i = 0; i < imageSize.width(); i++)
//        for (int ii = 0; ii < imageSize.height(); ii++) {
//            color = QColor(image.pixel(i, ii));
//            color.setRed((color.red() + color.green() + color.blue()) / 3);
//            color.setGreen(color.red());
//            color.setBlue(color.red());
//            image.setPixel(i, ii, color.rgb());
//        }
//    }

	QPainter painter;
	if (cameraMode && !cameraRear) {
		qDebug() << "CAPTURE FRONT CAMERA";
		QColor color;
//		QImage newImage = reader.read();
//		newImage = newImage.scaledToWidth(768, Qt::SmoothTransformation);
		QImage newImage(imageSize.width(), imageSize.height(),
				QImage::Format_ARGB32_Premultiplied);
		//Front Camera >> need to reflect image
		for (int y = 0; y < imageSize.height(); y++) {
			for (int x = 0; x < imageSize.width(); x++) {
				color = QColor(image.pixel(x, y));
				newImage.setPixel(imageSize.width() - x - 1, y, color.rgb());
			}
		}
		QPainter painter(&newImage);
		if (designIndex == 0) {
			paintImage1(painter, imageSize);
		} else if (designIndex == 1) {
			paintImage2(painter, imageSize);
		} else if (designIndex == 2) {
			paintImage3(painter, imageSize);
		} else if (designIndex == 3) {
			paintImage4(painter, imageSize);
		} else if (designIndex == 4) {
			paintImage5(painter, imageSize);
		} else if (designIndex == 5) {
			paintImage6(painter, imageSize);
		} else if (designIndex == 6) {
			paintImage7(painter, imageSize);
		} else if (designIndex == 7) {
			paintImage8(painter, imageSize);
		} else if (designIndex == 8) {
			paintImage9(painter, imageSize);
		} else if (designIndex == 9) {
			paintImage10(painter, imageSize);
		}
		newImage.save(fileName, "JPG");
		showPhotoInCard(fileName);
	} else {
		qDebug() << "CAPTURE REAR CAMERA";
		QPainter painter(&image);
		if (designIndex == 0) {
			paintImage1(painter, imageSize);
		} else if (designIndex == 1) {
			paintImage2(painter, imageSize);
		} else if (designIndex == 2) {
			paintImage3(painter, imageSize);
		} else if (designIndex == 3) {
			paintImage4(painter, imageSize);
		} else if (designIndex == 4) {
			paintImage5(painter, imageSize);
		} else if (designIndex == 5) {
			paintImage6(painter, imageSize);
		} else if (designIndex == 6) {
			paintImage7(painter, imageSize);
		} else if (designIndex == 7) {
			paintImage8(painter, imageSize);
		} else if (designIndex == 8) {
			paintImage9(painter, imageSize);
		} else if (designIndex == 9) {
			paintImage10(painter, imageSize);
		}
		image.save(fileName, "JPG");
		showPhotoInCard(fileName);
	}

//	QFile res(assetFolder+"font/Langdon.otf");
//	int fontID = QFontDatabase::addApplicationFontFromData(res.readAll());
//	qDebug() << "fontId = "+QString::number(fontID);

// Paint an image on top of another image, so we add the gray-scaled image first.
//	QPainter painter(&image);

//	if (designIndex == 0) {
//			paintImage1(painter, imageSize);
//		} else if (designIndex == 1) {
//			paintImage2(painter, imageSize);
//		} else if (designIndex == 2) {
//			paintImage3(painter, imageSize);
//		} else if (designIndex == 3) {
//			paintImage4(painter, imageSize);
//		} else if (designIndex == 4) {
//			paintImage5(painter, imageSize);
//		} else if (designIndex == 5) {
//			paintImage6(painter, imageSize);
//		} else if (designIndex == 6) {
//			paintImage7(painter, imageSize);
//		} else if (designIndex == 7) {
//			paintImage8(painter, imageSize);
//		} else if (designIndex == 8) {
//			paintImage9(painter, imageSize);
//		} else if (designIndex == 9) {
//			paintImage10(painter, imageSize);
//		}

//    reader.setFileName(bomberFileName);
//    QImage bombimage = reader.read();

//    QTransform myTransform;
//    myTransform.rotate(180);
//    bombimage = bombimage.transformed(myTransform);

//    QFont font = merger.font();
//    qDebug() << "font family: "+font.family();

//QPoint point = QPoint( 10, 20 );
//merger.drawText( point, "You can draw text from a point..." );

//	QString iconFolder = assetFolder + "images/fontimage/first/";
//	QString iconfileName;
//	int xIcon = 0;
//	for (int i = 0; i < 5; i++) {
//		if (i == 0) {
//			iconfileName = iconFolder + "H.png";
//		} else if (i == 1) {
//			iconfileName = iconFolder + "e.png";
//		} else if (i == 2) {
//			iconfileName = iconFolder + "l.png";
//		} else if (i == 3) {
//			iconfileName = iconFolder + "l.png";
//		} else if (i == 4) {
//			iconfileName = iconFolder + "o.png";
//		}
//		reader.setFileName(iconfileName);
//		QImage icon = reader.read();
//
//		// Read image current size.
//		QSize iconSize = icon.size();
//
//		painter.drawImage(xIcon, 2000, icon);
//		xIcon += iconSize.width();
//	}

//	if(!cameraMode){
//		//copy to camera foldeimagefileName
//		reader.setFileName(selectedFilePath);
//		icon = reader.read();
//		QString pathSave = QDir::currentPath() + "/shared/camera/"+imagefileName;
//		qDebug() << "pathSave: "+pathSave;
//		if(icon.save(pathSave,"JPG")){
//
//		// Show the photo by using this function that take use of the InvokeManager.
//			showPhotoInCard(pathSave);
//
//		}else{
//			showError("Can't save image. Please try again.");
//		}
//
//		//delete selectFilePath
//		QDir dir(selectedFilePath);
//		if(dir.exists()){
//			if(dir.remove(imagefileName)){
//				qDebug() << "REMOVE SUCCESS";
//			}else{
//				qDebug() << "REMOVE FAIL";
//			}
//		}
//		selectedFilePath = "";
//		imagefileName = "";
//
//
//	}else{
//	image.save(fileName, "JPG");
//
//	// Show the photo by using this function that take use of the InvokeManager.
//	showPhotoInCard(fileName);
}

int ApplicationUI::paintTextImage(int x, int y, int newHeight, QString wording,
		int maxWidth, QString folderIcon, QPainter &painter, bool isTemp,
		float opacity) {
//	qDebug()
//			<< "paintTextImage() x:" + QString::number(x) + ", y:"
//					+ QString::number(y) + ", newHeight:"
//					+ QString::number(newHeight) + ", wording:" + wording
//					+ ", maxWidth:" + maxWidth + ", folderIcon:" + folderIcon;

//start position
//	int newWidth;
	painter.setOpacity(opacity);
	for (int i = 0; i < wording.length(); ++i) {
		//limited width of
		if (maxWidth > 0 && x > maxWidth) {
			character = "_dot";
			iconfileName = assetFolder + "images/fontimage/" + folderIcon + "/"
					+ character + ".png";
			reader.setFileName(iconfileName);
			icon = reader.read();
			iconSize = icon.size();
//			newWidth = (newHeight * iconSize.width()) / iconSize.height();

			icon = icon.scaledToHeight(newHeight, Qt::SmoothTransformation); //(newWidth,newHeight,Qt::KeepAspectRatio, Qt::SmoothTransformation);
			iconSize = icon.size();
			for (int j = 0; j < 3; j++) {
				painter.drawImage(x, y, icon);
				x += iconSize.width();
			}
			break;
		}
		character = wording[i];

		if (character == "@") {
			character = "_at";
		} else if (character == ":") {
			character = "_colon";
		} else if (character == ",") {
			character = "_comma";
		} else if (character == "\"") {
			character = "_dbquote";
		} else if (character == ".") {
			character = "_dot";
		} else if (character == "\'") {
			character = "_quote";
		} else if (character == ";") {
			character = "_semicolon";
		} else if (character == "#") {
			character = "_sharp";
		} else if (character == "/") {
			character = "_sl";
		} else if (character == " ") {
			character = "_space";
		} else if (character == "*") {
			if (isTemp) {
				character = "_temp";
			} else {
				character = "_star";
			}
		} else if (character == "(") {
			character = "_bl";
		} else if (character == ")") {
			character = "_br";
		} else if (character == "&") {
			character = "_and";
		} else if (character == "!") {
			character = "_exclamation";
		} else if (character == "?") {
			character = "_question";
		} else if (character == "-") {
			character = "_minus";
		} else if (character == "=") {
			character = "_equal";
		} else if (character == "+") {
			character = "_plus";
		} else if (character == "~") {
			character = "_tilde";
		} else if (character == "`") {
			character = "_grave";
		} else if (character == "[") {
			character = "_lsb";
		} else if (character == "]") {
			character = "_rsb";
		} else if (character == "{") {
			character = "_lcb";
		} else if (character == "}") {
			character = "_rcb";
		} else if (character == "<") {
			character = "_less";
		} else if (character == ">") {
			character = "_more";
		} else if (character == "%") {
			character = "_percent";
		} else if (character == "_") {
			character = "_under";
		} else if (character == "÷") {
			character = "_divide";
		} else if (character == "±") {
			character = "_plusminus";
		} else if (character == "•") {
			character = "_dotup";
		} else if (character == "\\") {
			character = "_bsl";
		} else if (character == "|") {
			character = "_line";
		} else if (character == "¤") {
			character = "_ostar";
		} else if (character == "«") {
			character = "_less2";
		} else if (character == "»") {
			character = "_more2";
		} else if (character == "¥") {
			character = "_yen";
		} else if (character == "€") {
			character = "_euro";
		} else if (character == "£") {
			character = "_pound";
		} else if (character == "$") {
			character = "_dollar";
		} else if (character == "¡") {
			character = "_iexc";
		} else if (character == "¿") {
			character = "_iques";
		} else {
			character = checkCase(character);
		}
		iconfileName = assetFolder + "images/fontimage/" + folderIcon + "/"
				+ character + ".png";

		reader.setFileName(iconfileName);
		icon = reader.read();
//		iconSize = icon.size();
//		newWidth = (newHeight * iconSize.width()) / iconSize.height();
		if (newHeight > 0) {
			icon = icon.scaledToHeight(newHeight, Qt::SmoothTransformation); //(newWidth,newHeight,Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
		}
		iconSize = icon.size();
//		qDebug() << "WIDTH = " + QString::number(iconSize.width());

		painter.drawImage(x, y, icon);
		x += iconSize.width();
	}

	return x;
}

void ApplicationUI::paintBG(QPainter &painter, int x, int y, int width,
		int height, QColor &color, float opacity) {
	painter.setOpacity(opacity);
	painter.fillRect(x, y, width, height, QBrush(color, Qt::SolidPattern));
	painter.setOpacity(1.0);
}

int ApplicationUI::paintImageView(QPainter &painter, QString iconfileName,
		int newHeight, int x, int y, float opacity) {
	reader.setFileName(iconfileName);
	icon = reader.read();
	if (newHeight > 0) {
		icon = icon.scaledToHeight(newHeight, Qt::SmoothTransformation);
	}
	iconSize = icon.size();
	painter.setOpacity(opacity);
	painter.drawImage(x, y, icon);
	x += iconSize.width();
	return x;
}

void ApplicationUI::paintImage1(QPainter &painter, QSize &size) {

	int imageWidth = size.width();
	int imageHeight = size.height();

	//black layer
	QColor color(0, 0, 0);
	paintBG(painter, 0, 0, imageWidth / 2 + 40, imageHeight, color, 0.2);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	int x1 = 40;
	int y1 = 120;
	paintTextImage(x1, y1, 250, tempText, 0, FONT_WINTER, painter, true, 1.0);

	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
	int y2 = y1 + 230;
	int xr = paintImageView(painter, iconfileName, 200, x1 - 10, y2, 1.0);

	int x2 = x1 + (xr - (x1 - 10)) + 20;
	y2 += 20;
	QString textDesc = weatherDescValue;
	if (textDesc.indexOf(' ') != -1) {
		//substring
		while (textDesc.indexOf(' ') != -1) {
			QString text = textDesc.left(textDesc.indexOf(' '));
			paintTextImage(x2, y2, 80, text, 0, FONT_WINTER, painter, false,
					1.0);
			y2 += 80;
			qDebug() << "list append: " + text;
			textDesc = textDesc.right(textDesc.indexOf(' '));
		}
		paintTextImage(x2, y2, 80, textDesc, 0, FONT_WINTER, painter, false,
				1.0);
		qDebug() << "list append: " + textDesc;
	} else {
		paintTextImage(x2, y2, 80, textDesc, 0, FONT_WINTER, painter, false,
				1.0);
		qDebug() << "list append: " + textDesc;
	}

	QString max = "";
	QString min = "";
	QVariantMap f = forecastList.at(0).toMap();
	if (settings->value("temp").toString() == "c") {
		max = f.value("tempMaxC").toString() + "*C";
		min = f.value("tempMinC").toString() + "*C";
	} else {
		max = f.value("tempMaxF").toString() + "*F";
		min = f.value("tempMinF").toString() + "*F";
	}
	int y3 = y2 + 200 - 80;
	paintTextImage(x1 + 20, y3, 80, max + "/" + min, 0, FONT_WINTER, painter,
			true, 1.0);

	int x3 = 40;
	int y4 = 1024 - 20 - 130;
	paintTextImage(x3, y4, 130, areaName, 600, FONT_CANDAL, painter, false,
			1.0);
	//
	int y5 = y4 - 60;
	QString word = dateNowText + "  " + timeNowText;
	qDebug() << "word = " + word;
	paintTextImage(x3, y5, 60, word, 0, FONT_WINTER, painter, false, 1.0);

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);

}

void ApplicationUI::paintImage2(QPainter &painter, QSize &size) {

	int imageWidth = size.width();
	int imageHeight = size.height();

	int y0 = ((imageHeight * 2) / 3);
		QColor color(0,0,0);
		paintBG(painter, 0, y0, imageWidth, imageHeight / 3, color, 0.2);

		int x1 = 30;
		int y1 = 30;
		paintTextImage(x1, y1, 60, dateNowText, 0, FONT_WINTER, painter,
				false, 1.0);
		y1 += 60;
		paintTextImage(x1, y1, 60, timeNowText, 0, FONT_WINTER, painter,
				false, 1.0);

		QString iconfileName;
		QImageReader reader;
		QImage icon;
		QSize iconSize;
		iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
		reader.setFileName(iconfileName);
		icon = reader.read();
		iconSize = icon.size();
		int nh = 200;
		int nw = (nh * iconSize.width()) / iconSize.height();
//		ImageView* imageView = ImageView::create(iconfileName);
//		imageView->setPreferredSize(nw, nh);
//		AbsoluteLayoutProperties* imgProperties =
//				AbsoluteLayoutProperties::create();

		int x2 = 768 - nw - 40;
		int y3 = y0 - nh / 2 + 30;
//		imgProperties->setPositionX(x2);
//		imgProperties->setPositionY(y3);
//		imageView->setLayoutProperties(imgProperties);
//		imgProperties = 0;
//		mainContainer2->add(imageView);
//		imageView = 0;
		paintImageView(painter, iconfileName, 200, x2, y3, 1.0);

		QString tempText = "";
		if (settings->value("temp").toString() == "c") {
			tempText = temp_C + "*C";
		} else {
			tempText = temp_F + "*F";
		}
		int x3 = 540;
		int y4 = y0 + nh / 2 + 20;
		paintTextImage(x3, y4, 160, tempText, 0, FONT_WINTER, painter, true,
				1.0);

		iconfileName = assetFolder + "images/redpin_big" + ".png";
		reader.setFileName(iconfileName);
		icon = reader.read();
		iconSize = icon.size();
//		ImageView* imageView2 = ImageView::create(iconfileName);
		int newHeight = 160;
		int newWidth = (newHeight * iconSize.width()) / iconSize.height();
//		imageView2->setPreferredSize(newWidth, newHeight);
//		AbsoluteLayoutProperties* imgProperties2 =
//				AbsoluteLayoutProperties::create();

		int x4 = 50;
		int y5 = y0 - iconSize.height() / 2 + 90;

		qDebug() << "renderContainer2";
		qDebug() << "y5: " + QString::number(y5);
		qDebug() << "iconSize.height(): " + QString::number(iconSize.height());

//		imgProperties2->setPositionX(x4);
//		imgProperties2->setPositionY(y5);
//		imageView2->setLayoutProperties(imgProperties2);
//		imgProperties2 = 0;
//		mainContainer2->add(imageView2);
//		imageView2 = 0;
		paintImageView(painter, iconfileName, newHeight, x4, y5, 1.0);

		int y6 = y5 + newHeight + 20;
		int x5 = x4 - 30;
		paintTextImage(x5, y6, 120, areaName, 370, FONT_CANDAL, painter,
				false, 1.0);

		y6 += 120 - 20;
		paintTextImage(x5, y6, 80, country, 370, FONT_CANDAL, painter, false,
				1.0);

		//draw logo
		int xl = 480;
		int yl = 5;
		iconfileName = assetFolder + "images/logotran.png";
		paintImageView(painter, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::paintImage3(QPainter &painter, QSize &size) {

	int imageWidth = size.width();
	int imageHeight = size.height();

	int y0 = (imageHeight / 2) - 30;
	QColor color(0, 0, 0);
	paintBG(painter, 0, y0, imageWidth, (imageHeight / 2) + 30, color, 0.2);

	int xd = 30;
	int yd = 30;
	paintTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, painter, false,
			1.0);
	yd += 60;
	paintTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, painter, false,
			1.0);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	int x1 = 30;
	int y1 = y0 + 20;
	paintTextImage(x1, y1, 80, weatherDescValue + " / " + tempText, 650,
			FONT_CANDAL, painter, true, 1.0);

	y1 += 60;
	paintTextImage(x1, y1, 150, areaName, 580, FONT_CANDAL, painter, false,
			1.0);

	y1 += 180;
	int x2 = 128;
	if (foreCastQueue.size() > 0) {

		for (int i = 1; i < 4; i++) {
			QVariantMap map = foreCastQueue.at(i).toMap();
			QString path = "";

			qDebug()
					<< "map.value(weatherCode) = "
							+ map.value("weatherCode").toString();
			if (isDay) {
				path =
						dayIconMap.value(map.value("weatherCode").toString()).toString();

			} else {
				path =
						nightIconMap.value(map.value("weatherCode").toString()).toString();

			}
			iconfileName = assetFolder + "images/weather/" + path + ".png";
			int yy = y1 - 30;
			paintImageView(painter, iconfileName, 150, x2 - 75, yy, 1.0);

			yy += 150;

			paintTextImage(x2 - 25, yy, 70, (map.value("dayOfWeek").toString()),
					0, FONT_WINTER, painter, false, 1.0);
			yy += 70;

			QString max = "";
			QString min = "";
			if (settings->value("temp").toString() == "c") {
				max = map.value("tempMaxC").toString() + "*C";
				min = map.value("tempMinC").toString() + "*C";
			} else {
				max = map.value("tempMaxF").toString() + "*F";
				min = map.value("tempMinF").toString() + "*F";
			}
			paintTextImage(x2 - 90, yy, 70, max + "/" + min, 0, FONT_WINTER,
					painter, true, 1.0);

			x2 += 240;
		}
	}

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::paintImage4(QPainter &painter, QSize &size) {

	int imageWidth = size.width();
	int imageHeight = size.height();

	QColor color(0, 0, 0);
	paintBG(painter, 0, 0, imageWidth, imageHeight / 2 - 70, color, 0.2);

	int x0 = 40;
	int y0 = 30;
	paintTextImage(x0, y0, 170, areaName, 500, FONT_CANDAL, painter, false,
			1.0);

	y0 += 190 - 50;
	paintTextImage(x0, y0, 90, country, 600, FONT_CANDAL, painter, false, 1.0);

	y0 += 80;

	iconfileName = assetFolder + "images/termometer.png";
	int x00 = paintImageView(painter, iconfileName, 130, x0, y0 + 40, 1.0);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	paintTextImage(x00 + 10, y0 + 20, 160, tempText, 0, FONT_WINTER, painter,
			true, 1.0);

	int x1 = 768 - 280;
	int y1 = imageHeight / 2 - 150 - 80;
	//pressure
	QString pathPressure = assetFolder + "images/pressure.png";
	int xP = paintImageView(painter, pathPressure, 60, x1, y1, 1.0);
	paintTextImage(xP + 20, y1, 60, pressure + " hPa", 0, FONT_WINTER, painter,
			false, 1.0);

	y1 += 60 + 10;
	//precipMM
	QString pathPrecipMM = assetFolder + "images/rain.png";
	paintImageView(painter, pathPrecipMM, 60, x1, y1, 1.0);
	paintTextImage(xP + 20, y1, 60, precipMM + " mm", 0, FONT_WINTER, painter,
			false, 1.0);

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::paintImage5(QPainter &painter, QSize &size) {

	int imageWidth = size.width();
	int imageHeight = size.height();

	int y0 = ((imageHeight * 2) / 3);
	QColor color(0, 0, 0);
	paintBG(painter, 0, y0, imageWidth, imageHeight / 3, color, 0.2);

	int x1 = 30;
	int y1 = 30;
	paintTextImage(x1, y1, 60, dateNowText, 0, FONT_WINTER, painter, false,
			1.0);
	y1 += 60;
	paintTextImage(x1, y1, 60, timeNowText, 0, FONT_WINTER, painter, false,
			1.0);

	int x0 = 30;
	y0 -= 80;
	paintTextImage(x0, y0, 160, areaName, 380, FONT_CANDAL, painter, false,
			1.0);
	paintTextImage(x0, y0 + 140, 80, weatherDescValue, 500, FONT_CANDAL,
			painter, false, 1.0);

	iconfileName = assetFolder + "images/redpin_big" + ".png";
	paintImageView(painter, iconfileName, 180, 600, y0 + 20, 1.0);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}

	int y3 = y0 + 240;

	iconfileName = assetFolder + "images/termometer.png";
	int x2 = paintImageView(painter, iconfileName, 120, x0, y3 + 30, 1.0);

	paintTextImage(x2 + 10, y3, 160, tempText, 0, FONT_WINTER, painter, true,
			1.0);

	y3 += 10;
	//pressure
	QString pathPressure = assetFolder + "images/pressure.png";
	int xP = paintImageView(painter, pathPressure, 60, 500, y3, 1.0);
	paintTextImage(xP + 20, y3, 60, pressure + " hPa", 0, FONT_WINTER, painter,
			false, 1.0);

	y3 += 60 + 10;
	//precipMM
	QString pathPrecipMM = assetFolder + "images/rain.png";
	paintImageView(painter, pathPrecipMM, 60, 500, y3, 1.0);
	paintTextImage(xP + 20, y3, 60, precipMM + " mm", 0, FONT_WINTER, painter,
			false, 1.0);

	//	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
	//	renderImageView(mainContainer5, iconfileName, 200, 550, y3-30, 1.0);

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);

}

void ApplicationUI::paintImage6(QPainter &painter, QSize &size) {

	int imageWidth = size.width();
	int imageHeight = size.height();
	int delta = 50;

	//black layer
	int y0 = imageHeight / 2 + delta;
	QColor color(0, 0, 0);
	paintBG(painter, 0, y0, imageWidth, imageHeight / 2 - delta, color, 0.2);

	int xd = 30;
	int yd = 30;
	paintTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, painter, false,
			1.0);
	yd += 60;
	paintTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, painter, false,
			1.0);

	int x0 = 30;
	QString pathLine = assetFolder + "images/line.png";
	paintImageView(painter, pathLine, 0, 0, y0 + 120, 1.0);
	paintTextImage(x0, y0, 150, areaName, 550, FONT_CANDAL, painter, false,
			1.0);

	int y1 = y0 + 130;
	paintImageView(painter, pathLine, 0, 0, y1 + 90, 1.0);
	paintTextImage(x0, y1, 100,
			getTempText() + " / " + weatherDescValue.toUpper(), 650,
			FONT_WINTER, painter, true, 1.0);

	int y2 = y1 + 130;
	//pressure
	QString pathPressure = assetFolder + "images/pressure.png";
	int xP = paintImageView(painter, pathPressure, 60, x0, y2, 1.0);
	int xPt = paintTextImage(xP, y2, 60, pressure + " hPa", 0, FONT_WINTER,
			painter, false, 1.0);

	//humidity
	QString pathHumidity = assetFolder + "images/humidity.png";
	int xH = paintImageView(painter, pathHumidity, 60, xPt + 60, y2, 1.0);
	int xHt = paintTextImage(xH, y2, 60, humidity + " %", 0, FONT_WINTER,
			painter, false, 1.0);

	//wind speed
	QString pathWind = assetFolder + "images/wind.png";
	int xW = paintImageView(painter, pathWind, 60, xHt + 60, y2, 1.0);
	paintTextImage(xW, y2, 60, windspeedKmph + " km/h", 0, FONT_WINTER, painter,
			false, 1.0);

	int y3 = y2 + 80;
	//precipMM
	QString pathPrecipMM = assetFolder + "images/rain.png";
	paintImageView(painter, pathPrecipMM, 60, x0, y3, 1.0);
	paintTextImage(xP, y3, 60, precipMM + " mm", 0, FONT_WINTER, painter, false,
			1.0);

	//visibility
	QString pathVisibility = assetFolder + "images/visibility.png";
	paintImageView(painter, pathVisibility, 60, xPt + 60, y3, 1.0);
	paintTextImage(xH, y3, 60, visibility + " m", 0, FONT_WINTER, painter,
			false, 1.0);

	//wind direction
	QString pathDirection = assetFolder + "images/direction.png";
	paintImageView(painter,pathDirection,60, xHt+60, y3, 1.0);
	QString directionText = getDirectionText(true);
	paintTextImage(xW, y3, 60, directionText, 0, FONT_WINTER, painter, false,
			1.0);

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);

}

void ApplicationUI::paintImage7(QPainter &painter, QSize &size) {
	int imageWidth = size.width();
	int imageHeight = size.height();
	int xd = 30;
	int yd = 30;
	paintTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, painter, false,
			1.0);
	yd += 60;
	paintTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, painter, false,
			1.0);

	int x0 = (imageWidth - 500) / 2;
	int y0 = (imageHeight - 500) / 2;
	//	int y0 = topY + ((imageHeight - 80 - 120 - 20) - 500)/2 + 50;
	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
	paintImageView(painter, iconfileName, 500, x0, y0, 0.7);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	int width1 = getWidthText(80, tempText + " | " + weatherDescValue,
			FONT_WINTER, 0, true);
	int x1 = (768 - width1) / 2;
	int y1 = imageHeight - 80 - 120 - 20;
	paintTextImage(x1, y1, 80, tempText + " | " + weatherDescValue, 0,
			FONT_WINTER, painter, true, 1.0);

	y1 += 80;

	QString aName = areaName;
	if (aName.length() > 12) {
		aName = aName.left(12);
		aName = aName + "...";
	}
	qDebug() << "aName = " + aName;

	int width2 = getWidthText(120, aName + ", " + country2, FONT_WINTER, 0,
			true);
	int x2 = (768 - width2) / 2;
	paintTextImage(x2, y1, 120, aName + ", " + country2, 0, FONT_WINTER,
			painter, true, 1.0);

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::paintImage8(QPainter &painter, QSize &size) {
//	int imageWidth = size.width();
	int imageHeight = size.height();
	int xd = 30;
	int yd = 30;
	paintTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, painter, false,
			1.0);
	yd += 60;
	paintTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, painter, false,
			1.0);

	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*";
	} else {
		tempText = temp_F + "*";
	}
	getWidthText(350, tempText, FONT_CANDAL, 0, true);
	int x1 = 30;
	int y1 = (imageHeight - 350);
	int x2 = paintTextImage(x1, y1, 350, tempText, 0, FONT_WINTER, painter,
			true, 1.0);

	x2 -= 40;
	int y2 = (imageHeight - 100 - 40);
	paintTextImage(x2, y2, 100, areaName, 650, FONT_WINTER, painter, true, 1.0);

	int x3 = 768 - 200 - 30;
	iconfileName = assetFolder + "images/weather/" + weatherIconPath + ".png";
	paintImageView(painter, iconfileName, 200, x3, y1, 1.0);

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::paintImage9(QPainter &painter, QSize &size) {
	int imageWidth = size.width();
	int imageHeight = size.height();
	int x0 = 30;
	int y0 = (imageHeight * 3) / 4 - 50;
	QColor color(0, 0, 0);
	paintBG(painter, 0, y0, imageWidth, imageHeight / 4 + 50, color, 0.2);

	int xd = 30;
	int yd = 30;
	paintTextImage(xd, yd, 60, dateNowText, 0, FONT_WINTER, painter, false,
			1.0);
	yd += 60;
	paintTextImage(xd, yd, 60, timeNowText, 0, FONT_WINTER, painter, false,
			1.0);

	paintTextImage(x0, y0, 100, areaName, 600, FONT_CANDAL, painter, false,
			1.0);

	int y1 = y0 + 90;
	iconfileName = assetFolder + "images/direction.png";
	int x1 = paintImageView(painter, iconfileName, 100, x0, y1, 1.0);
	QString directionText = getDirectionText(true);
	int x2 = paintTextImage(x0, y1 + 100, 80, directionText, 0, FONT_WINTER,
			painter, false, 1.0);
	int x3 = 0;
	if (x1 >= x2) {
		x3 = x1 + 10;
	} else {
		x3 = x2 + 10;
	}
	int x4 = paintTextImage(x3, y1 - 15, 250, windspeedKmph, 0, FONT_CANDAL,
			painter, false, 1.0) + 10;
	paintTextImage(x4, y1 + 70, 120, "km/h", 0, FONT_WINTER, painter, false,
			1.0);

	int x5 = 550;
	int y2 = y0 + (imageHeight - y0 - 50 - 10 - 50 - 10 - 50) / 2 + 30;
	//temperature
	QString pathTemp = assetFolder + "images/termometer.png";
	int xP = paintImageView(painter, pathTemp, 50, x5 + 15, y2, 1.0);
	QString tempText = "";
	if (settings->value("temp").toString() == "c") {
		tempText = temp_C + "*C";
	} else {
		tempText = temp_F + "*F";
	}
	xP += 30;
	paintTextImage(xP, y2, 50, tempText, 0, FONT_WINTER, painter, true, 1.0);
	y2 += 50 + 10;
	//pressure
	QString pathPressure = assetFolder + "images/pressure.png";
	paintImageView(painter, pathPressure, 50, x5, y2, 1.0);
	paintTextImage(xP, y2, 50, pressure + " hPa", 0, FONT_WINTER, painter,
			false, 1.0);
	y2 += 50 + 10;
	//precipMM
	QString pathPrecipMM = assetFolder + "images/rain.png";
	paintImageView(painter, pathPrecipMM, 50, x5, y2, 1.0);
	paintTextImage(xP, y2, 50, precipMM + " mm", 0, FONT_WINTER, painter, false,
			1.0);

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);

}

void ApplicationUI::paintImage10(QPainter &painter, QSize &size) {
//	int imageWidth = size.width();
//	int imageHeight = size.height();

	//draw area, country
	int x0 = 20;
	int y0 = 40;
	paintTextImage(x0, y0, 150, areaName, 550, FONT_CANDAL, painter, false,
			1.0);
	y0 += 150;
	paintTextImage(x0, y0 - 20, 80, country, 550, FONT_CANDAL, painter, false,
			1.0);

	int y1 = y0 + 80;
	int x1 = 350;
	int x2 = 510;
	if (foreCastQueue.size() > 0) {

		qDebug()
				<< "foreCastQueue.size() = "
						+ QString::number(foreCastQueue.size());

		for (int i = 0; i < 7; i++) {
			QVariantMap map = foreCastQueue.at(i).toMap();

			paintTextImage(x0, y1 + 10, 80,
					(map.value("fullDayOfWeek").toString()), x1, FONT_WINTER,
					painter, false, 1.0);
			QString path = "";
			if (isDay) {
				path =
						dayIconMap.value(map.value("weatherCode").toString()).toString();
			} else {
				path =
						nightIconMap.value(map.value("weatherCode").toString()).toString();
			}
			iconfileName = assetFolder + "images/weather/" + path + ".png";
			paintImageView(painter, iconfileName, 90, x1, y1, 1.0);

			QString max = "";
			QString min = "";
			if (settings->value("temp").toString() == "c") {
				max = map.value("tempMaxC").toString() + "*C";
				min = map.value("tempMinC").toString() + "*C";
			} else {
				max = map.value("tempMaxF").toString() + "*F";
				min = map.value("tempMinF").toString() + "*F";
			}
			paintTextImage(x2, y1 + 10, 80, max + "/" + min, 0, FONT_WINTER,
					painter, true, 1.0);

			if (i <= 5) {
				//draw line
				iconfileName = assetFolder + "images/line.png";
				paintImageView(painter, iconfileName, 14, 0, y1 + 90, 1.0);
			}
			y1 += 110;
		}
	}

	//draw logo
	int xl = 480;
	int yl = 5;
	iconfileName = assetFolder + "images/logotran.png";
	paintImageView(painter, iconfileName, 40, xl, yl, 0.5);
}

void ApplicationUI::refreshLocation() {
	qDebug() << "refreshLocation()";
	if (locationDataModel) {
		qDebug() << "locationDataModel != null";
		locationDataModel->clear();
		locationList = _db->getLocationByLatLon(m_latitude, m_longitude);
		if (locationList.length() > 0) {
			qDebug()
					<< "locationList.length() = "
							+ QString::number(locationList.length());
			locationDataModel->insertList(locationList);
		} else {
			qDebug() << "locationList.length() == 0";
		}
	} else {
		qDebug() << "locationDataModel == null";
	}
}

void ApplicationUI::refreshForecast() {
	qDebug() << "refreshForecast()";
	forecastList = _db->getForecastByTime(timeRequestData);
}

void ApplicationUI::chooseLocation(QVariantList indexPath) {
	qDebug() << "chooseLocation()";
	QVariantMap locationObj = locationDataModel->data(indexPath).toMap();
	areaName = locationObj.value("areaName").toString();
	country = locationObj.value("country").toString();
	region = locationObj.value("region").toString();

	qDebug() << "---- areaName: " + areaName;
	qDebug() << "---- country: " + country;
	qDebug() << "---- region: " + region;

//	loadCon1 = false;
//	loadCon2 = false;
//	loadCon3 = false;
//	loadCon4 = false;
//	loadCon5 = false;
//	loadCon6 = false;
//	loadCon7 = false;
//	loadCon8 = false;
//	loadCon9 = false;
//	loadCon10 = false;

	refresh();

//	if (designIndex == 0) {
//		renderContainer1();
//	} else if (designIndex == 1) {
//		renderContainer2();
//	} else if (designIndex == 2) {
//		renderContainer3();
//	} else if (designIndex == 3) {
//		renderContainer4();
//	} else if (designIndex == 4) {
//		renderContainer5();
//	} else if (designIndex == 5) {
//		renderContainer6();
//	} else if (designIndex == 6) {
//		renderContainer7();
//	} else if (designIndex == 7) {
//		renderContainer8();
//	} else if (designIndex == 8) {
//		renderContainer9();
//	} else if (designIndex == 9) {
//		renderContainer10();
//	}

}

void ApplicationUI::refresh() {
	closeHelp();
	isLoadComplete = false;
	renderContainer1();
	renderContainer2();
	renderContainer3();
	renderContainer4();
	renderContainer5();
	renderContainer6();
	renderContainer7();
	renderContainer8();
	renderContainer9();
	renderContainer10();
	isLoadComplete = true;
}

bool ApplicationUI::loadComplete() {
	return isLoadComplete;
}

QString ApplicationUI::getCountryText() {
	return country + ", " + country2;
}

QString ApplicationUI::getLattitudeText() {
	return QString::number(m_latitude);
}

QString ApplicationUI::getLongitudeText() {
	return QString::number(m_longitude);
}

bool ApplicationUI::addLocation(QString name, QString desc) {
	qDebug() << "addLocation() name:" + name + ", desc:" + desc;
	if (name.isNull() || name.isEmpty()) {
		//alert
		showError("Please enter your location name!");
		return false;
	} else {
		if (!checkLocationNameEng(name.toUpper())) {
			if (desc.isNull() || desc.isEmpty()) {
				desc = "";
			}
			//insert
			LocationObject loc = LocationObject();
			loc.setAreaName(name);
			loc.setCountry(country);
			loc.setCountry2(country2);
			loc.setIsUserLocation("y");
			loc.setLatitude(m_latitude);
			loc.setLongitude(m_longitude);
			loc.setRegion(desc);
			loc.setCreatetime(getTimeMillis());
			_db->insertOrUpdateLocation(loc);
			refreshLocation();

			emit closeAddLocationSheet();
			return true;
		} else {
			//alert
			showError(
					"Your location name contains any letter that isn't an English character. Please try again.");
			return false;
		}

	}

}

bool ApplicationUI::checkLocationNameEng(QString name) {
	qDebug() << "name: " + name;
	bool notFound = false;
	for (int i = 0; i < name.length(); i++) {
		qDebug() << "name[" + QString::number(i) + "] = " + name.at(i);

		// =-+*/:;'"()#$!?@~`[]{}<>^%_÷±•\|¤«»¥€£$¡¿&
		if (name.at(i) == 'A' || name.at(i) == 'B' || name.at(i) == 'C'
				|| name.at(i) == 'D' || name.at(i) == 'E' || name.at(i) == 'F'
				|| name.at(i) == 'G' || name.at(i) == 'H' || name.at(i) == 'I'
				|| name.at(i) == 'J' || name.at(i) == 'K' || name.at(i) == 'L'
				|| name.at(i) == 'M' || name.at(i) == 'N' || name.at(i) == 'O'
				|| name.at(i) == 'P' || name.at(i) == 'Q' || name.at(i) == 'R'
				|| name.at(i) == 'S' || name.at(i) == 'T' || name.at(i) == 'U'
				|| name.at(i) == 'V' || name.at(i) == 'W' || name.at(i) == 'X'
				|| name.at(i) == 'Y' || name.at(i) == 'Z' || name.at(i) == ' '
				|| name.at(i) == '*' || name.at(i) == '(' || name.at(i) == ')'
				|| name.at(i) == '.' || name.at(i) == ',' || name.at(i) == '&'
				|| name.at(i) == '@' || name.at(i) == '!' || name.at(i) == '?'
				|| name.at(i) == '-' || name.at(i) == '=' || name.at(i) == '+'
				|| name.at(i) == '/' || name.at(i) == ':' || name.at(i) == ';'
				|| name.at(i) == '\'' || name.at(i) == '\"' || name.at(i) == '#'
				|| name.at(i) == '~' || name.at(i) == '`' || name.at(i) == '['
				|| name.at(i) == ']' || name.at(i) == '{' || name.at(i) == '}'
				|| name.at(i) == '<' || name.at(i) == '>' || name.at(i) == '%'
				|| name.at(i) == '_' || name.at(i) == '\\' || name.at(i) == '|'
				|| name.at(i) == '$'

				|| name.at(i) == '÷' || name.at(i) == '±' || name.at(i) == '•'
				|| name.at(i) == '¤' || name.at(i) == '«' || name.at(i) == '»'
				|| name.at(i) == '¥' || name.at(i) == '€' || name.at(i) == '£'
				|| name.at(i) == '¡' || name.at(i) == '¿'

				|| name.at(i) == '1' || name.at(i) == '2' || name.at(i) == '3'
				|| name.at(i) == '4' || name.at(i) == '5' || name.at(i) == '6'
				|| name.at(i) == '7' || name.at(i) == '8' || name.at(i) == '9'
				|| name.at(i) == '0') {
			continue;
		} else {
			notFound = true;
			qDebug() << "FOUND NOT ENGLISH!!!";
			break;
		}
	}
	return notFound;
}

bool ApplicationUI::doEditLocation(QString createtime, QString newName,
		QString newDesc) {
	qDebug()
			<< "doEditLocation() createtime:" + createtime + ", newName:"
					+ newName + ", newDesc:" + newDesc;
	if (newName.isNull() || newName.isEmpty()) {
		//alert
		showError("Please enter your location name!");
		return false;
	} else {
		if (!checkLocationNameEng(newName.toUpper())) {
			if (newDesc.isNull() || newDesc.isEmpty()) {
				newDesc = "";
			}

			_createtime = createtime;
			_newName = newName;
			_newDesc = newDesc;

			SystemDialog *popup = new SystemDialog("OK", "Cancel");
			QString wordHead("Confirm editing?");
			popup->setTitle(wordHead);
//			QString wordBody("");
//			popupError->setBody(/*QString(wordBody.toUtf8())*/wordBody);
			connect(popup, SIGNAL(finished(bb::system::SystemUiResult::Type)),
					this, SLOT(
							clickEditLocationOK(
									bb::system::SystemUiResult::Type)));
			popup->show();

			return true;
		} else {
			//alert
			showError(
					"Your location name contains any letter that isn't an English character. Please try again.");
			return false;
		}

	}
}

void ApplicationUI::clickEditLocationOK(
		bb::system::SystemUiResult::Type value) {
	qDebug() << "clickEditLocationOK()";
	if (value == bb::system::SystemUiResult::ConfirmButtonSelection) {
		//update
		_db->updateLocationByCreateTime(_createtime, _newName, _newDesc);
		refreshLocation();

		emit closeEditLocationSheet();
	} else if (value == bb::system::SystemUiResult::CancelButtonSelection) {

	}

	_createtime = "";
	_newName = "";
	_newDesc = "";

}
