/****************************************************************************
** Meta object code from reading C++ file 'applicationui.hpp'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../src/applicationui.hpp"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'applicationui.hpp' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_ApplicationUI[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      59,   14, // methods
       1,  309, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,
      31,   14,   14,   14, 0x05,
      50,   14,   14,   14, 0x05,
      74,   14,   14,   14, 0x05,
     100,   14,   14,   14, 0x05,
     125,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
     149,   14,   14,   14, 0x08,
     168,   14,   14,   14, 0x08,
     187,   14,   14,   14, 0x08,
     206,   14,   14,   14, 0x08,
     225,   14,   14,   14, 0x08,
     244,   14,   14,   14, 0x08,
     263,   14,   14,   14, 0x08,
     282,   14,   14,   14, 0x08,
     301,   14,   14,   14, 0x08,
     320,   14,   14,   14, 0x08,
     340,   14,   14,   14, 0x08,
     365,  357,   14,   14, 0x08,
     393,  387,   14,   14, 0x08,
     453,  447,   14,   14, 0x08,
     494,  447,   14,   14, 0x08,
     536,  447,   14,   14, 0x08,
     577,   14,   14,   14, 0x08,
     598,  594,   14,   14, 0x08,
     632,   14,   14,   14, 0x08,

 // methods: signature, parameters, type, tag, flags
     656,   14,   14,   14, 0x02,
     677,   14,   14,   14, 0x02,
     690,   14,   14,   14, 0x02,
     705,   14,   14,   14, 0x02,
     723,   14,   14,   14, 0x02,
     734,   14,   14,   14, 0x02,
     745,   14,   14,   14, 0x02,
     762,   14,  757,   14, 0x02,
     785,  775,  757,   14, 0x02,
     814,   14,  757,   14, 0x02,
     838,  831,   14,   14, 0x02,
     868,  860,   14,   14, 0x02,
     885,  882,   14,   14, 0x02,
     908,   14,   14,   14, 0x02,
     920,   14,   14,   14, 0x02,
     936,   14,   14,   14, 0x02,
     950,   14,  757,   14, 0x02,
     970,  965,   14,   14, 0x02,
    1007,   14,  999,   14, 0x02,
    1041, 1029,   14,   14, 0x02,
    1071, 1062,   14,   14, 0x02,
    1096, 1062,   14,   14, 0x02,
    1127, 1121,   14,   14, 0x02,
    1147,   14,  999,   14, 0x02,
    1164,   14,  999,   14, 0x02,
    1183,   14,  999,   14, 0x02,
    1202,   14,   14,   14, 0x02,
    1222,   14,   14,   14, 0x02,
    1242,   14,   14,   14, 0x02,
    1261, 1256,   14,   14, 0x02,
    1281,   14,   14,   14, 0x02,
    1326, 1299,  757,   14, 0x02,
    1366,   14,   14,   14, 0x02,
    1391, 1381,   14,   14, 0x02,

 // properties: name, type, flags
    1420,  757, 0x01495103,

 // properties: notify_signal_id
       2,

       0        // eod
};

static const char qt_meta_stringdata_ApplicationUI[] = {
    "ApplicationUI\0\0chooseCelcius()\0"
    "chooseFahrenheit()\0cameraModeChanged(bool)\0"
    "showTermConditionDialog()\0"
    "closeEditLocationSheet()\0"
    "closeAddLocationSheet()\0renderContainer1()\0"
    "renderContainer2()\0renderContainer3()\0"
    "renderContainer4()\0renderContainer5()\0"
    "renderContainer6()\0renderContainer7()\0"
    "renderContainer8()\0renderContainer9()\0"
    "renderContainer10()\0onInvokeResult()\0"
    "success\0onInviteSuccess(bool)\0value\0"
    "clickEditLocationOK(bb::system::SystemUiResult::Type)\0"
    "reply\0onRequestFinishedWeather(QNetworkReply*)\0"
    "onRequestFinishedLocation(QNetworkReply*)\0"
    "onRequestFinishedCountry(QNetworkReply*)\0"
    "onShutterFired()\0pos\0"
    "positionUpdated(QGeoPositionInfo)\0"
    "positionUpdateTimeout()\0startUpdateWeather()\0"
    "openFBPage()\0openAppWorld()\0"
    "openAppWorldPro()\0openMail()\0showHelp()\0"
    "closeHelp()\0bool\0isShowHelp()\0name,desc\0"
    "addLocation(QString,QString)\0"
    "isShowTutorial()\0isShow\0setShowTutorial(bool)\0"
    "selTemp\0setTemp(bool)\0ok\0"
    "setTermCondition(bool)\0saveImage()\0"
    "restartCamera()\0closeCamera()\0"
    "loadComplete()\0path\0setSelectedFilePath(QString)\0"
    "QString\0getSelectedFilePath()\0isOpenFlash\0"
    "setFlashCamera(bool)\0fileName\0"
    "manipulatePhoto(QString)\0"
    "showPhotoInCard(QString)\0index\0"
    "setDesignIndex(int)\0getCountryText()\0"
    "getLattitudeText()\0getLongitudeText()\0"
    "showErrorInternet()\0openTermOfService()\0"
    "openPrivacy()\0next\0refreshDesign(bool)\0"
    "refreshLocation()\0createtime,newName,newDesc\0"
    "doEditLocation(QString,QString,QString)\0"
    "switchCamera()\0indexPath\0"
    "chooseLocation(QVariantList)\0cameraMode\0"
};

void ApplicationUI::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        ApplicationUI *_t = static_cast<ApplicationUI *>(_o);
        switch (_id) {
        case 0: _t->chooseCelcius(); break;
        case 1: _t->chooseFahrenheit(); break;
        case 2: _t->cameraModeChanged((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 3: _t->showTermConditionDialog(); break;
        case 4: _t->closeEditLocationSheet(); break;
        case 5: _t->closeAddLocationSheet(); break;
        case 6: _t->renderContainer1(); break;
        case 7: _t->renderContainer2(); break;
        case 8: _t->renderContainer3(); break;
        case 9: _t->renderContainer4(); break;
        case 10: _t->renderContainer5(); break;
        case 11: _t->renderContainer6(); break;
        case 12: _t->renderContainer7(); break;
        case 13: _t->renderContainer8(); break;
        case 14: _t->renderContainer9(); break;
        case 15: _t->renderContainer10(); break;
        case 16: _t->onInvokeResult(); break;
        case 17: _t->onInviteSuccess((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->clickEditLocationOK((*reinterpret_cast< bb::system::SystemUiResult::Type(*)>(_a[1]))); break;
        case 19: _t->onRequestFinishedWeather((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 20: _t->onRequestFinishedLocation((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 21: _t->onRequestFinishedCountry((*reinterpret_cast< QNetworkReply*(*)>(_a[1]))); break;
        case 22: _t->onShutterFired(); break;
        case 23: _t->positionUpdated((*reinterpret_cast< const QGeoPositionInfo(*)>(_a[1]))); break;
        case 24: _t->positionUpdateTimeout(); break;
        case 25: _t->startUpdateWeather(); break;
        case 26: _t->openFBPage(); break;
        case 27: _t->openAppWorld(); break;
        case 28: _t->openAppWorldPro(); break;
        case 29: _t->openMail(); break;
        case 30: _t->showHelp(); break;
        case 31: _t->closeHelp(); break;
        case 32: { bool _r = _t->isShowHelp();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 33: { bool _r = _t->addLocation((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 34: { bool _r = _t->isShowTutorial();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 35: _t->setShowTutorial((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 36: _t->setTemp((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 37: _t->setTermCondition((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 38: _t->saveImage(); break;
        case 39: _t->restartCamera(); break;
        case 40: _t->closeCamera(); break;
        case 41: { bool _r = _t->loadComplete();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 42: _t->setSelectedFilePath((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 43: { QString _r = _t->getSelectedFilePath();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 44: _t->setFlashCamera((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 45: _t->manipulatePhoto((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 46: _t->showPhotoInCard((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 47: _t->setDesignIndex((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 48: { QString _r = _t->getCountryText();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 49: { QString _r = _t->getLattitudeText();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 50: { QString _r = _t->getLongitudeText();
            if (_a[0]) *reinterpret_cast< QString*>(_a[0]) = _r; }  break;
        case 51: _t->showErrorInternet(); break;
        case 52: _t->openTermOfService(); break;
        case 53: _t->openPrivacy(); break;
        case 54: _t->refreshDesign((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 55: _t->refreshLocation(); break;
        case 56: { bool _r = _t->doEditLocation((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QString(*)>(_a[3])));
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        case 57: _t->switchCamera(); break;
        case 58: _t->chooseLocation((*reinterpret_cast< QVariantList(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData ApplicationUI::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject ApplicationUI::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_ApplicationUI,
      qt_meta_data_ApplicationUI, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &ApplicationUI::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *ApplicationUI::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *ApplicationUI::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_ApplicationUI))
        return static_cast<void*>(const_cast< ApplicationUI*>(this));
    return QObject::qt_metacast(_clname);
}

int ApplicationUI::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 59)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 59;
    }
#ifndef QT_NO_PROPERTIES
      else if (_c == QMetaObject::ReadProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = isCameraMode(); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::WriteProperty) {
        void *_v = _a[0];
        switch (_id) {
        case 0: setCameraMode(*reinterpret_cast< bool*>(_v)); break;
        }
        _id -= 1;
    } else if (_c == QMetaObject::ResetProperty) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 1;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 1;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void ApplicationUI::chooseCelcius()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void ApplicationUI::chooseFahrenheit()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}

// SIGNAL 2
void ApplicationUI::cameraModeChanged(bool _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void ApplicationUI::showTermConditionDialog()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void ApplicationUI::closeEditLocationSheet()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void ApplicationUI::closeAddLocationSheet()
{
    QMetaObject::activate(this, &staticMetaObject, 5, 0);
}
QT_END_MOC_NAMESPACE
